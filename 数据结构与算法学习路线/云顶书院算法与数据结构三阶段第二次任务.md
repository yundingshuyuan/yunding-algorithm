# 云顶书院算法与数据结构社群学习

### 学习内容：

- 树、二叉树、森林的定义
- 树的相关节点名称、度、深度、层次等相关概念
- 二叉树的五个性质（前三个掌握，后两个了解）
- 了解完全二叉树以及满二叉树
- 了解及实现树的存储结构（顺序、链式）
- 了解及实现遍历一个二叉树的方法

### 参考资料：

* 代码随想录：https://www.programmercarl.com/

* 算法第四版

  「算法 第四版.pdf」https://www.aliyundrive.com/s/5DE3Dcd3qLc 点击链接保存，或者复制本段内容，打开「阿里云盘」APP ，无需下载极速在线查看，视频原画倍速播放。

* 数据结构-严蔚敏

  「数据结构（C语言版）.pdf」https://www.aliyundrive.com/s/mxnaDUzYxbH 点击链接保存，或者复制本段内容，打开「阿里云盘」APP ，无需下载极速在线查看，视频原画倍速播放。

* 数据结构、算法与应用C++描述-机械工业出版社

  「数据结构、算法与应用  C+...描述  原书第2版.pdf」https://www.aliyundrive.com/s/qYQ1tqrZTtr 点击链接保存，或者复制本段内容，打开「阿里云盘」APP ，无需下载极速在线查看，视频原画倍速播放。

* 漫画算法小灰的算法之旅-Java描述

  「漫画算法小灰的算法之旅[www.j9p.com].pdf」https://www.aliyundrive.com/s/6USwfuPC1DH 点击链接保存，或者复制本段内容，打开「阿里云盘」APP ，无需下载极速在线查看，视频原画倍速播放。

## 本次作业

1. 实现一个二叉树（顺序、链式），

   - 并实现创建
   - 遍历
   - 求一个二叉树中一个节点的双亲节点
   - 求一个二叉树中一个节点的子节点
   - 输出一个二叉树所有叶子节点

2. 二叉树的层序遍历

   [102. 二叉树的层序遍历 - 力扣（LeetCode） (leetcode-cn.com)](https://gitee.com/link?target=https%3A%2F%2Fleetcode-cn.com%2Fproblems%2Fbinary-tree-level-order-traversal%2F)

语言可以自由选择，不做限制

### 作业验收

首先要在书院的数据结构与算法开源仓库的Learning/OS&CPU下建立一个自己的文件夹

仓库地址：

https://gitee.com/yundingshuyuan/yunding-algorithm/tree/dev/Learning/%E5%85%A8%E6%A0%88

* 1.2022年5月21日晚上10点前将作业提交至开源仓库中自己的文件夹中
* 2.文件夹命名格式：CPU&OS-吴勇-数据结构二阶段第一次任务
* 3.提交内容： 程序实现的源代码
* 4.不得无故不交作业，不得抄袭

