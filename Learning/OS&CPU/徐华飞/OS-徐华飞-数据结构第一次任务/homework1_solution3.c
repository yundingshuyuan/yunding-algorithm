#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#define num 6
#define min(x,y) ((x)<(y)?(x):(y))
void sys_err(char* str){
	perror(str);
}

int minSubArrayLen(int target, int* nums, int numsSize){
	int lef=0,rig=0,sum=0,len=0,ret=numsSize+1;//标志异常状态
	while(rig<numsSize){
		if ( sum < target ){//和小于目标
			sum += nums[rig]; len++; rig++;//右移动
		}
		for ( ;sum >= target ; ){//找到一个数，比target大
			ret=min(ret,len);//大于target的就可以判断len的长度
			sum -= nums[lef]; lef++; len--;//大于左移
		}
	}return ret>numsSize?0:ret;
}

int main(int argc,char* argv[]){
	int target,arr[num]={2,3,1,2,4,3};
	scanf("%d",&target);
	printf("%d",minSubArrayLen(target,arr,sizeof(arr)/sizeof(int)));
}
