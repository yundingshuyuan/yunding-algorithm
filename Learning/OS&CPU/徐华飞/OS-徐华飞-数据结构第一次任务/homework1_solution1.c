#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#define num 10 
void sys_err(char* str){
	perror(str);
}

int minSubArrayLen(int target, int* nums, int numsSize)
{
	int sum,len,flag=0,min=numsSize;
	if(!numsSize)
		return 0;
	for(int i=0;i<numsSize;i++){
		sum=nums[i];len=1;
		for(int j=i+1;j<numsSize && sum<target;j++){
			sum+=nums[j];len++;
		}if(sum>=target){
			min=len<min?len:min;
			flag=1;
		}
	}if(!flag)
			return 0;
	return min;
}

int main(int argc,char* argv[]){
	int target,arr[num]={5,1,3,5,10,7,4,9,2,8};
	scanf("%d",&target);
	printf("%d",minSubArrayLen(target,arr,sizeof(arr)/sizeof(int)));
	return 0;
}
