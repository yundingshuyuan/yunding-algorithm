#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#define num 6
#define Search_min_string Sea_min_s;

void sys_err(char* str){
	perror(str);
}

int Sea_min_s(const int* target,const int* nums,const int* numsSize,int lef,int rig,int mid_index);//分治照字符串

int min(const int* pf,const int* pr,const int* pm,const int* numsSize)//返回最小值
{
	int min=(*pf)<(*pr)?(*pf):(*pr);
	return min<(*pm)?min:(*pm);
}

int left_right(const int* target,const int* nums,const int* numsSize,int lef,int rig)
{
	int mid_index=(rig+lef)/2,sum=0,len=0;
	if((rig-lef)>=2){
		return Sea_min_s(target,nums,numsSize,lef,rig,mid_index);
	}else{
		for(int i=lef;(sum<=(*target)) && i<=rig;i++){
			sum+=nums[i];len++;
		}
		if(sum>=(*target))
			return len;
		else
			return (*numsSize)+1;//找不到，返回最大值，标志着找不到
	}
}

int middle(const int* target,const int* nums,const int* numsSize,int lef,int rig,int mid_index)
{
	int sum=nums[mid_index],i=mid_index-1,j=mid_index+1,len=1;
	while((i>=lef || j<=rig)&&(sum<*target) ){//只要有一个合法，就继续循环
		if(i<lef && j<=rig){//i非法但是j合法
			sum+=nums[j];j++;len++;
		}else if(j>rig && i>=lef){//j非法但是i合法
			sum+=nums[i];i--;len++;
		}else if(i>=lef && j<=rig){//i,j都合法
			if(nums[i]>nums[j]){
				sum+=nums[i];i--;len++;
			}else if(nums[j]>=nums[i]){
				sum+=nums[j];j++;len++;
			}
		}
	}
	if(sum>=*target)
		return len;
	else
		return (*numsSize)+1;//找不到，返回最大值+1，标志着找不到
}

int Sea_min_s(const int* target,const int* nums,const int* numsSize,int lef,int rig,int mid_index)//分治照字符串
{
	int Len_f=*numsSize,Len_r=*numsSize,Len_m=*numsSize;
	Len_f=left_right(target,nums,numsSize,lef,mid_index-1);//
	Len_r=left_right(target,nums,numsSize,mid_index+1,rig);
	Len_m=middle(target,nums,numsSize,lef,rig,mid_index);

	return min(&Len_f,&Len_r,&Len_m,numsSize);
}

int minSubArrayLen(int target, int* nums, int numsSize){
	int mid_index=numsSize/2,ret=0;
	ret=Sea_min_s(&target,nums,&numsSize,0,numsSize-1,mid_index);
	return ret>numsSize?0:ret;//不存在target是全局变量了，所以要直接传
}

int main(int argc,char* argv[]){
	int target,arr[num]={2,3,1,2,4,3},numsSize=sizeof(arr)/sizeof(int);
	scanf("%d",&target);
	printf("%d",minSubArrayLen(target,arr,numsSize));
}

