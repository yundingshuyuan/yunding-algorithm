class Solution {
    //快速排序
    public int[] sortArray(int[] nums) {
        qsort(nums, 0, nums.length - 1);
        return nums;
    }
    public void qsort(int[] nums, int l, int r) {
        if(l > r) return;
        int begin = l, end = r, base = nums[l], tmp;
        while(begin != end) {
            for(; nums[end] >= base && begin < end; --end);//找小于base的数
            for(; nums[begin] <= base && begin < end; ++begin);//找大于base的数
            if(begin < end) {//swap
                tmp = nums[end];
                nums[end] = nums[begin];
                nums[begin] = tmp;
            }
        }
        nums[l] = nums[begin];
        nums[begin] = base;//base归位
        qsort(nums, l, begin - 1);
        qsort(nums, begin + 1, r);
    }
}