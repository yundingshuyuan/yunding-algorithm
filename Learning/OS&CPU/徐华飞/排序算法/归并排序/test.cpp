class Solution {
    //归并排序
    public int[] sortArray(int[] nums) {
        int[] tmp = new int[nums.length]; 
        mergeSort(nums, tmp, 0, nums.length - 1);
        return nums;
    }
    public void mergeSort(int[] nums, int[] tmp, int lef, int rig) {
        if(lef < rig){
            int mid = (lef + rig) >> 1;
            mergeSort(nums, tmp, lef, mid);//排序左侧
            mergeSort(nums, tmp, mid + 1, rig);//排序右侧
            merge(nums, tmp, lef, rig);//合并
        }
    }
    public void merge(int[] nums, int[] tmp, int lef, int rig) {
        if(lef >= rig) return;
        int mid = (lef + rig) >> 1, l = lef, r = mid + 1, idx = lef;
        while(l != mid + 1 && r != rig + 1) {
            if(nums[l] <= nums[r]) 
                tmp[idx++] = nums[l++];
            else 
                tmp[idx++] = nums[r++];
        }
        while(l <= mid) tmp[idx++] = nums[l++];
        while(r <= rig) tmp[idx++] = nums[r++];
        for(int i = lef; i <= rig; ++i) nums[i] = tmp[i];
    }
}