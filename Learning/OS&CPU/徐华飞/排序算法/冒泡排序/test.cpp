class Solution {
    //快速排序
    public int[] sortArray(int[] nums) {
        int len = nums.length;
        for(int i = len; i > 0; --i) {//控制需要遍历元素的个数
            for(int j = 0; j < i - 1; ++j) {
                if(nums[j] > nums[j + 1]) {//升序   
                    int tmp = nums[j + 1];
                    nums[j + 1] = nums[j];
                    nums[j] = tmp;
                }
            }
        }
        return nums;
    }
}