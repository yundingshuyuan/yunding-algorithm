bool hasCycle(struct ListNode *head) {
    int count = 0;
    while (head!=NULL){
        head = head->next;
        count++;
        if (count > 10000)
            return true;
    }
    return false;
}
