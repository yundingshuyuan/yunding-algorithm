#ifndef __CIRCLE__LIST__
#define __CIRCLE__LIST__
#include <stdbool.h>
#include <stddef.h>

typedef struct ListNode{
    int date;
    struct ListNode* next;
} LNode;

typedef struct ListRecord{
    LNode* tail;
    int count;
} LRecord;


void Insert_CircleList(LNode** phead, int value, int position);

LNode* CreatNode(LNode* pcur,int value);

void Listprint(LNode* phead);
#endif
