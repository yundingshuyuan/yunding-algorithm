#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <stdbool.h>
typedef struct queue {
	int* pBase;
	int front;
	int rear;
	int max;
	int curSize;
}QUEUE;

QUEUE* InitQueue(QUEUE* pQ, int value) {
	pQ = (QUEUE*)malloc(sizeof(QUEUE));
	pQ->pBase = (int*)malloc(sizeof(int)*value);
	pQ->front = 0;
	pQ->rear = 0;
	pQ->max = value;
	pQ->curSize = 0;
	return pQ;
}

bool IsFull(QUEUE* _Q) {
	if (_Q->curSize == _Q->max)
		return true;
	else 
		return false;
}

bool IsEmpty(QUEUE* _Q) {
	if (_Q->curSize == 0)
		return true;
	else 
		return false;
}

bool EnQueue(QUEUE** ppQ, int value) {
	if (IsFull(*ppQ))
		return false;
	(*ppQ)->pBase[(*ppQ)->rear] = value;
	(*ppQ)->rear++;
	(*ppQ)->rear %= (*ppQ)->max;
	(*ppQ)->curSize++;
	return true;
}

bool DeQueue(QUEUE** ppQ, int* value) {
	if (IsEmpty(*ppQ)) 
		return false;
	*value = (*ppQ)->pBase[(*ppQ)->front];
	(*ppQ)->front++;
	(*ppQ)->front %= (*ppQ)->max;
	(*ppQ)->curSize--;
	return true;
}

int main() {
	QUEUE* Q = NULL;
	int ret;
	Q = InitQueue(Q,5);
	for (int i = 0; EnQueue(&Q, i); i++);
	for (;DeQueue(&Q, &ret);) {
		printf("%d\n",ret);
	}
	return 0;
}
