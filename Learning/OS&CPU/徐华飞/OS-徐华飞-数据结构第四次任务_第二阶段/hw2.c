#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
int  compareFunc(const void *a,const void *b) {
	return *(int *)b-*(int *)a;
}
int* deckRevealedIncreasing(int* deck, int deckSize, int* returnSize) {
	qsort(deck,deckSize,sizeof(int),compareFunc);
	int index = 0, front = 0, rear = 0;
	*returnSize =deckSize;
	int* returnNum= (int*)malloc(sizeof(int)*deckSize);
	returnNum[front] = deck[index++];
	do{
		rear = (rear+1)%deckSize;
		returnNum[rear] = returnNum[front]; //置底
		front = (front+1)%deckSize;
		rear = (rear+1)%deckSize;
		if (index < deckSize)
			returnNum[rear] = deck[index++]; //入队
	}while(front != 0);
	int ret = returnNum[deckSize-1];
	for (int i = deckSize-2; i >= 0; i--) {
		returnNum[i+1] = returnNum[i];
	}
	returnNum[0] = ret;
	int t,N = deckSize;
	for(int i=0;i<N/2;i++)
	{
		t = returnNum[i];
		returnNum[i] = returnNum[N-1-i];
		returnNum[N-1-i] = t;
	}
	return returnNum;
}
int main() {
	int deck[7] = {17,13,11,2,3,5,7};
	int* p = NULL;
	p =deckRevealedIncreasing(deck,7,p);
	for (int i=0;i<7;i++){
		printf("%d ",p[i]);
	}
}
