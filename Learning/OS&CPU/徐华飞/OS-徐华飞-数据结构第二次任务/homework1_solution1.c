#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#define N 2
void sys_err(char* str){
	perror(str);
}

int SeaDichotomy(int* nums, int lef, int rig, int target){
    int mid = (lef+rig)/2;
    if ( target == nums[mid] ){
        return mid;
    }
    if ( lef >= rig)
        return -1;
    else if ( target > nums[mid] ){//目标大于中间数值
        return SeaDichotomy(nums, mid+1, rig, target);
    }
    else if ( target < nums[mid] ){//目标小于中间数值
        return SeaDichotomy(nums, lef, mid-1, target);
    }
    return 1;
}

int search(int* nums, int numsSize, int target){
    if(!numsSize)//空
        return -1;
    return SeaDichotomy(nums,0,numsSize-1,target);
}

int main(int argc,char* argv[]){
    int nums[N] = {2,5}, target, ret;
    scanf("%d",&target);
    ret = search(nums, sizeof(nums)/sizeof(int), target);
    printf("%d",ret);
}
