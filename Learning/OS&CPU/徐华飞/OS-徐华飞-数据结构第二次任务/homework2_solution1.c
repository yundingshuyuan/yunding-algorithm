#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
void sys_err(char* str){
    perror(str);
}

struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* removeNthFromEnd(struct ListNode* head, int n){
    int sz=1,count = 0;
    struct ListNode* cur = head,*tmp = NULL;
    while ( cur->next != NULL){
        cur = cur->next;
        sz++;
    }
    if (sz == 1)
        return NULL;
    if (n == sz )
        return head->next; 
    cur = head;
    while ( count < (sz-n-1)){//确定倒数第n+1
        cur = cur->next;
        count++;
    }
    tmp = cur->next;//找到第倒数第n个
    cur->next = tmp->next;
    return head;
}

int main(int argc,char* argv[]){
    /*struct ListNode N5 = {5, NULL};
    struct ListNode N4 = {4, &N5};
    struct ListNode N3 = {3, &N4};
    */struct ListNode N2 = {2, NULL};
    struct ListNode N1 = {1, &N2};
    struct ListNode* head = &N1;
    struct ListNode* cur= &N1;
    int n;
    scanf("%d",&n);
    /*while (cur != NULL){
        printf("%d ",cur->val);
        cur = cur->next;
    }*/
    cur = removeNthFromEnd(head, n);
    //printf("\n");
    while (cur != NULL){
        printf("%d ",cur->val);
        cur = cur->next;
    }
}
