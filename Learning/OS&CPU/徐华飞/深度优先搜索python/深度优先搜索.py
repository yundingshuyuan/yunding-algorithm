maze = [[2, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 1, 0]] #初始化迷宫

maxCol,maxRaw = len(maze[0]),len(maze) 

CurPosition = (0,0) #现在的坐标

NextPosition = [[0,0]] #记录下一个坐标

#global flag 
flag = False #NextPosition的位置是否走得通

step =[ [[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1]], #记录每个位置是由那个坐标达到的
        [[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1]],
        [[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1]],
        [[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1]],
        [[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1]], ]

def VisitNextPosition(NextRaw, NextCol, NowPosition):
    x,y = NowPosition #x表示raw，y表col
    maze[NextRaw][NextCol] = 2 #将下一位置在maze上标记
    NextPosition.append([NextRaw,NextCol]) #存储下一位置
    step[NextRaw][NextCol] = [x,y] #记录如何达到下一位置

def Juge():
    global flag
    #if not flag: #flag为False，改为True
    flag = True

while NextPosition: #下一个位置非空，循环继续
#    global flag
    CurPosition = NextPosition[len(NextPosition)-1] #获取下一个坐标
    raw,col = CurPosition #获取当前位置的列号，行号
    if col == maxCol-1 and raw == maxRaw-1:
        break
    if raw+1 < maxRaw and maze[raw+1][col]==0: #down,当前不是最后一行,下一行对应位置为0
        Juge()
        VisitNextPosition(raw+1, col, CurPosition)
    if col+1 < maxCol and maze[raw][col+1]==0: #right   
        Juge()
        VisitNextPosition(raw, col+1, CurPosition)
    if col-1 >= 0 and maze[raw][col-1]==0: #left   
        Juge()
        VisitNextPosition(raw, col-1, CurPosition)
    if raw-1 >= 0 and maze[raw-1][col]==0: #up
        Juge()
        VisitNextPosition(raw-1, col, CurPosition)
    if not flag:
        NextPosition.pop() #删除NextPosition中的最后一个元素，因为该路径走不同

if not NextPosition:
    print('no path')
else:
    x,y = step[maxRaw-1][maxCol-1][0],step[maxRaw-1][maxCol-1][1]
    while x != -1:
        print((x,y))
        x,y = step[x][y][0],step[x][y][1]



