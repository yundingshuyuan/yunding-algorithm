#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
void sys_err(char* str){
    perror(str);
}

struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* reverseList(struct ListNode* head){
    struct ListNode* cur = head;
    struct ListNode* parr[5001] = {head};
    int index = 0;
    if (head == NULL ){//空链表
        return NULL
    }
    while (cur->next != NULL){//存在下一个节点，记录所有节点的地址
        parr[++index] = cur->next;
        cur = cur->next;
    }
    head = cur;//记录反转后的头指针地址
    while (index >= 0){//将每个节点的next指针赋值为前一个节点的地址
        cur->next = parr[--index];
        cur = cur->next;
    }
    cur->next = NULL;//将最后一个节点的next赋值为NULL
    return head;
}

void ShowList(struct ListNode* phead){//打印链表函数，直接copy上次作业的
    struct ListNode* cur = phead;
    if (cur == NULL)
        printf("空链表");
    else{
        while (cur->next != NULL )//不是尾节点，打印；尾节点，结束打印
        {
            printf("%d ",cur->val);
            cur = cur->next;
        }   printf("%d\n",cur->val);//打印尾节点内部的内容,顺便还行
    }
}

int main(int argc,char* argv[]){
    struct ListNode N5 = {5, NULL};
    struct ListNode N4 = {4, &N5};
    struct ListNode N3 = {3, &N4};
    struct ListNode N2 = {2, &N3};
    struct ListNode N1 = {1, &N2};
    struct ListNode* head = &N1;
    struct ListNode* cur= &N1;
    head = reverseList(head);
    ShowList(head);
}
