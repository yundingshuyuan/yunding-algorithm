/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
bool hasCycle(struct ListNode *head) {
    if(head == NULL){
        return false;
    }
    struct ListNode* ss = head;
    struct ListNode* sp = head->next;
    if(sp == NULL||sp->next == NULL){
        return false;
    }
    while(1){
        if(ss == sp){
            return true;
        }
        ss = ss->next;
        sp = sp->next->next;
        if(ss == NULL||sp == NULL||ss->next == NULL||sp->next == NULL){
            return false;
        }
    }
}
