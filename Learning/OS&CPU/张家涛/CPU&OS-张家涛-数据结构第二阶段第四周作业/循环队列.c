#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<malloc.h>

typedef struct Queue
{
	int front;
	int rear;
	int *str;
}Queue;

//初始化队列
bool Queue_Init(Queue* q);
//压入队列
void Queue_Push(Queue* q, int x);
//压出队列
void Queue_Pop(Queue* q);
//查询队列首
void Queue_Pike(Queue* q);
//输出队列
void Queue_Print(Queue* q);
//查看队列是否为空
bool Queue_Empty(Queue* q);
//查看队列是否为满
bool Queue_Full(Queue* q);
//清空队列
void Queue_Clear(Queue* q);
//销毁队列
void Queue_Destroy(Queue* q);

int main()
{
	int length = 6;
	Queue Q;
	Queue_Init(&Q);
	Queue_Print(&Q);
	Queue_Push(&Q, 1);
	Queue_Push(&Q, 2);
	Queue_Push(&Q, 3);
	Queue_Push(&Q, 4);
	Queue_Push(&Q, 5);
	Queue_Push(&Q, 6);
	Queue_Push(&Q, 7);
	//printf("111\n");
	Queue_Print(&Q);
	Queue_Pop(&Q);
	Queue_Print(&Q);

	Queue_Clear(&Q);
	Queue_Print(&Q);
	Queue_Push(&Q, 8);
	Queue_Push(&Q, 9);
	Queue_Push(&Q, 10);
	Queue_Push(&Q, 11);
	Queue_Push(&Q, 12);
	Queue_Push(&Q, 13);
	Queue_Print(&Q);
	Queue_Pike(&Q);
	Queue_Pop(&Q);
	Queue_Pike(&Q);

	Queue_Destroy(&Q);
	return 0;
}

bool Queue_Init(Queue* q)
{
	int i = 0;
	q->str = (int*)malloc(sizeof(int) * 6);
	if (q->str != NULL)
	{
		for (i = 0; i < 6; i++) {
			q->str[i] = -1;
		}
		q->front = 0;
		q->rear = 0;
		return true;
	}
	else {
		printf("内存分配失败！\n");
		return false;
	}
}

void Queue_Push(Queue* q, int x)
{
	if (Queue_Full(q))
	{
		printf("the queue is full!\n");
		return;
	}
	else
	{
		q->str[q->rear] = x;
		q->rear = (q->rear + 1) % 6;
		printf("the push number is %d\n", x);
	}
}

void Queue_Pop(Queue* q)
{
	if (Queue_Empty(q))
	{
		return;
	}
	printf("Pop Node is %d\n", q->str[q->front]);
	q->front += 1;
}

void Queue_Pike(Queue* q)
{
	printf("the first node is %d\n ", q->str[q->front]);
}

void Queue_Print(Queue* q)
{
	int i = 0;
	int j = 0;
	int s = 0;
	//printf("0000\n");
	if (Queue_Empty(q))
	{
		//printf("11111111\n");
		printf("The line is empty!\n");
		return;
	}
	if (Queue_Full(q))
	{
		s = 1;
	}
	for (i = q->front; i != q->rear+s; i++)
	{
		s = 0;
		printf("the %d number is %d\n", j++, q->str[i]);
		if (i == 5)
		{
			i = -1;
		}
		if (i+1 == q->rear + s)
		{
			printf("over\n");
		}
		
	}
}

void Queue_Clear(Queue* q)
{
	int i = 0;
	q->front = 0;
	q->rear = q->front;
	for (i = 0; i < 6; i++)
	{
		q->str[i] = -1;
	}
	printf("Clear success!\n");
}

void Queue_Destroy(Queue* q)
{
	free(q->str);
	q->front = q->rear = 0;
	printf("the queue is destroied\n");
}

bool Queue_Full(Queue* q)
{
	if (q->front == q->rear && q->str[q->front] != -1)
	{
		return true;
	}
	else
	{
		return false;
	}
}


bool Queue_Empty(Queue* q)
{
	//printf("empty！\n");
	if (q->front == q->rear && q->str[q->front] == -1)
	{
		return true;
	}
	else
	{
		return false;
	}
}