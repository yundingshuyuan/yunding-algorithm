/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* deckRevealedIncreasing(int* deck, int deckSize, int* returnSize) {
    int i = 0, j = 0;
    int* num = (int*)malloc(sizeof(int) * deckSize);
    *returnSize = deckSize;
    for (i = 0; i < deckSize; i++)
    {
        for (j = 0; j < deckSize - i - 1; j++)
        {
            if (deck[j] > deck[j + 1])
            {
                int tmp = deck[j + 1];
                deck[j + 1] = deck[j];
                deck[j] = tmp;
            }
        }
    }
    for (i = deckSize - 1, j = deckSize - 1; i >= 0; i--, j--)
    {
        int x = deck[i];
        if (i == (deckSize - 1) || (deckSize - 2) == i)
        {
        }
        else
        {
            int s = 0;
            int l = num[deckSize - 1];
            for (s = deckSize - 1; s > j + 1; s--)
            {
                num[s] = num[s - 1];
            }
            num[j + 1] = l;
        }
        num[j] = x;
    }
    for (i = 0; i < deckSize; i++)
    {
        printf(" %d ", num[i]);
    }


    return num;
}