#include<stdio.h>
#include<malloc.h>
#include<stdbool.h>

typedef struct LinkNode
{
    int val;
    struct LinkNode* next;
}LinkNode;

typedef struct Queue
{
    LinkNode* front;
    LinkNode* rear;
}Queue;

int i = 0;
//初始化队列 
void Queue_Create(Queue* q);
//队列的进入 
void Queue_Push(Queue* q, int x);
//队列的出 
void Queue_Pop(Queue* q);
//输出队列的首个 
void Queue_Peek(Queue* q);
//判断队列是否为空 
bool Queue_Empty(Queue* q);
//打印队列 
void Queue_Print(Queue* q);
//销毁队列 
void Queue_Clear(Queue* q);
//销毁队列 
void Queue_Destroy(Queue* q);

int main()
{
    printf("/////\n");
    Queue* Q = (Queue*)malloc(sizeof(Queue));
    printf("******\n");
    Queue_Create(Q);
    //printf("/////\n");
    Queue_Push(Q, 1);
    Queue_Push(Q, 2);
    Queue_Push(Q, 3);
    Queue_Push(Q, 4);
    Queue_Push(Q, 5);
    Queue_Print(Q);
//    Queue_Print(Q);

    Queue_Pop(Q);
    Queue_Print(Q);
    Queue_Peek(Q);

    Queue_Clear(Q);
    if (Queue_Empty(Q))
    {
        printf("the line is clear!\n");
    }
    else
    {
        printf("not success!\n");
    }

    Queue_Push(Q, 6);
    Queue_Push(Q, 7);
    Queue_Push(Q, 8);
    Queue_Push(Q, 9);
    Queue_Peek(Q);
    Queue_Print(Q);

    Queue_Destroy(Q);
    return 0;
}

void Queue_Create(Queue* q)
{
    printf("1");
    //q = (Queue*)malloc(sizeof(Queue));
    q->front = (LinkNode*)malloc(sizeof(LinkNode));
    q->front->next = NULL;
    printf("2");
    q->rear = q->front;
    printf("3");
    q->front->next = NULL;
    printf("4");
    printf("111\n");
}

void Queue_Push(Queue* q, int x)
{
    printf("1");
    LinkNode* Node = (LinkNode*)malloc(sizeof(LinkNode));
    Node->next = NULL;
    printf("2");
    q->rear->val = x;
    printf("3");
    printf("x = %d\n", x);
    printf("4");
    q->rear->next = Node;
    printf("5");
    q->rear = q->rear->next;
    printf("6\n");
}

void Queue_Pop(Queue* q)
{
    int x;
    i++;
    LinkNode* Node = q->front;
    x = Node->val;
    q->front = Node->next;
    printf("the %d Node is %d.\n", i, x);
    free(Node);
    Node = NULL;
}

void Queue_Peek(Queue* q)
{
    int x;
    LinkNode* k = q->front;
    x = k->val;
    printf("the first node number is %d.\n", x);
}

bool Queue_Empty(Queue* q)
{
    if (q->front == q->rear)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Queue_Print(Queue* q)
{
    LinkNode* ll = q->front;
    int c = 0;
    while (ll->next != NULL)
    {
        int x = 0;
        c++;
        x = ll->val;
        printf("the line %d location is %d.\n", c, x);
        ll = ll->next;
    }
}

void Queue_Clear(Queue* q)
{
    while (q->front != q->rear)
    {
        LinkNode* Node = (LinkNode*)malloc(sizeof(LinkNode));
        Node = q->front;
        q->front = q->front->next;
        free(Node);
        Node = NULL;
    }
}

void Queue_Destroy(Queue* q)
{
    Queue_Clear(q);
    free(q->front);
    q->front = NULL;
    printf("Destroy is successful!");
}
