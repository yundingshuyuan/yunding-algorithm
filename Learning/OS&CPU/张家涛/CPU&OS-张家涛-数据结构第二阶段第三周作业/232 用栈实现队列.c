typedef struct LinkNode
{
    int val;
    struct LinkNode* next;
}LinkNode;

typedef struct Stack
{
    LinkNode* top;
    LinkNode* bottom;
}Stack;

Stack* Stack_Create()
{
    Stack * sf =(Stack*)malloc(sizeof(Stack));
    sf->top = (LinkNode*)malloc(sizeof(LinkNode));
    sf->bottom = sf->top;
    sf->bottom->next = NULL; 
    return sf;
}

void Stack_Push(Stack * sa,int x)
{
    LinkNode* Node = (LinkNode*)malloc(sizeof(LinkNode));
    Node->val = x;
    printf("%d",x);
    Node->next = sa->top;
    sa->top = Node;
}

int Stack_Pop(Stack* sp)
{
    // printf(" ddd = %p ",sp);
    LinkNode * Node = (LinkNode*)malloc(sizeof(LinkNode));
    Node = sp->top;
    // printf("top ret = %d",sp->top->val);
    sp->top = sp->top->next;
    int ret = Node->val;
    // printf(" Pop_S_ret = %d " ,ret);
    free(Node);
    Node = NULL;
    return ret;
}

bool Stack_Empty(Stack* sg)
{
    if(sg->top == sg->bottom)
    {
        return true;
    }else{
        return false;
    }
}

void Stack_Change(Stack* sh,Stack* si)
{
    int con = 0;
    // int ccc = sh->top->val;
    // printf(" 1111 = %p " ,sh);
    // printf(" 2222 = %p " ,si);
    // printf(" ccc = %d",ccc);
    while(1)
    {
        // printf("************");
        // printf(" aaaaa = %p " ,sh);
        if(Stack_Empty(sh)){
            break;
        }
        // printf(" xxxxx = %p " ,sh);
        con = Stack_Pop(sh);
        // printf(" con = %d ",con);
        Stack_Push(si,con);
    }
}

void Stack_Free(Stack* sy)
{
    while(sy->top != NULL)
    {
        LinkNode* Node = (LinkNode*)malloc(sizeof(LinkNode));
        Node = sy->top;
        sy->top = sy->top->next;
        free(Node);
        Node = NULL;
    }
}

typedef struct MyQueue
{
    Stack* s_in;
    Stack* s_out;
}MyQueue;

MyQueue* myQueueCreate() 
{
    MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));
    obj->s_in = Stack_Create();
    obj->s_out = Stack_Create();
    // printf(" 777 = %p ",obj->s_out);
    return obj;
}
// int count = 0;
void myQueuePush(MyQueue* obj, int x) {
    Stack_Push(obj->s_in,x);
    // printf("count = %d ",count);
    // count++;
}

int myQueuePop(MyQueue* obj) {
    int ret = 0;
    Stack_Change(obj->s_in,obj->s_out);
    ret = Stack_Pop(obj->s_out);
    // printf(" Pop_ret = %d ",ret);
    Stack_Change(obj->s_out,obj->s_in);
    return ret;
}

int myQueuePeek(MyQueue* obj) {
    int ret = 0;
    Stack_Change(obj->s_in,obj->s_out);
    ret = Stack_Pop(obj->s_out);
    Stack_Push(obj->s_out,ret);
    Stack_Change(obj->s_out,obj->s_in);
    // printf(" Peek_ret = %d " ,ret);
    return ret;
}

bool myQueueEmpty(MyQueue* obj) {
    if(Stack_Empty(obj->s_in))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void myQueueFree(MyQueue* obj) {
    Stack_Free(&obj->s_in);
}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);
 
 * int param_2 = myQueuePop(obj);
 
 * int param_3 = myQueuePeek(obj);
 
 * bool param_4 = myQueueEmpty(obj);
 
 * myQueueFree(obj);
*/
