bool isValid(char * s){
    int length = strlen(s);
    int ban = length % 2;
    int can = length / 2;
    int i = 0,j = 0;
    if(ban == 1){
        return false;
    }
    char stack[length];
    for(i = 0;i < length+2;i++){
        if(s[i] == '\0' && j == 0){
            return true;
        }
        if(s[i] == '(' || s[i] == '{' || s[i] == '['){
            stack[j] = s[i];
            j++;
        }else{
            if(j == 0){
                return false;
            }
            j--;
            if(s[i] == ')' && stack[j] == '('){
                continue;
            }else if(s[i] == '}' && stack[j] == '{'){
                continue;
            }else if(s[i] == ']' && stack[j] == '['){
                continue;
            }else{
                return false;
            }
        }
    }
    return true;
}
