#include<stdio.h>
#include<stdlib.h>

typedef struct LinkNode{
    int val;
    struct LinkNode* next;
}LinkNode;

typedef struct Stack{
    LinkNode* STop;
    LinkNode* SBottom;
}Stack;

int init_stack(Stack *s);//��ʼ��ջ
int push_stack(Stack* s,int value);//ѹջ
int take_stack(Stack* s);//��ջ
int print_stack(Stack* s);//��ӡջ
int clear_stack(Stack* s);//���ջ

int main(){
    Stack s;
    init_stack(&s);
    push_stack(&s,1);
    push_stack(&s,2);
    push_stack(&s,3);
    push_stack(&s,4);
    push_stack(&s,5);
    push_stack(&s,6);
    push_stack(&s,7);
    print_stack(&s);
    take_stack(&s);
    print_stack(&s);
    take_stack(&s);
    print_stack(&s);
    clear_stack(&s);
    print_stack(&s);
    return 0;
}

int init_stack(Stack* s)
{
    s->STop = (LinkNode*) malloc(sizeof(LinkNode));
    s->SBottom = s->STop;
    s->SBottom->next = NULL;
    return 0;
}

int print_stack(Stack* s)
{
    LinkNode * hl = s->STop;
    int i = 1;
    while(hl != s->SBottom){
        printf("the %d node value is %d\n",i,hl->val);
        hl = hl->next;
        i++;
    }
    printf("***************\n");
    printf("over!\n");
    return 0;
}

int push_stack(Stack* s,int value)
{
    LinkNode* df = (LinkNode*) malloc(sizeof (LinkNode));
    df->val = value;
    df->next = s->STop;
    s->STop = df;
    return 0;
}

int take_stack(Stack* s)
{
    LinkNode* fg = s->STop;
    s->STop = s->STop->next;
    printf("the taken node is %d\n",fg->val);
    free(fg);
    fg = NULL;
    return 0;
}

int clear_stack(Stack* s)
{
    while(s->STop != s->SBottom){
        LinkNode * sd = s->STop;
        s->STop = s->STop->next;
        printf("the taken node is %d\n",sd->val);
        free(sd);
        sd = NULL;
    }
    return 0;
}
