 typedef struct LinkNode
{
    int val;
    struct LinkNode * next;
}LinkNode;

typedef struct Stack
{
    LinkNode* STop;
    LinkNode* SBottom;
}Stack;

//初始化 
int initalize_stack(Stack *s)
{
    s->STop = (LinkNode *)malloc(sizeof(LinkNode));
    s->SBottom = s->STop;
    s->STop->next = NULL;
    return 0;
}

//压入 
int push_stack(Stack *s,int input_value)
{
    LinkNode* sf = (LinkNode*)malloc(sizeof(LinkNode));
    sf->val = input_value;
//    printf("4cahr = %d  ",input_value);
    sf->next = s->STop;
    s->STop = sf;
    return 0;
}

//压出 
int take_stack(Stack* s)
{
    LinkNode* sg = s->STop;
    // s->STop = s->STop->next;
    if(s->STop != NULL){
    int out_value = sg->val;
    s->STop = s->STop->next;
    free(sg);
    sg = NULL;
    return out_value;
    }
    return 0;
    // s->STop = s->STop->next; 
    
}

//判断数据长度的栈是否为空 
int just_stack(Stack * f){
    if(f->STop == f->SBottom){
        return 1;
    }else{
        return 0;
    }
}

int evalRPN(char ** tokens, int tokensSize)
{
//    printf("222222222");
    // char ** ml;
    // ml = tokens;
    int i = 0;
    int j = 0;
    // int l = 0;
    int twol;
    int tial = 0;
    int count = 0;
    Stack s;
    Stack f;
    initalize_stack(&f);
    initalize_stack(&s);
//    printf(".......");
    // for(i = 0;i < tokensSize; i++){
    //     for(j = 0;j<4;j++){
    //         if(ml[i][j] != '\0'){
    //         // printf("char = %d ",ml[i][j]);
    //         }else{
    //             break;
    //         }
    //     }
        // tokens++;
        // **tokens = **tokens +1 ;
    // }
    for(i = 0; i < tokensSize; i++){
    	//tial是一个判断数据，为1时表示该部分为正数，为-1时表示该部分为负数，为零时表示符号位 
        tial = 0;
        count = 0;
        if(tokens[i][0] != '+' && tokens[i][0] != '*' && tokens[i][0] != '/'){
            tial = 1;
        for(j = 0; j < 5; j++){
            if(tokens[i][0] == '-' && tokens[i][1] == '\0'){
                tial = 0;
                break;
            }else if(tokens[i][0] == '-'){
                tial = -1;
            }else{
                tial = 1;
            }
            if(tokens[i][j] != '\0'){
//                printf("xxxxx");
                
            // printf("%d ",tokensSize);
//                printf("1char = %c ",tokens[i][j]);
//                printf("2char = %d ",tokens[i][j]);
//                printf("3char = %d ",tokens[i][j] - '0');
                if(j == 0 && tial == -1){
                    continue;
                }
                push_stack(&s,tokens[i][j]-'0');
                //printf("i = %d ",i);
                //printf("j = %d ",j);
            }else{
            	//判断数据是否为负 
                if(tial == -1){
                    twol = j + 9;
                }else{
                    twol = j;
                }
//                printf("twol = %d " ,twol); 
                push_stack(&f,twol);
                // tokens++;
                break;
            }
            // printf("%d",l);
        }
        } 
        // {
        //     printf("%c",**tokens);
        //     if(**tokens == '+'){
        //         printf("√");
        //     }
        // }
//        printf("i =  %d   ",i);
		//当且仅当只有符号时进入 
        if((tokens[i][0] == '+'||tokens[i][0] == '*' ||tokens[i][0] == '-'|| tokens[i][0] == '/') && tial == 0){
//            printf("******* ");
            int x = 0;
            int y = 0;
            int m = 0;
            int s1 = 0;
            int z = 0;
            int d = 0;
            s1 = take_stack(&f);
            if(s1 > 10){
            	//长度大于10的时候数据为负，因为题已经说明数据为[-200,200]之间 
                s1 -= 10;
                for(d = 0; d <s1;d++){
                z = take_stack(&s);
                y += z*pow(10,d);
                }
                y = 0- y;
            }else{
                for(d = 0; d <s1;d++){
                z = take_stack(&s);
                y += z*pow(10,d);
                }
            }
            int s2 = 0;
            s2 = take_stack(&f);
            if(s2 > 10){
                s2 -= 10;
                for(d = 0; d < s2;d++){
                z = take_stack(&s);
                x += z*pow(10,d);
                }
                x = 0 - x;
            }else{
                for(d = 0; d <s2;d++){
                z = take_stack(&s);
                x += z*pow(10,d);
                }
            }
            //printf("x = %d ",x);
            //printf("y = %d ",y);
            if(tokens[i][0] == '+'){
                //printf(" 11111 ");
                m = x + y;
                push_stack(&s,m);
            }else if(tokens[i][0] == '-'){
                //printf(" 22222 ");
                m = x - y;
                push_stack(&s,m);
            }else if(tokens[i][0] == '*'){
                //printf(" 33333 ");
                m = x * y;
                push_stack(&s,m);
            }else if(tokens[i][0] == '/') {
                //printf(" 44444 ");
                m = x / y;
                push_stack(&s,m);
            }
            push_stack(&f,1);
        }
    }
    //先判断数据长度的栈是否为空，如果为空的话说明**tokens为空 
    if(just_stack(&f)){
        //printf("usefui");
        return take_stack(&s);
    }else{
    	//反之，进入到这里说明**tokens不为空且剩余一个未使用或者是刚刚压入到栈中的结果 
        int e = take_stack(&f);
        int d = 0;
        int w = 0;
        int o = 0;
        if(e > 10){
            e -= 10;
            for(d = 0; d <e;d++){
            o = take_stack(&s);
            w += o*pow(10,d);
            }
            //printf(" 77777 ");
            w = 0- w;
        }else{
            for(d = 0; d <e;d++){
            o = take_stack(&s);
            //printf(" 5555555 ");
            w += o*pow(10,d);
            }
        }
        return w;
    }
    
    // int i = 0;
    // printf("%d",(int)**tokens);
    // tokens++;
    // printf("%d",(int)**tokens);
    // tokens++;
    // // printf("%d",(int)**tokens);
    // // tokens++;
    // for(i = 0;i < tokensSize; i++){
    // printf("%c",**tokens);
    // tokens++;
    // }
}
