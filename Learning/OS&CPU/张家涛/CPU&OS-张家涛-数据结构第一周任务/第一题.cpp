int Sit(int a1,int a2,int *x,int mb,int l,int *s,int *k,int size)
{
    if(a1>a2){//左大于右 
        *x-=(l+*s);
        mb+=a1;
        if(*x>1){
            *k=1;
        }else{
            *k=4;
        }
    }else if(a1==a2){//左右相等 
        *k=2;
        (*s)++;
        mb+=a1;
    }else if(a1<a2){//右大于左 
        *x+=(l+*s);
        mb+=a2;
        if(*x<size){
            *k=3;
        }else{
            *k=5;
        }
    }
    return mb;
}

int minSubArrayLen(int target, int* nums, int numsSize){
    int i,mb,l=0,*s,*x,*k,size;
    int a=0;
    size=numsSize;
    k=&a;
    x=&a;
    s=&a;
    int most=nums[0],mi=0;
    //求得数组当中的最大值 
    for(i=1;i<numsSize;i++){
        if(nums[i]>most){
            most=nums[i];
            mi=i;
        }
    }
    //如果最大的是第一个 
    if(mi==0){
        mb=nums[0];
        for(i=1;i<numsSize;i++){
            if(mb>=target) {
                break;
            }
            mb+=nums[i];
        }
    }else if(mi==numsSize){//如果最大的是最后一个 
        mb=nums[numsSize];
        for(i=numsSize-1;i>=0;i--){
            if(mb>=target){
                break;
            }
            mb+=nums[i];
        }
    }else{//从中间往两侧选择 
        x=&mi;
        mb=nums[mi];
        for(i=0;i<numsSize-1;i++){
            if(mb>=target){
                break;
            }else{
                l++;
                switch(*k){
                    case 0://第一次作比较 
                        mb=Sit(nums[*x-1],nums[*x+1],x,mb,l,s,k,size);
                        break;
                    case 1://选择左边 
                        mb=Sit(nums[*x-1],nums[*x+1+l+*s],x,mb,l,s,k,size);
                        break;
                    case 2://左右相等
                        mb=Sit(nums[*x-*s-l],nums[*x+*s+l],x,mb,l,s,k,size);
                        break;
                    case 3://选择右边 
                        mb=Sit(nums[*x-1-l-*s],nums[*x+1],x,mb,l,s,k,size);
                        break;
                    case 4://选择到了最左边 
                        for(int j=(*x+1+l+*s);j<numsSize;j++){
                            if(mb>=target){
                                break;
                            }
                            mb+=nums[j];
                        }
                        break;
                    case 5://选择到了最右边 
                        for(int q=(*x-1-l-*s);q>=0;q--){
                            if(mb>=target){
                                break;
                            }
                            mb+=nums[q];
                        }
                }
            }
        }
    }if(mb>=target) {//输出值 
        printf("%d", i + 1);
    }else{
        printf("%d",0);
    }
    return 0;
}
