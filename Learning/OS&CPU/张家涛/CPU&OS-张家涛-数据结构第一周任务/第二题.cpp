/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* sortedSquares(int* nums, int numsSize, int* returnSize){
    *returnSize=numsSize;
    int s;
    int i,k;
    for( i=0;i<numsSize;i++){
        nums[i]*=nums[i];
    }
    for(k=0;k<numsSize;k++){
        for(int m=k+1;m<numsSize;m++){
            if(nums[k]>nums[m]){
                s=nums[k];
                nums[k]=nums[m];
                nums[m]=s;
            }
        }
    }
    printf("[");
    for(i=0;i<numsSize;i++){
        printf("%d",nums[i]);
        if(i!=(numsSize-1)){
            printf(",");
        }
    }
    printf("]\n");
    return 0;
}



