/*
	file name:num_min.c
	file object:找到最小的输出
	created time: 2022-03-15
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<unistd.h>
#include<pthread.h>


void sys_err(char* str){
	perror(str);
	exit(1);
}

int main(int argc,char* argv[]){
	int target=0,i=0,j=0,k=0,sum=0,len=6;
	int nums[20];
	printf("请输入目标数字大小:	");
	scanf("%d",&target);
	printf("请输入数组中的元素（按q停止输入）：");
	for(i=0;scanf("%d",&nums[i])==1;i++){
		getchar();
		len++;
	}
	for(i=1;i<=len;i++){		//决定以几个元素为一个组合
		for(k=0;k<=len-i;k++){		//推动元素前进（改变元素的内部）
			sum=0;
			for(j=k;j<k+i;j++){
				sum+=nums[j];
			}
			if(sum >=target){
				printf("结果为%d\n",i);
				return 0;
			}
		}
	}
	printf("你可真是个大聪明,结果为0.\n");
	return 0;
}
