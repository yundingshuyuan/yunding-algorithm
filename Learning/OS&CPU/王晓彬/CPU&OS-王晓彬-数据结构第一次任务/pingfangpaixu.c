/*
	file name:pingfangpaixu.c
	file object:
	created time: 2022-03-15
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<unistd.h>
#include<pthread.h>


void sys_err(char* str){
	perror(str);
	exit(1);
}

int main(int argc,char* argv[]){
	int nums[20],i=0,len=0,k=0;
	printf("请输入数组中的元素（按q停止输入）：");
	for(i=0;scanf("%d",&nums[i])==1;i++){
		getchar();
		len++;
	}
	for(i=0;i<len;i++){
		nums[i]=nums[i]*nums[i];
	}
 	for(i=len-2;i>=0;i--){
		for(k=0;k<=i;k++){
			if(nums[k]>=nums[k+1]){
				int count=0;
				count=nums[k];
				nums[k]=nums[k+1];
				nums[k+1]=count;
			}
		}
	}
	printf("输出为下：\n");
	for(i=0;i<len;i++){
		printf("%d  ",nums[i]);
	}
	return 0;
}
