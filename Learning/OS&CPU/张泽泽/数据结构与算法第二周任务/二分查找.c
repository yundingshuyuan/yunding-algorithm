int search(int* nums, int numsSize, int target){
    int left=0;
    int right=numsSize-1;
    int middle=0;
    while(left<=right)
    {
        middle=(left+right)/2;
        if(target>nums[middle])
        {
            left=left+1;
        }
        else if(target<nums[middle])
        {
            right=right-1;
        }
        else if(target==nums[middle])
        {
            return middle;
        }
    }
    return -1;
}