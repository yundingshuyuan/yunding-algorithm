/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* removeNthFromEnd(struct ListNode* head, int n){
    struct ListNode* dummy=malloc(sizeof(struct ListNode));

    dummy->next=head;
    struct ListNode* fast=dummy;
    struct ListNode* slow=dummy;
    while(n--&&fast!=NULL)
    {
        fast=fast->next;
    }
    fast=fast->next;
    while(fast!=NULL)
    {
        fast=fast->next;
        slow=slow->next;
    }
    slow->next=slow->next->next;
    return dummy->next;
}