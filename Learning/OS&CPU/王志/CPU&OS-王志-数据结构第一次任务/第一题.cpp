int minSubArrayLen(int target,int*nums,int numsSize)
{
	int sum=0;
	long ans = INT_MAX;
	int j = 0;
	for(int i=0;i<numSize;i++)
	{
		 sum=nums[i]+sum;
		 while(sum>=target){
		 	ans=fmin(ans,i-j+1);
		 	sum-=nums[j++];
		 }
	}
	return ans ==INT_MAX?0:ans;
 } 
 /*该题在搜索了相关资料后，我确定了解题方法为滑动窗口
 滑动窗口的相关知识：
 维护一个窗口，不断滑动然后更新答案。
 滑动窗口是一个运行在大数组上的子列表，该数组是一个底层元素的集合。
  
