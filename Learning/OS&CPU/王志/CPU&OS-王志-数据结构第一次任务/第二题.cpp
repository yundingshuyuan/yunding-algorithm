int* sortedSquares(int* A, int ASize, int* returnSize){
    short i,j=0,k=ASize-1;
    for(i=0;i<ASize;i++) A[i]=A[i]*A[i];
    int* res=malloc(ASize*sizeof(int));
    while(i--)
        if(A[j]>A[k]) res[i]=A[j++];
        else res[i]=A[k--];
    *returnSize=ASize;
    return res;
}
/*要想实现该项功能，先将原数组中的数进行平方后输入其他数组中，
但是原数组中已经是乱序排序，就没有设计其余环节用于乱序排序，
但是按照完全考虑的话应该设计能够避免升序排序的乱序排序方式*/ 
