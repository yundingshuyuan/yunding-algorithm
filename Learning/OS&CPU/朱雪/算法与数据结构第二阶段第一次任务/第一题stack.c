#include "stack.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
//1.构造一个空栈，并初始化 
int init_stack(my_stack* my_stack){
	//创建一块空间，使my_stack->base指向空间的首地址 
	my_stack->top = -1;
	my_stack->stack[100] = 0;
	return 1;
}
//2. 栈里的元素个数，即栈的大小
int  size_stack(my_stack* my_stack){
	printf("****************\n"); 
	printf("栈的大小:%d\n",(my_stack->top+1));
	printf("****************\n"); 
	return 1;
}
//3.检查栈空
void empty_stack(my_stack* my_stack){
	if( -1 == my_stack->top){
		printf("栈是空的\n"); 
		return ;
	} 

}
//4.检查栈满 
void full_stack(my_stack* my_stack){
	if(99 == my_stack->top){
		printf("栈已经满了\n");
		return ;
	} 
}
//5.入栈操作
int  push_stack(my_stack *my_stack,int number){
      full_stack(my_stack);
      ++my_stack->top;
      my_stack->stack[my_stack->top] = number; 
      printf("****************\n"); 
      printf("%d已经入栈\n",my_stack->stack[my_stack->top]);
      return 1;
}
//6.出栈操作
int pop_stack(my_stack* my_stack){
	  empty_stack(my_stack);
	   printf("****************\n"); 
	  printf("%d已经出栈\n",my_stack->stack[my_stack->top]);
	  my_stack->top--;
	  return 1;
}

//7.清理栈 
int clean_stack(my_stack* my_stack){
	if(NULL == my_stack){
		printf("****************\n"); 
		printf("栈已经清理完毕\n");
	}
	else{
		printf("****************\n"); 
		printf("栈正在清理\n");
		free(my_stack);//清除栈 
		my_stack = NULL;//置空 
	}
	return 1;
}