//顺序栈的基本功能 
	//0.构造一个栈的结构体 
	/*
		typedef struct{
			int base;
			int top;
			int size;
			int stack[];
		}my_stack;
	*/
	#include "stack.h"
	#include "stdio.h"
	#include "string.h"
	#include "stdlib.h"
	/*
	//1.构造一个空栈，并初始化 
	int init_stack(my_stack* my_stack);
	//2.栈里的元素个数，即栈的大小
	int size_stack(my_stack* my_stack);
	//3.检查栈空与满
	int full_stack(my_stack* my_stack);
	//4.入栈操作
	int push_stack(my_stack* my_stack,int number);
	//5.出栈操作
	int pop_stack(my_stack* my_stack);
	//6.清理栈 
	int clean_stack(my_stack* my_stack);
	*/
int main() {
	my_stack my_stack;//定义一个栈 
	init_stack(&my_stack);//初始化栈 
	int i = 0,push_number = 0; 
	int str[stack_long];
	int a = 1;
	printf("请您输入您要往栈里输入的数值个数\n");
	scanf("%d", &push_number);
	for(i = 0; i < push_number;i++){
		str[i] = a++;
		push_stack(&my_stack,str[i]);
	}
	size_stack(&my_stack);
	for(i = 0; i < push_number;i++){
		printf("栈中的第%d个数为：%d\n",(i+1),str[i]);
	}
	clean_stack(&my_stack);
	return 0;
}