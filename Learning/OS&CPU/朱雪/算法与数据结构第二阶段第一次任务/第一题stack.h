//引用在这是因为stack.c与main.c都会用到stack.h，为了避免冲突 
#ifndef STACK_STACK_H 
#define STACK_STACK_H 
#define stack_long 100
#define stack_increase 20
//构建一个保存数组
//0.构造一个栈的结构体 
typedef struct{
	int base;
	int top;
	int size;
	int stack[];
}my_stack;
//1.构造一个空栈，并初始化 
int init_stack(my_stack* my_stack);
//2. 栈里的元素个数，即栈的大小
int size_stack(my_stack* my_stack);
//3.检查栈满 
void full_stack(my_stack* my_stack);
//4.检查栈空
void empty_stack(my_stack* my_stack); 
//5.入栈操作
int push_stack(my_stack* my_stack,int number);
//6.出栈操作
int pop_stack(my_stack* my_stack);
//7.清理栈 
int clean_stack(my_stack* my_stack);
#endif //STACK_STACK_H