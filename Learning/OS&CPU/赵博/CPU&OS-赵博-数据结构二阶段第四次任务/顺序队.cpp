#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

const int MaxSize = 10;

typedef struct Queue {
	int Queue[MaxSize];
	int front;
	int rear;
}SeqQueue;

void InitQueue(SeqQueue* Q) {
	Q->front = Q->rear = 0;
}

int IsEmpty(SeqQueue* Q) {
	if (Q->front == Q->rear) {
		return 1;
	}
	return 0;
}

int IsFull(SeqQueue* Q) {
	if (Q->rear == MaxSize) {
		return 1;
	}
	return 0;
}

//入队
void EnterQueue(SeqQueue* Q, int data) {
	if (IsFull(Q)) {
		cout<<"队列已满"<<endl;
		return ;
	}
	Q->Queue[Q->rear] = data;
	Q->rear = Q->rear + 1;
}

//出队
int DeleteQueue(SeqQueue* Q, int* data) {
	if (IsEmpty(Q)) {
		cout << "队列为空" << endl;
		return 0 ;
	}
	*data = Q->Queue[Q->front];
	Q->front = (Q->front) + 1;
	return 1;
}

//获取队首元素
int GetHead(SeqQueue* Q, int* data) {
	if (IsEmpty(Q)) {
		cout<<"队列为空"<<endl;
	}
	return *data = Q->Queue[Q->front];
}

//清空队列
void ClearQueue(SeqQueue* Q) {
	Q->front = Q->rear = 0;
}
void PrintfQueue(SeqQueue* Q) {
	int i = Q->front;
	while (i < Q->rear) {
		cout << Q->Queue[i];
		i++;
	}
	cout << endl;
}

int main(){
	SeqQueue Q;
	int data;
	//初始化队列
	InitQueue(&Q);
	//入队
	EnterQueue(&Q, 1);
	EnterQueue(&Q, 2);
	EnterQueue(&Q, 3);
	EnterQueue(&Q, 4);
	EnterQueue(&Q, 5);
	
	cout << "队列中的元素为：";
	PrintfQueue(&Q);
	cout << endl;
	DeleteQueue(&Q, &data);
	cout<<"出队元素为："<< data<<endl;
	DeleteQueue(&Q, &data);
	cout << "出队元素为：" << data << endl;
	printf("队列中的元素为：");
	PrintfQueue(&Q);
	cout << endl;
	//获取队首元素
	data = GetHead(&Q, &data);
	cout << "队首元素为：" << data << endl;
	cout << "元素8入队" << endl;
	EnterQueue(&Q, 8);
	cout<<"队列中的元素为："<<endl;
	PrintfQueue(&Q);

	system("pause");
	return EXIT_SUCCESS;
}
