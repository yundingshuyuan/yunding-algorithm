class Solution {
public:
    vector<int> deckRevealedIncreasing(vector<int>& deck) {
        //将数组先进行升序排列然后反推步骤得到目标数组
        sort(deck.begin(),deck.end());

        int n = deck.size();
        deque<int>dq;//创建一个双端队列
        for(int i = n-1; i >= 0; i--){    //n-1逆序添加
             //当队列非空时将底部的牌置于顶部，并在每次添加时保证当前牌处于牌顶
            if(!dq.empty()){   
                dq.push_front(dq.back());
                dq.pop_back();
            }
            dq.push_front(deck[i]);
        }
        vector<int>res(dq.begin(),dq.end());
        return res;

    }
};
