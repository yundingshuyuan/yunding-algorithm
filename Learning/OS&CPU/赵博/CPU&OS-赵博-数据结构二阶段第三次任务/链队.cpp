#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;

typedef struct Qnode {
	int data;
	struct Qnode* next;
}QNode,*QueuePointer;

typedef struct {
	QueuePointer front;//队首指针
	QueuePointer rear;//队尾指针
}LinkQueue;

//链队的初始化
int InitQueue(LinkQueue& Q) {
	Q.front = Q.rear = new QNode;
	Q.front->next = NULL;
	return 1;
}

//链队的销毁
int DestoryQueue(LinkQueue& Q) {
	while (Q.front) {
		QNode *p = Q.front->next;
		delete Q.front;
		Q.front = p;
	}
	return 1;
}

//链队的入队
int EnQueue(LinkQueue& Q, int e) {
	QNode* p = new QNode;//为入对元素分配空间
	p->data = e;
	p->next = NULL;
	Q.rear->next = p;
	Q.rear = p;
	return 1;
}

//链队的出对
int DeQueue(LinkQueue& Q, int& e) {
	if (Q.front == Q.rear)
		return 0;
	QNode *p = Q.front->next;
	e = p->data;
	Q.front->next = p->next;
	if (Q.rear == p)
		Q.rear = Q.front;
	delete p;
	return 1;
}

//获取链队队首元素
int GetHead(LinkQueue& Q) {
	if (Q.front != Q.rear)//队列非空
		return Q.front->next->data;
}

//循环输出链队元素
void CyclicOutput(LinkQueue& Q) {
	if (Q.front == Q.rear)
		return;
	QNode* p = Q.front->next;
	while (p != Q.rear) {
		cout << p->next << "" << endl;
		p = p->next;
	}
	cout << endl;
}

int main(){
	LinkQueue Q;

	if (InitQueue(Q)) {
		cout << "链队初始化成功" << endl;
	}
	else {
		cout << "链栈初始化失败" << endl;
	}
	cout << "1.入队 2.出队 3.查看队顶元素 4.遍历链队 5销毁链队 0 退出" << endl;
	
	int opt;
	int n;
	
	while (1) {
		scanf("%d",&opt);
		if (opt == 0) {
			break;
		}
		switch (opt) {
		case 1:
			cout << "请输入进队元素" << endl;
			scanf("%d",&n);
			if (EnQueue(Q, n)) {
				cout << "入队成功" << endl;
			}
			else {
				cout << "入队失败" << endl;
			}
			break;
		case 2:
			if (DeQueue(Q, n)) {
				cout << "出队元素是" << n << endl;
			}
			else {
				cout << "此队列为空" << endl;
			}
			break;
		case 3:cout << "队首元素是:" << GetHead(Q) << endl;
			break;
		case 4:cout << "遍历队列:";
			CyclicOutput(Q);
			break;
		case 5:if (DestoryQueue(Q)) {
			cout << "链队销毁成功"<<endl;
		}
		default:cout << "请重新输入" << endl;
			break;
		}
	}
	system("pause"); 
	return 0;
}
