/*
创建两个栈，stackIn用来存放数据，stackOut用来输出数据
stackIntop和stackOuttop分别指向栈顶
*/
typedef struct {
	int stackInTop,stackOutTop;
	int stackIn[100],stackOut[100];
}MyQueue; 

//队列的创建及初始化
MyQueue* myQueueCreate(){
	MyQueue *queue = (MyQueue*)malloc(sizeof(MyQueue));
	queue->	stackInTop = 0;
	queue->	stackOutTop = 0;
	return queue;
} 

//将元素存入stackIn栈中，存入后栈顶元素+1
void myQueuePush(MyQueue* obj,int x){
	obj->stackIn[obj->stackInTop] = x;
	obj->stackInTop++;
} 
/*
若输出栈为空且输入栈占有元素时，将输入栈中元素复制到输出栈中，并将栈顶元素保存
当输出栈非空时，将第二个栈中原书复制到第一个栈中 
*/
int myQueuePop(MyQueue *obj){
	int stackInTop = obj->stackInTop;
	int stackOutTop = obj->stackOutTop;
	
	//若输出栈为空，将输入栈中元素复制到输出栈中 
	if(stackOutTop == 0){	
		while(stackInTop == 0){
			obj->stackOut[stackOutTop++] = obj->stackIn[--stackInTop];
		}
	} 
	//将输出栈中栈顶元素出栈
	int top = obj->stackOut[--stackOutTop];
	//将输出栈中元素放回输入栈中
	while(stackOutTop > 0){
		obj->stackIn[stackInTop++] = obj->stackOut[--stackOutTop];
	}
	//更新栈顶指针
	obj->stackInTop = stackInTop;
	obj->stackOutTop = stackOutTop;
	return top; 
} 

//返回输入栈中的栈底元素
int myQueuePeek(Myqueue* obj){
	return obj->stackIn[0];
} 
//若栈顶指针均为0，则代表队列为空 
bool myQueueEmpty(MyQueue *obj){
	return (obj->stackInTop == 0) && (obj->stackOutTop == 0);
}

//将栈顶指针置为0
void myQueueFree(MyQueue *obj){
	obj->stackInTop = 0;
	obj->stackOutTop = 0;
} 
