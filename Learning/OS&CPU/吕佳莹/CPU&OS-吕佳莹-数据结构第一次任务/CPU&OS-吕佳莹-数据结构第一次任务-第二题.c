#include<stdio.h> 

/*实现思路：通过以数组元素的正负为界限，设出z和f；
            nums[z]与nums[f]的平方值比较，较小的一个按顺序赋值给num数组并自增（z）或自减（f）；
            重复第二步，直到z达到最大值（或f达到最小值），再用if语句处理 */ 

int main(){
	int z,f = 0;
	int nums[5] = {};
	int num[5] = {};
	
	int i;
	for(i = 0;i < 5;i++)
		scanf("%d",&nums[i]);
	
	for(i = 0;i < 5;i++){
		if(nums[i] > 0){
			z = i;
			break;
		}
		f = i;
	}	
	
	for(i = 0;i < 5;i++){
		if(nums[f]*nums[f] > nums[z]*nums[z]){
			num[i] = nums[z]*nums[z];
			if(z < 4){
				z++;
				continue;
			}
			else{
				for(i++;f >= 0;i++,f--){
					num[i] = nums[f]*nums[f];
				}
				break;
			}
		}else{
			num[i] = nums[f]*nums[f];
			if(f > 0){
				f--;
				continue;
			}
			else{
				for(i++;z < 5;i++,z++){
					num[i] = nums[z]*nums[z];
				}
				break;
			}
		}
	}
	
	for(i = 0;i < 5;i++)
		printf("\n%d",num[i]);
	
	return 0;
}
