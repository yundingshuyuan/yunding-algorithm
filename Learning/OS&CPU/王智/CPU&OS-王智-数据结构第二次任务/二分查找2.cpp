int search(int* nums, int numsSize, int target){
    int left = 0;
    int right = numsSize;
    while(left  < right){
        int middle = ((left + right)/2);
        if(nums[middle] > target){
            right = middle;
        }else if(nums[middle] < target){
            left = middle;
        }else{
            return middle;
        }
    }
    return -1;
}
