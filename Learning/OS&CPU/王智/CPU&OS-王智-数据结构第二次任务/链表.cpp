// 数据结构——链表的简单实现增删改查以及合并（C语言）
#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>

typedef struct LNode//链表结构体 
{
	int data;
	struct LNode *next;
}LNode,*LinkList;
int CreateList_L(LinkList &L,int n)
{//逆序位输入n个值，建立带表头结点的单链表L 
	int i;
	LinkList p;
	L=(LinkList)malloc(sizeof(LNode));//建立空链表 
	L->next=NULL;
	for(i=n;i>0;i--)
	{
		printf("正在输入第%d个元素",i);
		p=(LinkList)malloc(sizeof(LNode));//开辟空间，生成新结点 
		scanf("%d",&p->data);//输入元素 
		p->next=L->next;
		L->next=p;//插入到表头 
	}
	return 1;
}

int ListInset_L(LinkList &L,int i,int e)//链表的插入 
{//在带头的结点的单链表L中第i个位置前插入元素e 
	LinkList p,s;
	int j=0;
	p=L;
	while(p&&j<i-1)
	{
		p=p->next;
		++j;
	}//寻找第i-1个结点 
	if(!p||j>i-1)
	return 0;//i<1或者大于表长+1 
	s=(LinkList)malloc(sizeof(LNode));//生成新结点 
	s->data=e;
	s->next=p->next;
	p->next=s;//插入l中 
	return 1;
}
int ListDelete_L(LinkList &L,int i,int &e)//删除链表元素 
{//在带头结点的单链表L中，删除第i个元素，并由e返回其值 
	LinkList p,q;
	p=L;
	int j=0;
	while(p->next&&j<i-1)
	{
		p=p->next;
		++j;
	}//寻找第i个结点，并令p指向其前趋 
	if(!(p->next)||j>i-1)
		return 0;//删除位置不合理 
	q=p->next;
	p->next=q->next;//删除并释放结点 
	e=q->data;
	free(q);
	return 1;
}
int GetElem_L(LinkList L,int i,int &e)//按位置查找元素 
{//L为带头结点的单链表的头指针
//当第i个元素存在时，其值给e并由e返回 
	LinkList p;
	p=L->next;
	int j=1;//初始化p，j为计数器 
	while(p&&j<i)	
	{
		p=p->next;
		++j;
	}//顺时针向后查找，直到p指向第i个元素或为空 
	if(!p||j>i)
		return 0;//第i个元素不存在 
	e=p->data;//取第i个元素 
	return 1; 
}

void MergeList_L(LinkList &La,LinkList &Lb,LinkList &Lc)
{//已知单链表La和Lb元素按非递减排列
//归并La和Lb得到新链表Lc，Lc的元素也按非递减排列 
	LinkList pa,pb,pc;
	pa=La->next;
	pb=Lb->next;
	Lc=pc=La;//用La的头结点作为Lc的头结点 
	while(pa&&pb)
	{
		if(pa->data<=pb->data)
		{
			pc->next=pa;
			pc=pa;
			pa=pa->next;
		}
		else
		{
			pc->next=pb;
			pc=pb;
			pb=pb->next;
		}
	}
	pc->next=pa?pa:pb;//插入剩余段 
	free(Lb);//释放Lb的头结点 
}
int Disp(LinkList L)//输出链表 
{
	LinkList p;
	p=L->next;
	if(p==NULL)
	{
		printf("此为空表\n");
	}
	else
	{
		printf("输出链表：\n"); 
		while(p!=NULL)
		{
			printf("%d",p->data);
			printf("\t"); 
			p=p->next;
		 } 
	}
	
	printf("\n");
	return 1;
}
void show()
{
	int n;
	printf("请输入想选择的指令:\n");
	printf("1.建立链表\n");
	printf("2.插入元素\n");
	printf("3.删除元素\n");
	printf("4.查找元素\n");
	printf("5.合并链表\n");
	printf("0.退出\n");
	printf("-------------------\n");
}
int main()
{
	LinkList p1,p2,p3;
	int i,j,k,e;
	int n;
	show();
	scanf("%d", &n);
	while (n != 0)
	{
		switch (n)
		{
		case 1:
			printf("请输入你想建立一号链表内元素的个数:\n");
			scanf("%d", &k);
			CreateList_L(p1, k);
			Disp(p1);
			printf("请输入你想建立二号链表内元素的个数:\n");
			scanf("%d", &k);
			CreateList_L(p2, k);
			Disp(p2);break;
		case 2:
			printf("请输入你想插入在一号链表内的元素和插入的位置：\n");
			scanf("%d %d", &e, &i);
			ListInset_L(p1, i, e);
			Disp(p1);
			printf("请输入你想插入在二号链表内的元素和插入的位置：\n");
			scanf("%d %d", &e, &i);
			ListInset_L(p2, i, e);
			Disp(p2); break;
		case 3:
			printf("请输入你想删除的一号链表的位置：\n");
			scanf("%d", &i);
			ListDelete_L(p1, i, e);
			Disp(p1);
			printf("请输入你想删除的二号链表的位置：\n");
			scanf("%d", &i);
			ListDelete_L(p2, i, e);
			Disp(p2); break;
		case 4:
			printf("请输入你想查看一号链表的位置:\n");
			scanf("%d", &i);
			GetElem_L(p1, i, e);
			printf("该位置元素为:%d\n", e);
			printf("请输入你想查看二号链表的位置:\n");
			scanf("%d", &i);
			GetElem_L(p2, i, e);
			printf("该位置元素为:%d\n", e); break;
		case 5:
			MergeList_L(p1, p2, p3);
			printf("合并后链表：\n");
			Disp(p3); break;
		}
		printf("\n");
		show();
		scanf("%d", &n);
	}
	return 0;
}

