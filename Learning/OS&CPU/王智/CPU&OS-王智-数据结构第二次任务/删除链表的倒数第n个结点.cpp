/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode* removeNthFromEnd(struct ListNode* head, int n){
 struct ListNode* dummyHead = malloc(sizeof(struct ListNode));
    if (dummyHead == NULL) {
        return NULL;
    }

    dummyHead->val = 0, dummyHead->next = head;
    struct ListNode* fast = dummyHead;
    struct ListNode* slow = dummyHead;
    for (int i = 0; i < n; ++i) {
        fast = fast->next;
    }
    while (fast->next != NULL) {
        fast = fast->next;
        slow = slow->next;
    }
    slow->next = slow->next->next;
    struct ListNode* res = dummyHead->next;
    free(dummyHead);
    
    return res;
}

