#include<iostream>
#include<vector>//use for vector
#include<algorithm>//use for sort
using namespace std;

int dichotomy_1(vector<int>& nums, int target) {//[left , right]
	int left = 0;
	int right = nums.size() - 1;
	
	while (left <= right) {//left == right have meaning
		int middle = (right - left) / 2;
		if (nums[middle] > target) {
			right = middle - 1;//middle != target,set right == middle - 1,move scope to left half
		} else if (nums[middle] < target) {
			left = middle + 1;//middle != target,set left == middle + 1,move scope to right half		
		} else if (nums[middle] == target) {
			return middle;
		}
	}
	return -1;
}


int dichotomy_2(vector<int>& nums, int target) {//[left , right)
	int left = 0;
	int right = nums.size();
	
	while (left <= right) {//left == right have no meaning
		int middle = (right - left) / 2;
		if (nums[middle] > target) {
			right = middle;//middle != target,set right == middle - 1,move scope to left half
		} else if (nums[middle] < target) {
			left = middle + 1;//middle != target,set left == middle + 1,move scope to right half
		} else if (nums[middle] == target) {
			return middle;
		}
	}
	return -1;
}


int main() {
	int target = 0;
	vector<int> v_nums = { 1,3,5,7,3,4,523,5634,734,23452,344,233,5234,6352,52 };
	
	cin >> target;
	sort(v_nums.begin(),v_nums.end());//sort the vector for dichotomy
	//int pos = dichotomy_1(v_nums, target);
	int pos = dichotomy_2(v_nums, target);
	cout << pos << endl;

	return EXIT_SUCCESS;
}