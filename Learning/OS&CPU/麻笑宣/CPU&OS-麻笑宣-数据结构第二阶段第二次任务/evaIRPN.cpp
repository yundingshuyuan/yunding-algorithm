#include<iostream>
#include<vector>
#include<stack>
using namespace std;

class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        bool isNumber(string & token);
        stack<int> stk;
        int num1, num2;

        for (int i = 0; i < tokens.size(); i++)
        {
            if (isNumber(tokens[i]))
            {
                stk.push(atoi(tokens[i].c_str()));
            }
            else
            {
                num1 = stk.top();
                stk.pop();
                num2 = stk.top();
                stk.pop();

                switch (tokens[i][0])
                {
                case '+':
                    stk.push(num2 + num1);
                    break;

                case '-':
                    stk.push(num2 - num1);
                    break;

                case '*':
                    stk.push(num2 * num1);
                    break;

                case '/':
                    stk.push(num2 / num1);
                    break;
                }
            }
        }
        return stk.top();
    }
};

bool isNumber(string& token)
{
    return !(token == "+" || token == "-" || token == "*" || token == "/");
}