#include<iostream>
using namespace std;

int minSubArrLen(int target, const int* nums,int times) {
	int sum = 0;
	int pos = 0;
	bool flag = 0;

	while (sum < target && pos < times) {
		sum += nums[pos];
		pos++;

		if (sum >= target) {
			flag = 1;
		}
	}//对排序后的数组从左到右叠加与目标数对比 
	
	if (1 == flag) {
		return pos;
	}else {
		return 0;
	}//判断能否加到目标数
	
	
}

void bubbleSort(int* nums,int sum){
	for (int times = 0; times < sum; times++) {
		for (int pos = 0; pos < sum; pos++) {
			if (nums[pos] < nums[pos + 1]) {
				swap(nums[pos], nums[pos + 1]);
			}
		}
	}
}//冒泡排序数组

int main() {

	int target = 0;
	int nums[1000];
	int pos = 0;

	cin >> target;
	do{
		cin >> nums[pos];
		pos++;
	} while (getchar() != '\n');
	//C++数组录入

	bubbleSort(nums, pos + 1);
	int minLen = minSubArrLen(target,nums,pos);

	cout << minLen << endl;

	return EXIT_SUCCESS;
}