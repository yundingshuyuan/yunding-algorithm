#include<iostream>
using namespace std;

struct ListNode {
	int val;
	ListNode* next;
	ListNode() :val(0), next(nullptr) {};
	ListNode(int n) :val(n), next(nullptr) {};
	ListNode(int n, ListNode* next) :val(n), next(next) {};
};

class MyClass
{
public:
	ListNode* mycycleListNode(ListNode* head) {
		ListNode* ptr = head;
		while (NULL != ptr->next) {
			ptr = ptr->next;
		}
		ptr->next = head;

		return head;
	}

	ListNode* addForCycle(ListNode* head,int pos,ListNode* addVal){//在第pos个节点后加入节点
		ListNode* addNode(addVal);
		ListNode* dummyhead = new ListNode(0,head);
		ListNode* ptr = dummyhead;
		while (pos-- && NULL != ptr->next){
			ptr = ptr->next;
		}
		addNode->next = ptr->next;
		ptr->next = addNode;

		return dummyhead->next;
	}

	ListNode* subForCycle(ListNode* head, int pos) {
		ListNode* ptr = new ListNode(0, head);
		while (pos-- && NULL != ptr->next) {
			ptr = ptr->next;
		}
		ptr->next = ptr->next->next;
		return head;
	}

	ListNode* searchNode(ListNode* head, int val) {
		ListNode* ptr = head;
		bool flag = 0;
		while (NULL != ptr->val) {
			ptr = ptr->next;
			if (val == ptr->val) {
				flag = 1;
				break;
			}
		}
		if (flag) {
			return ptr;
		}else{
			cout << "error" << endl;
			return NULL;
		}
	}
};