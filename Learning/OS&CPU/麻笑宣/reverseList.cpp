#include<iostream>
using namespace std;


//Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};


class Solution {
public:
    ListNode* init_reverseList(ListNode* head) {
        /*ListNode* newBefore(0);
        ListNode* ptr(0);
        ListNode* newNext(0);
        newNext = head;
        ptr = head->next;
        newBefore = head->next->next;*/

        ListNode* newBefore = head->next->next;
        ListNode* ptr = head->next;
        ListNode* newNext = head;


        while (NULL != newBefore->next){
            ptr->next = newNext;
            newNext = ptr;
            ptr = newBefore;
            newBefore = newBefore->next;
        }
        ptr->next = newNext;
        newNext = ptr;
        ptr = newBefore;
        //stop newBefore when newBefore.next == NULL
        //only move ptr and newNext to compelet the function

        return ptr;
    }
};

class Solution {
public:
    ListNode* reverseList_V2(ListNode* head) {
        /*ListNode* ptr(0);
        ListNode* cur(0);
        ListNode* tmp(0);
        cur = head;*/

        ListNode* ptr = NULL;
        ListNode* cur = head;
        ListNode* tmp;

        while (NULL != cur->next) {
            tmp = cur->next;
            cur->next = ptr;
            ptr = cur;
            cur = tmp;
        }
    
        return cur;
    }
};

class Solution {
public:
    ListNode* reverseList_Carl(ListNode* head) {
        ListNode* temp; // 保存cur的下一个节点
        ListNode* cur = head;
        ListNode* pre = NULL;
        while (cur) {
            temp = cur->next;  // 保存一下 cur的下一个节点，因为接下来要改变cur->next
            cur->next = pre; // 翻转操作
            // 更新pre 和 cur指针
            pre = cur;
            cur = temp;
        }
        return pre;
    }
};