#include<iostream>
#include<algorithm>
using namespace std;


//Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {};
    ListNode(int x) : val(x), next(nullptr) {};
    ListNode(int x, ListNode* next) : val(x), next(next) {};
};

class Solution {
public:
    ListNode* swapNodes(ListNode* head, int k) {
		if (head == NULL) {
			return head;
		}

		ListNode* dummyhead = new ListNode(0, head);
		ListNode* fast = dummyhead;
		ListNode* reverse = dummyhead;
		ListNode* order = dummyhead;
		int times = k;//k在移动fast时使用，值已经改变，保存k的值到times供order使用

		while (k-- && fast->next != NULL) {
			fast = fast->next;
		}

		while (NULL != fast) {
			fast = fast->next;
			reverse = reverse->next;
		}

		while (times-- && NULL != order->next) {
			order = order->next;
		}

		swap(order->val, reverse->val);

		return dummyhead->next;
    }
};