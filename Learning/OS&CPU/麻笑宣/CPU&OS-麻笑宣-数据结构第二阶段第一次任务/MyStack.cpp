#include<iostream>
using namespace std;

class stack//元素为整型数据的栈
{
private:
	
public:
	int length;
	int* top = NULL;
	int* bottom = NULL;

	bool empty(stack* s);
	//栈位空返回TRUE，非空返回FALSE
	int size(stack* s);
	//返回栈中元素个数
	auto topval(stack* s);
	//返回栈顶元素
	void pop(stack* s);
	//删除栈顶元素
	void push(stack* s,int x);
	//将元素x压入栈顶
	//Q:按参数决定类型

	stack(int n): bottom((int*)malloc(n*sizeof(stack))),top(bottom+ 1){}
	//malloc函数返回值是开辟空间的初始地址
};


int main()
{
	stack(100);
	return EXIT_SUCCESS;
}


bool stack::empty(stack* s)
{
	if (NULL == s->bottom)
	{
		return true;
	}
	else
	{
		return false;
	}
}//栈位空返回TRUE，非空返回FALSE


int stack::size(stack* s)
{
	int num = s->top - s->bottom;
	return num-1;
}//返回栈中元素个数


auto topval(stack* s)
{
	auto val = *(s->top);
	return val;
}//返回栈顶元素

void pop(stack* s)
{
	if (s->bottom != (s->top - 1))
	{
		s->top = s->top - 1;
	}
	else
	{
		*(s->bottom) = NULL;
	}
}//删除栈顶元素

void stack::push(stack* s,int x)
{
	if (s->length > (s->top - s->bottom + 1))
	{
		s->top += 1;
		*(s->top) = x;
	}
	else
	{
		cout << "push error" << endl;
	}
}//将元素x压入栈顶