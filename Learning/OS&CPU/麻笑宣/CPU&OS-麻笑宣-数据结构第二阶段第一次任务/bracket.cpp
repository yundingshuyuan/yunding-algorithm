class Solution {
public:
    bool isValid(string s) {
        int n = s.size();
        if (1 == (n % 2)) {
            return false;
        }

        stack<char> zhan;
        //int i = 0;
        while (n--)
        {
            if (s[n] == ')' || s[n] == ']' || s[n] == '}')
            {
                zhan.push(s[n]);
            }
            else if ((!zhan.empty()) && s[n] == '(' && zhan.top() == ')')
            {
                zhan.pop();
            }
            else if ((!zhan.empty()) && s[n] == '[' && zhan.top() == ']')
            {
                zhan.pop();
            }
            else if ((!zhan.empty()) && s[n] == '{' && zhan.top() == '}')
            {
                zhan.pop();
            }
            else
            {
                return false;
            }
        }
        return zhan.empty();
    }
};