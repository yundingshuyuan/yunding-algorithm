#pragma once
#include<iostream>
using namespace std;
typedef int Elemtype;

class Node
{
public:
	Elemtype val;
	Node* next;
};

class Qnode
{
public:
	void push(Elemtype nval);
	void pop();
	bool isEmpty();
	void clear();
	int size();
	Elemtype getFront();
	Elemtype getRear();

private:
	Node* qFront;
	Node* qRear;//两个指针都是节点类的指针（指向单位为节点）
	int qSize;
};