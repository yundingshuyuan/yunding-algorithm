#include"Qnode.h"
using namespace std;

void Qnode::push(Elemtype nval)
{
	Node* newnode = (Node*)malloc(sizeof(Node));
	newnode->val = nval;
	qRear = newnode;
	qSize++;
}

void Qnode::pop()
{
	if (isEmpty())
	{
		cout << "Qnode is empty" << endl;
		return;
	}

	Node* ptr = qFront;
	qFront = qFront->next;
	free(ptr);
	ptr = NULL;
	qSize--;
}

bool Qnode::isEmpty()
{
	return qFront == qRear == NULL;
}

void Qnode::clear()
{
	if (isEmpty())
	{
		cout << "Qnode is empty" << endl;
		return;
	}
	qRear = qRear->next;//NULL
	qFront = qRear;
}

int Qnode::size()
{
	return qSize;
}

Elemtype Qnode::getFront()
{
	return qFront->val;
}

Elemtype Qnode::getRear()
{
	return qRear->val;
}