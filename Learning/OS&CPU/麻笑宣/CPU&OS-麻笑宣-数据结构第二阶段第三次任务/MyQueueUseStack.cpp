#include<iostream>
using namespace std;
#include<stack>

class MyQueue {
public:
    MyQueue() {

    }

    stack<int> stk1;
    stack<int> stk2;

    void push(int x) {
        stk1.push(x);
    }

    int pop() {
        if (stk2.size() == 0)
        {
            int k = stk1.size();
            for (int i = 0; i < k; i++)
            {
                stk2.push(stk1.top());
                stk1.pop();
            }
        }

        int rsu = stk2.top();
        stk2.pop();
        return rsu;
    }

    int peek() {
        if (stk2.size() == 0)
        {
            int k = stk1.size();
            for (int i = 0; i < k; i++)
            {
                stk2.push(stk1.top());
                stk1.pop();
            }
        }

        int rsu = stk2.top();
        return rsu;
    }

    bool empty() {
        return 0 == (stk2.size() + stk1.size());
    }

};
