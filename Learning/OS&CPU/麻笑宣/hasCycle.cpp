#include<iostream>
#include<algorithm>
using namespace std;

struct ListNode {
	int val;
	ListNode* next;
	ListNode(int n) :val(n), next(nullptr) {};
};

class Solution {
public:
    bool hasCycle(ListNode* head) {
        if (NULL == head || NULL == head->next) {
            return false;
        }

        ListNode* fast = head;
        ListNode* slow = head;
        while (fast != NULL && fast->next != NULL) {
            fast = fast->next->next;
            slow = slow->next;
            if (slow == fast) {
                return true;
            }
        }

        return false;
    }
};