#pragma once
#include<iostream>
using namespace std;
typedef int Elemtype;

class Queue
{
public:
	Queue();

	Queue(int capa);

	void qPush(Elemtype val);//因为是循环队列，所以不需要判满

	void qPop();

	Elemtype getTop();

	Elemtype getRear();

	bool isEmpty();

	bool isFull();

	Elemtype& operator[](int pos);

	Elemtype* getAddress();

private:
	Elemtype* Address;
	int capacity;
	int pFront;
	int pRear;
	int size;
};