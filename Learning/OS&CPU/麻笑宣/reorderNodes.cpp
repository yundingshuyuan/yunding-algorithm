#include<iostream>
#include<algorithm>
using namespace std;

struct ListNode {
	int val;
	ListNode* next;
	ListNode() :val(0), next(nullptr) {};
	ListNode(int n) :val(n), next(nullptr) {};
	ListNode(int n, ListNode* next) :val(n), next(next) {};
};

class Solution {
public:
    ListNode* orderhead = head;
    ListNode* reversehead = head->next;
    ListNode* first = orderhead;
    ListNode* second = reversehead;
    ListNode* temp_or, * temp_re;

    while ((NULL != first->next && NULL != first->next->next) || (NULL != second->next && NULL != second->next->next)) {
        if (NULL != first->next->next) {
            first->next = first->next->next;
            first = first->next;
        }
        if (NULL != second->next->next) {
            second->next = second->next->next;
            second = second->next;
        }
    }
    first->next = NULL;
    second->next = NULL;

    reversehead = reverseList_Carl(reversehead);
    first = orderhead;
    second = reversehead;
    temp_or = first->next;
    temp_re = second->next;

    while (NULL != temp_or || NULL != temp_re) {
        first->next = second;
        second->next = temp_or;
        first = temp_or;
        second = temp_re;
        if (NULL != temp_or) {
            temp_or = temp_or->next;
        }
        if (NULL != temp_re) {
            temp_re = temp_re->next;
        }
    }

    ListNode* reverseList_Carl(ListNode* head) {
        ListNode* temp; // 保存cur的下一个节点
        ListNode* cur = head;
        ListNode* pre = NULL;
        while (cur) {
            temp = cur->next;  // 保存一下 cur的下一个节点，因为接下来要改变cur->next
            cur->next = pre; // 翻转操作
            // 更新pre 和 cur指针
            pre = cur;
            cur = temp;
        }
        return pre;
    }
};