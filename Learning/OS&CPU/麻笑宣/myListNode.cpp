#include<iostream>
using namespace std;

struct myListNode{
	int val;
	myListNode* next;//to next pointer
	myListNode() : val(0), next(nullptr) {};//default constructor
	myListNode(int x) : val(x), next(nullptr) {};//custom constructor_1
	myListNode(int x,myListNode* next) : val(x), next(next) {};//custom constructor_2
};

myListNode* addListNode(myListNode* head,int pos, int x, myListNode* next) {
	myListNode* dummyhead(0);
	dummyhead->next = head;//link dummyhead with head
	myListNode* ptr(0);
	myListNode* ptr_next(0);
	ptr = dummyhead;

	while (pos-- && NULL != ptr) {
		ptr = ptr->next;
	}//move ptr to target pos
	
	ptr_next = ptr->next;//save ptr_nxet for link
	ptr->next = next;//link target with added
	ptr = next;//move ptr to added
	ptr->next = ptr_next;//link added with ptr_next
	
	return dummyhead->next;
}

myListNode* redListNode(myListNode* head, int pos) {
	myListNode* dummyhead(0);
	dummyhead->next = head;
	myListNode* ptr(0);
	ptr = dummyhead;

	while (pos-- && NULL != ptr) {
		ptr = ptr->next;
	}//move ptr to target pos

	ptr->next = ptr->next->next;//remove target

	return dummyhead->next;
}

myListNode* searchListNode(myListNode* head, int pos) {
	myListNode* ptr = head;
	while(--pos && NULL != ptr){
		ptr = ptr->next;
	}
	return ptr;
}

myListNode* changeListNode(myListNode* head, int pos,int change) {
	myListNode* ptr = head;
	while (--pos && NULL != ptr) {
		ptr = ptr->next;
	}

	ptr->val = change;
	return ptr;
}