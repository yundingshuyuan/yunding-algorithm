#include<math.h>
#include<stdio.h>
#include<windows.h>
int minSubArrayLen(int target, int* nums, int numsSize){
    if (numsSize == 0){
        return 0;
    }
    int res=INT_MAX;
    for (int i=0;i<numsSize;i++){
        int sum=0;
        for(int j=i;j<numsSize;j++){
            sum +=nums[j];
            if(sum >=target){
                if(res>j-i+1)
                res=j-i+1;
                break;
            }
        }
    }
    return res == INT_MAX ? 0 : res;
}
int main(){
	int numsSize=5;
	int nums[5]={5,1,4,8,3};
	int target=12;
	printf("%d",minSubArrayLen(target,nums,numsSize));
	return 0;
}
