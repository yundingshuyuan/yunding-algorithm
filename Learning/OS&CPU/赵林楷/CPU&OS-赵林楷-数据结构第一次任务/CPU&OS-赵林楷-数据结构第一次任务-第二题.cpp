#include<stdio.h>
int* Squares(int* nums, int numsSize, int* returnSize){
    int i;
    for(i=0;i<numsSize;i++){
        returnSize[i]=nums[i]*nums[i];
    }
    return returnSize;
}
void Quick_Sort(int *arr, int begin, int end){
    if(begin > end)
        return;
    int tmp = arr[begin];
    int i = begin;
    int j = end;
    while(i != j){
        while(arr[j] >= tmp && j > i)
            j--;
        while(arr[i] <= tmp && j > i)
            i++;
        if(j > i){
            int t = arr[i];
            arr[i] = arr[j];
            arr[j] = t;
        }
    }
    arr[begin] = arr[i];
    arr[i] = tmp;
    Quick_Sort(arr, begin, i-1);
    Quick_Sort(arr, i+1, end);
}
int main()
{
	int numsSize=5;
	int nums[5]={1,2,-6,3,-5};
	int returnSize[5];
	int *returnS;
	returnS=Squares(nums,numsSize,returnSize);
	Quick_Sort(returnS,0,numsSize-1);
	for(int i=0;i<numsSize;i++)
	{
		printf("%d\n",returnSize[i]);
	}
	return 0;
}
