#include<stdio.h>
#include<stdlib.h>

//定义结构体 
typedef struct Lik
{
	int val;
	struct Lik *next;
} LinkList;

LinkList *creat();
 
void insert(LinkList *list);

void delet(LinkList *list);

void change(LinkList *list);

void getval(LinkList* list); 

int main()
{
	LinkList *t=creat();
	int choose;
	puts("请选择你要进行的操作：\n插入输入 1 \n删除输入 2 \n改值输入 3 \n查询输入4");
	scanf("%d",&choose);
	switch(choose)
	{
		case 1:insert(t);
		break;
		case 2:delet(t);
		break;
		case 3:change(t);
		break;
		case 4:getval(t);
		break;
		default:break; 
	}
	return 0;
}



//创建链表 
LinkList *creat()
{
	int n;
	puts("请输入你要创建多少个节点：");
	scanf("%d",&n); 
	LinkList *head, *node, *end;
	head = (LinkList*)malloc(sizeof(LinkList));
	end = head;
	for (int i = 0; i < n; i++) 
	{
		node = (LinkList*)malloc(sizeof(LinkList));
		puts("请输入某个节点的值：");
		scanf("%d", &node->val);
		end->next = node;
		end = node;
	}
	end->next = NULL;
	return head;
}



//插入节点 
void insert(LinkList *list) 
{
	int n;
	puts("请输入你要插入第几个节点：");
	scanf("%d",&n); 
	LinkList *t = list, *in;
	int i = 0;
	while (i < n && t != NULL) 
	{
		t = t->next;
		i++;
	}
	if (t != NULL) 
	{
		in = (LinkList*)malloc(sizeof(LinkList));
		puts("请输入要插入的值");
		scanf("%d", &in->val);
		in->next = t->next;
		t->next = in;
	}
	else 
	{
		puts("节点不存在");
	}
}



//删除节点 
void delet(LinkList *list) 
{
	int n;
	puts("请输入你要删除第几个节点：");
	scanf("%d",&n);
	LinkList *t = list, *in;
	int i = 0;
	while (i < n && t != NULL) 
	{
		in = t;
		t = t->next;
		i++;
	}
	if (t != NULL) 
	{
		in->next = t->next;
		free(t);
	}
	else 
	{
		puts("节点不存在");
	}
}



//修改节点的值 
void change(LinkList *list) 
{ 
    LinkList *t = list;
    int n;
	puts("请输入你要修改第几个节点的值：");
	scanf("%d",&n);
	int i = 0;
	while (i < n && t != NULL) 
	{
		t = t->next;
		i++;
	}
	if (t != NULL) 
	{
		puts("请输入你要改成什么：");
		scanf("%d", &t->val);
	}
	else 
	{
		puts("节点不存在");
	}
}



//查某个节点的值
void getval(LinkList* list)
{
    LinkList *t = list;
    int n;
	puts("请输入你要查第几个节点：");
	scanf("%d",&n); 
    for (int i = 0; i < n; i++) 
	{
        t=t->next;
    }
    if (t != NULL) 
	{
		printf("该节点值为%d",t->val);
	}
	else 
	{
		puts("节点不存在");
	}
} 
