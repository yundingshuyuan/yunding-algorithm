//二分查找 
int search(int* nums, int numsSize, int target){
    int low=0,high=numsSize-1;
        while (low <= high) {
            int mid=(high+low)/2;
            int num = nums[mid];
            if (num == target) {
                return mid;
            } else if (num > target) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }
//删除倒数第n个节点 
struct ListNode* removeNthFromEnd(struct ListNode* head, int n){
    struct ListNode* t1=head,*t2=head,*in;
    int s=0;
    while(t1!=NULL)
    {
        t1=t1->next;
        s++;
    }
    int i=0;
    while(i<s-n&&t2!=NULL)
    {
        in=t2;
        t2=t2->next;
        i++;
    }
    if(t2!=NULL)
    {
        in->next=t2->next;
    }
    if(s==n)
    {
        head=head->next;
    }
return head;
}
