int minSubArrayLen(int s, int *nums, int numsSize)
{
    int res = INT_MAX;
    int first = 0;
    int second = 0;
    int sum = 0;
    while (first <= second && second<numsSize) {
        sum += nums[second];
        if (sum >= s) {
            res = fmin(res, second - first + 1);
            sum -= nums[first];
            sum -= nums[second];
            first++;
        } else {
            second++;
        }
    }
    if(res==INT_MAX) {
        return 0;
    }
    return res;
}