#include<stdio.h>
#include<stdlib.h>

//结构体定义
struct ListNode {
	int data;//存放数据
	ListNode* next;//指向下一个节点
	ListNode(int x) : data(x),next(NULL) {}//节点构造函数
};

struct ListNode* head = NULL;
struct ListNode* end = NULL;

//尾增
void AddList(int data);
//遍历链表
void Scan();
//查询指定节点
struct ListNode* FindNode(int data);
//链表清空
void FreeList();
//在指定位置添加
void AppointList(int index,int data);
//修改节点内容
void Amend(int data,int redata);
//尾删
void DeleteEnd();
//头删
void DeleteHead();
//删除节点
void DeleteList(int data);
//删除倒数第N个节点
void DeleteNth(int n);

int main()
{
	return 0;
}

void AddList(int data)
{
	//创建一个节点
	struct ListNode* temp = (struct ListNode*)malloc(sizeof(struct ListNode));

	//节点数据赋值
	temp->data = data;
	temp->next = NULL;

	if (NULL == head)
	{
		head == temp;
	}
	else {
		end->next = temp;
	}
	end = temp;
}

void Scan()
{
	struct ListNode* temp = head;
	while (temp != NULL)
	{
		printf("%d", temp->data);
		temp = temp->next;
	}
}

struct ListNode* FindNode(int data)
{
	struct ListNode* temp = head;
	while (temp != NULL) {
		if (data == temp->data) {
			return temp;
		}
		else {
			temp = temp->next;
		}
	}
	return NULL;
}

void FreeList()
{
	struct ListNode* temp = head;
	while (temp != NULL)
	{
		struct ListNode* pt = temp;
		temp = temp->next;
		free(pt);
	}
	head = NULL;
	end = NULL;
}

void AppointList(int index, int data)
{
	if (NULL == head)
	{
		printf("链表没有节点]\n");
		return;
	}
	struct ListNode* pt = FindNode(index);
	if (NULL == pt) {
		printf("没有指定节点\n");
		return;
	}
	//创建临时节点
	struct ListNode* temp = (struct ListNode*)malloc(sizeof(struct ListNode));
	temp->data = data;//节点成员赋值
	temp->next = NULL;
	//连接到链表上
	if (pt == end)
	{
		end->next = temp;
		end = temp;
	}
	else
	{
		temp->next = pt->next;
		pt->next = temp;
	}
}

void Amend(int data, int redata)
{
	struct ListNode* temp = FindNode(data);
	if (temp == NULL)
	{
		printf("没有该节点\n");
	}
	else {
		temp->data = redata;
	}
}

void DeleteEnd()
{
	if (NULL == end)
	{
		printf("链表为空\n");
		return;
	}
	if (head == NULL)
	{
		free(head);
		head = NULL;
		end = NULL;
	}
	else
	{
		struct ListNode* temp = head;
		while (temp->next != end)
		{
			temp = temp->next;
		}
		free(end);
		end = temp;
		end->next = NULL;
	}
}

void DeleteHead()
{
	struct ListNode* temp = head;
	if (NULL == head)
	{
		printf("链表为空\n");
		return;
	}
	head = head->next;
	free(temp);
}

void DeleteList(int data)
{
	if (NULL == head) {
		printf("链表为空\n");
		return;
	}

	struct ListNode* temp = FindNode(data);
	if (NULL == temp)
	{
		printf("无此节点\n");
		return;
	}

	if (head == end)//链表只有一个节点
	{
		free(head);
		head = NULL;
		end = NULL;
	}
	else {
		if (end == temp)
			DeleteEnd();
		else if (temp == head)
			DeleteHead();
		else {
			struct ListNode* pt = head;
			while (pt->next != temp)
			{
				pt = pt->next;
			}
			pt->next->next;
			free(temp);
		}
	}
}

void DeleteNth(int n)
{
	if (head == end)
	{
		printf("链表为空\n");
		return;
	}
	
	struct ListNode* temp = (struct ListNode*)malloc(sizeof(struct ListNode));
	temp->next = head;
	ListNode* fast = temp;
	ListNode* slow = temp;
	while(n-- && fast != NULL) {
		fast = fast->next;
	}
	fast = fast->next;
	while (fast != NULL) {
		fast = fast->next;
		slow = slow->next;
	}
	slow->next = slow->next->next;
	return;
}