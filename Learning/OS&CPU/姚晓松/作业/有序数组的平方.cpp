#include<stdio.h>

int main()
{
	int nums[5] = { -4, 1, 0, 3, 10 };
	int k = 0, j = 4, res[5] = { 0 };
	for (int i = 4; i >= 0; i--) {
		if (nums[k] * nums[k] > nums[j] * nums[j]) {
			res[i] = nums[k] * nums[k];
			k++;
		}
		else {
			res[i] = nums[j] * nums[j];
			j--;
		}
	}
	for (int i = 0; i < 5; i++) {
		printf("%d ", res[i]);
	}
	return 0;
}