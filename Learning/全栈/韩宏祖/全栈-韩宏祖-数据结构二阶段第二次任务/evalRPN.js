/**
 * @param {string[]} tokens
 * @return {number}
 */
 var evalRPN = function(tokens) {
    const stack = [];
    var len = tokens.length;
    let i = 0;
    while(i < len) {
        let val = tokens[i]
        if(val === '+' || val === '-' || val === '*' || val ==='/') {
            let result = 0;
            let num1 = stack.pop();
            let num2 = stack.pop();
            if(val === '+') {
                result = num1 + num2;
            } else if(val === '-') {
                result = num1 - num2;
            } else if(val === '*') {
                result = num1 * num2;
            } else if(val === '/') {
                result = Math.trunc(num1 / num2);
            }
            stack.push(result);
        } else {
            stack.push(Number(val));
        }
            i++;
    }
         return stack.pop();
   
};


//未通过全部案例