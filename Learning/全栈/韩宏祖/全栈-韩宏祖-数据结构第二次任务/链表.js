/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
 var removeNthFromEnd = function(head, n) {
    let ret = new ListNode(0,head);
    //判断力扣的ListNode
    console.log(ret);
    console.log(ret.next);
    console.log(ret.val);
    // ListNode(X,head)给数组head首位加了一个X
    let slow = ret;
    let fast = ret;
    //数组加了首位，所以fast走n步。
    while(n--) {
        fast = fast.next;
    }
    //验证
    //console.log(fast);   //2，3，4，5
    //让fast指向终点的null
    while (fast.next !== null) {
        fast = fast.next; 
        slow = slow.next;
    };
    //console.log(fast);  // 5
    console.log(slow);    //3,4,5
    console.log(slow.next);
    console.log(slow.next.next);
    slow.next = slow.next.next;  //让4,5 = 5 即删除了倒数第二个 
    return ret.next;
};











function ListNode(){
    
}