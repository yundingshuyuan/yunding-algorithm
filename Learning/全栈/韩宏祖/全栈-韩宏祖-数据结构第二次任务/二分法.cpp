#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include <stdlib.h>
#include <math.h>

//力扣代码
int search(int* nums, int numsSize, int target) {
	int left = 0, middle = 0;
	int right = numsSize - 1;
	while (left <= right) {
		middle = (left + right) / 2;
		if (nums[middle] < target) {
			left = middle + 1;
		}
		else if (nums[middle] > target) {
			right = middle - 1;
		}
		else {
			return middle;
		}
	}
	return -1;
}

/*
int main() {
		int n = 0, i = 0;
		int *array;   //数组指针！
		printf("请输入数组长度：\n");
		scanf("%d", &n);
		array = (int*)malloc(n * sizeof(int));
		if (!array) {
			printf("创建数组失败！");
			exit(1);
		}

		printf("请开始输入数组...(必须为升序！)\n");
		for (i = 0; i < n; i++) {
			printf("输入：");
			scanf("%d", &array[i]);
		}

		printf("输入的数组为：\n");
		printf("[");
		for (i = 0; i < n; i++) {
			printf("%d  ", array[i]);
		}
		printf("]\n");
		printf("请输入目标值：");
		int target = 0;
		scanf("%d", &target);

		//search(*array, n, target);
	return 0;
}
*/