/************节点*************/
class Node {
    constructor(element) {
        this.element = element; //当前节点的数据  
        this.next = null; //下一个节点数据  
    }
}

/*************************链表*****************************/
class CycleList {
    constructor() {
        this.head = new Node("head"); //头节点  
        this.head.next = this.head; 
    }
    //查找某一节点  
    find(item) {
        var currNode = this.head;
        while (currNode.element != item) {
            currNode = currNode.next;
            if (currNode == this.head)
                return null;
        }
        return currNode;
    }
    //查找尾节点  
    findLast() {
        var currNode = this.head;
        while (!(currNode.next == this.head)) {
            currNode = currNode.next;
        }
        return currNode;
    }
    //插入新节点  
    addend(newElement) {
        var newNode = new Node(newElement);
        var current = this.findLast(); //默认插入到尾部  
        newNode.next = current.next;
        current.next = newNode;
    }
    //删除某一个节点  
    remove(item) {
        var prevNode = this.findPrevious(item);
        if (!(prevNode.next == this.head)) { //不能删除头  
            prevNode.next = prevNode.next.next;
        }
    }
    //在控制台打印出所有节点
    display() {
        var currNode = this.head;
        while (!(currNode.next == this.head)) {
            console.log(currNode.next.element);
            currNode = currNode.next;
        }
    }
}


let arr = new CycleList;

arr.addend(1);
arr.addend(3);
arr.addend(5);
arr.display();
console.log(arr);

//参考代码地址  https://www.iteye.com/blog/cobain-li-2340642
