/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
 var reorderList = function(head) {
    let pre = head;
    let end = pre.next;

    while(end) {
        end.pre = pre;
        pre = end;
        end = pre ? pre.next:null;
    }
    end = pre;
    pre = head; 

    

    while(pre.next !== end && pre !== end) {
        let temp = pre.next;
        pre.next = end;
        end.next = temp;
        pre = temp;
        end = end.pre;
    }
    //去除环
    end.next = null;
};


//https://www.bilibili.com/video/BV1Rr4y1S7Mz?spm_id_from=333.880.my_history.page.click