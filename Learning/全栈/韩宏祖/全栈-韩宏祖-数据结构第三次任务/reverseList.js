/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var reverseList = function(head) {
    let temp = null;
    let fast = head;
    let slow = null;
    while(fast) {
        temp = fast.next;
        fast.next = slow;
        slow = fast;
        fast = temp; 
    }
    return slow;
};