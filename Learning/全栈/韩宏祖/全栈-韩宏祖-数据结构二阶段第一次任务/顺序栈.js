class ArrayStack {
    constructor() {
        var arr = [];

        //入栈
        this.push = function (element) {
            arr.push(element);
        };

        //出栈
        this.pop = function () {
            return arr.pop();
        };

        //获取栈顶
        this.top = function () {
            return arr[arr.length - 1];
        };

        //获取长度
        this.size = function () {
            return arr.length;
        };

        //清空
        this.clear = function () {
            arr = [];
            return true;
        };

        //打印
        this.toString = function () {
            return arr.toString();
        };
    }
} 


let stack = new ArrayStack();


stack.push(6);
stack.push(9);
stack.push(7);
console.log(stack.top()); // 7
console.log(stack.size()); // 3
console.log(stack.toString()); // 6,9,7
console.log(stack.clear());
console.log(stack.toString());  //空
