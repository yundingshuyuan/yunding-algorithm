/**
 * @param {string} s
 * @return {boolean}
 */
 var isValid = function(s) {
    const stack = [];
    /*
     * 字典-----是一些元素的集合。每个元素有一个称作key 的域，不同元素的key 各不相同。
     *Map字典是以[键，值]的形式存储
    */
    const map = new Map();
    //set()  设置键值对
    //设置键名key对应的键值为value，然后返回整个 Map 结构
    map.set('(',')');
    map.set('[',']');
    map.set('{','}');
    for (let i = 0;i < s.length;i++) {
        if(map.has(s[i])) {
            stack.push(s[i]);
        } else {
            const top = stack[stack.length-1];
            //get方法读取key对应的键值，如果找不到key，返回undefined
            if(map.get(top) === s[i]) {
                stack.pop()
            } else {
                return false;
            }
        }
    }
    return stack.length === 0;
};

//有效括号
//https://www.bilibili.com/video/BV1fQ4y1U7uc?spm_id_from=333.880.my_history.page.click
//Map 字典结构
//https://blog.csdn.net/yiyueqinghui/article/details/112876881