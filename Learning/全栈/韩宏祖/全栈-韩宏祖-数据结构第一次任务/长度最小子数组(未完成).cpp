#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 100


int create() {
	int n = 0,i = 0;
	int *array;   //数组指针！
	printf("请输入数组长度：\n");
	scanf("%d", &n);
	array = (int*)malloc(n*sizeof(int));
	if (!array) {
		printf("创建数组失败！");
		exit (1);
	}

	printf("请开始输入数组...\n");
	for (i = 0; i < n; i++) {
		printf("输入：");
		scanf("%d",&array[i]);
	}

	printf("输入的数组为：\n");
	printf("[");
	for (i = 0; i < n; i++) {
		printf("%d  ", array[i]);
	}
	printf("]\n");

	printf("请输入要求的和：");
	int target = 0;
	scanf("%d", &target);
	printf("%d",target);

	for (int i = 0; i < n; i++) {
		int sum = 0;
			for (int j = i; j < n; j++) {
			sum += array[j];
			if (sum >= target) {
				*array = fmin( *array, j - i + 1);
				break;
			}
		}
	}
	int *re = (array == array ? 0 : array);
	return *re;



	//free(array);
	//system("pause");


}



int main() {
	create();
}