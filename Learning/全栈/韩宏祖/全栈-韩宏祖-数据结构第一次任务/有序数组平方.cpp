#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 100

//完成但时间并未注意，有参考。

//https://www.runoob.com/cprogramming/c-function-qsort.html
//https://blog.csdn.net/qq2466200050/article/details/122620153

int cmp(const void* _a, const void* _b){
		int a = *(int*)_a, b = *(int*)_b;
		return a - b;
	}

int main() {
	int n = 0, i = 0;
	int *array;   //数组指针！
	printf("请输入数组长度：\n");
	scanf("%d", &n);
	array = (int*)malloc(n * sizeof(int));
	if (!array) {
		printf("创建数组失败！");
		exit(1);
	}

	printf("请开始输入数组...\n");
	for (i = 0; i < n; i++) {
		printf("输入：");
		scanf("%d", &array[i]);
	}

	printf("输入的数组为：\n");
	printf("[");
	for (i = 0; i < n; i++) {
		printf("%d  ", array[i]);
	}
	printf("]\n");


	

	/*printf("输出:\n");
	for (int j = 0; j < n; j++) {
		int add = array[j] * array[j];
		printf("%d \n", add);
	}*/
	int *ans = (int*)malloc(sizeof(int) * n);
	for (int i = 0; i < n; ++i) {
		ans[i] = array[i] * array[i];
	}
	qsort(ans, n, sizeof(int),cmp);
	for (int j = 0; j < n; j++) {
		printf("%d  ", ans[j]);
	}
	return 0;
}