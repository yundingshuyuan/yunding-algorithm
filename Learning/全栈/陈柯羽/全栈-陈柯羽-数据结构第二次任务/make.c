typedef struct myNode{
    int isEnd;
    struct myNode* Node[26];
} WordDictionary;

WordDictionary* wordDictionaryCreate() {
    WordDictionary* s = NULL;
    s = malloc(sizeof(WordDictionary));
    memset(s, 0, sizeof(WordDictionary));
    return s;
}

void wordDictionaryAddWord(WordDictionary* obj, char * word) {
    WordDictionary* tmp = obj;
    for (int i = 0; i < strlen(word); i++) {
        if (tmp->Node[word[i] - 'a'] == NULL) {
            tmp->Node[word[i] - 'a'] = malloc(sizeof(WordDictionary));
            memset(tmp->Node[word[i] - 'a'], 0, sizeof(WordDictionary));
        }
        tmp = tmp->Node[word[i] - 'a']; 
    }
    tmp->isEnd = 1;
    return;
}

bool wordDictionarySearch(WordDictionary* obj, char * word) {
    WordDictionary* tmp = obj;
    for (int i = 0; i < strlen(word); i++) {
        if (word[i] == '.') {
            bool dot = false;
            for (int j = 0; j < 26; j++) {
                // printf("%c strlen = %d \r\n ", ('a' + j), strlen(word));
                if (tmp->Node[j]) {
                    if (wordDictionarySearch(tmp->Node[j], word + i + 1)) {
                        dot = true;
                    }
                }
            }
            // printf(" false with %s \r\n", word);
            return dot;
        }
        if (tmp->Node[word[i] - 'a'] == NULL) {
            return false;
        }
        tmp = tmp->Node[word[i] - 'a'];
    }
    if (tmp->isEnd) {
        return true;
    }
    return false;
}

void wordDictionaryFree(WordDictionary* obj) {
    free(obj);
    return;
}