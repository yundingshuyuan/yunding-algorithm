//长度最小的子数组
    //滑动窗口解法
int searchMinArrayLength(int s, int nums[], int length){
    int sum = 0;               //  窗口中数组元素的和
    int res = length + 1;    //  最小的连续子数组的长度
    int left = 0, right = -1;  //  nums[left...right] 为滑动窗口
    while (left < length) {
        // sum 小于 s，窗口的右边界向前拓展，但要保证右边界 right 不越界
        if ((right < length - 1) && (sum < s)) {
            sum += nums[++right];
        //  sum 大于等于 s，窗口的左边界向前行进
        } else {
            sum -= nums[left++];
        }
        //  找到可行的连续子数组，取 res 和目前连续子数组的长度（前闭右闭，长度 +1）的最小值
        if (sum >= s) {
            res = res < right - left + 1 ? res : right - left + 1;
        }
    }
    // 不存在符合条件的子数组，则返回 0，否则返回最小的连续子数组的长度
    return res == length + 1 ? 0 : res;
}