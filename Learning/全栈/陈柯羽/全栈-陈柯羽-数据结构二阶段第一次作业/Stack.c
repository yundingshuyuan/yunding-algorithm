//数组栈
typedef struct{
    int data[SIZE];
    int top;
}Stack;
//将元素堆入栈中
int push(Stack &L,int e){
    //栈满
    if(L.top == SIZE -1){
        return 0;
    }
    //入栈
    L.data[L.top++] = e;
    //返回该元素
    return e;
}
//移除栈顶元素
int pop(Stack &L){
    if(L.top == SIZE -1){
        return 0;
    }
    //打印并返回栈
    int val = L.data[--L.top];
    printf("%d",val);
    return val;
}
//判断栈是否为空
Status isFull(Stack s){
    if(s.top != SIZE -1){
        return 1;
    }
    return 0;
}