#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
typedef struct Linknode{
    int data;
    struct Linknode *pNext;
}LNode,*LinkList;
LinkList phead;//定义头指针变量
//尾部插入
LinkList addEnd(LinkList phead,int data){
    LinkList pnew = (LinkList)malloc(sizeof(node));
    pnew -> data = data;
    if(phead == NULL){
        phead = pnew;
        pnew -> pNext = phead;
    }
    else{
        LinkList p = phead;
        while(p -> pNext != phead){//定位到尾部
            p = p -> pNext;
        }
        p -> pNext = pnew;//链接到新的节点
        pnew -> pNext = phead;//头尾相连
    }
    return phead;
}
//头部插入
LinkList addStart(LinkList phead,int data){
    LinkList pnew = ((LinkList)malloc(sizeof(node)));
    pnew -> data = data;
    if(phead == NULL){
        phead = pnew;
        pnew -> pNext = phead;
    }
    else{
        LinkList p = phead;
        while(p -> pNext != phead){//定位到尾部
            p = p -> pNext;
        }
        p -> pNext = pnew;//链接到新的节点
        pnew -> pNext = phead;//头尾相连
        phead = pnew;
    }
    return phead;
}
void showIt(LinkList phead){
    printf("Let's show it!");
    if(phead == NULL){
        return;
    }
    else if(phead -> pNext == phead){
        printf("%d,%p,%p",phead -> data,phead,phead -> next);
    }
    else{
        LinkList p = phead;
        while(p -> pNext != phead){
            printf("\n%d,%p,%p",p -> data,p,p -> pNext);
            p p -> pNext;
        }
        printf("\n%d,%p,%p",p -> data,p,p -> pNext);//补充相等的情况
    }
}
//查找
//找到第一个对应值为data的结点
LinkList findFirst(LinkList phead,int data){
    if(phead == NULL){
        return NULL;
    }
    else if(phead -> pNext == phead){
        if(phead -> data == data){
            return phead;
        }
    }
    else{
        LinkList p = phead;
        while(p -> pNext != phead){
            if(p -> data == data){
                return phead;
            }
            p = p -> pNext;
        }
        if(p -> data == data){
            return phead;
        }
    }
}
//删除
//删除第一个对应值为data的结点
LinkList deleteFirst(LinkList phead,int data,LinkList*ppNext){
    LinkList p1 = NULL;
    LinkList p2 = NULL:
    p1 = phead;
    while(p1 -> pNext != phead){
        if(p1 -> data == data){
            break;
        }
        else{
            p2 = p1;
            p1 = p1 -> pNext;
        }
    }
    if(p1 != phead){
        p2 -> pNext = p1 -> pNext;
        *ppNext = p1 -> pNext;
        free(p1);
    }
    else{//刚好是头节点
        LinkList p = phead;
        while(p -> pNext != phead){
            p = p -> pNext;
        }
        phead = phead -> pNext;
        *ppNext = p1 -> pNext;
        free(p1);
        p -> pNext = phead;
    }
    return phead;
}
//计算节点个数
int getNum(LinkList phead){
    if(phead == NULL){
        return 0;
    }
    else if(phead -> pNext == phead){
        return 1;
    }
    else{
        int i = 1;
        LinkList p = phead;
        while(p -> pNext != phead){
            i++;
            p = p -> pNext;
        }
        return i;
    }
}
//如果存在节点值为finddata的结点，则在其前面插入data
LinkList insertFirst(LinkList phead,int finddata,int data){
    LinkList p1 = NULL;
    LinkList p2 = NULL;
    p1 = phead;
    while(p1 -> pNext != phead){
        if(p1 -> data == finddata){
            break;
        }
        else{
            p2 = p1;
            p1 = p1 -> pNext;
        }
    }
    LinkList pnew = ((LinkList)malloc(sizeof(node)));
    pnew -> data = data;
    if(p1 != phead){
        pnew -> pNext = p1;
        p2 -> pNext = pnew;
    }
    else{
        LinkList p = phead;
        while(p -> pNext != phead){
            p = -> pNext;
        }
        p -> pNext = pnew;
        pnew -> pNext = phead;
        phead = pnew;
    }
    return phead;
}
void main(){
    LinkList phead = NULL;
    printf("addStart\n");
    for(int i = 0;i < 5;i++){
        phead = addStart(phead,i);
    }
    showIt(phead);
    printf("addEnd\n");
    phead = NULL;
    for(int i = 0;i < 5;i++){
        phead = addEnd(phead,i);
    }
    showIt(phead);
    printf("findFirst\n");
    LinkList pfind = findFirst(phead,3);
    pfind ->data = 666666;
    showIt(phead);
    printf("deleteFirst\n");
    phead = deleteFirst(phead,2);
    showIt(phead);
    printf("insertFirst\n");
    phead = insertFirst(phead,3,888);
    showIt(phead);
}