//Task one : reverseList
struct list*reverseList(struct list*head){
    if(head==NULL||head->next==NULL){
        return head;
    }
    struct list*newHead = reverseList(head -> next);
    head->next->next = head;//n1的下一个节点要指向空，否则可能产生环
    head->next = NULL;
    return newHead;
}

//Task two : swapNodes
    // struct list*swapNodes(list*head,int k){
    //     if((head -> next)==NULL){
    //         return head;
    //     }
    //     if(head == NULL){
    //         return;
    //     }
    //     else{
    //         list*p = NULL;
    //         int length = 0;
    //         length++;
    //         if()
    //     }
    // }
    list*swapNodes(list*head,int k){
        list*fast = head;
        for(int i = 1;i < k;i++){
            fast = fast -> next;
        }
        list*temp = fast;
        list*slow = head;
        while(fast -> next){
            fast = fast ->next;
            slow = slow -> next;
        }
        swap(slow -> val,temp -> val);
        return head;
    }
    
//Task three : reorderList
void reorderList(struct list*head){
    if(head == NUL){
        return;
    }
    struct list*vec[40001];
    struct list*node = head;
    int num = 0;
    while(node != NULL){
        vec[num++] = node;
        node = node -> next;
    }
    int i = 0,j = num - 1;
    while(i < j){
        vec[i] ->next = vec[j];
        i++;
        if (i == j){
            break;
        }
        vec[j] -> next = vec[i];
        j--;
    }
    vec[i] -> next = NULL;
}