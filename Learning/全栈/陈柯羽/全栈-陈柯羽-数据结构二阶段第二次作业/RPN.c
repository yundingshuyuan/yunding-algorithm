//逆波兰表达式(后缀表达式)：遇到数字则入栈；遇到运算符则取出栈顶两个数字进行计算，并将结果压入栈中
int RPN(char**tokens,int tokensRes){
    int n = tokensRes;
    int stk[n].top = 0;
    for(int i = 0; i < n; i++){
        char*tokens = tokens[i];
        if(strlen(token) > 1 || (token[0] >= '0' && token[0] <= '9')){
            stk[top++] = atoi(token);       //C 库函数 int atoi(const char *str) 把参数 str 所指向的字符串转换为一个整数（类型为 int 型）,该函数存放在C语言标准库 - <stdlib.h>中；
        }
        else{
            int numOne = stk[--top];
            int numTwo = stk[--top];
            switch(token[0]){
                case '+':
                    stk[top++] = numOne + numTwo;
                    break;
                case '-':
                    stk[top++] = numOne - numTwo;
                    break;
                case '*':
                    stk[top++] = numOne * numTwo;
                    break;
                case '/':
                    stk[top++] = numOne / numTwo;
                    break;
            }
        }
    }
    return stk[top - 1];
}