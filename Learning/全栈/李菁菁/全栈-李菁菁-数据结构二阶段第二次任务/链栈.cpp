#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>

typedef int elementype;

typedef struct node{
	elementype data;
	struct node *next;
}stacknode, *link;

typedef struct stack{
	link top;		//栈顶指针 
	int count;	   //计数器 
}Linkstack;

//判断是否为空栈
int stackempty(Linkstack S){
    if (S.count == 0)
        return 1;
    else
        return 0;
}

//入栈
bool push(Linkstack *S, elementype e){
	link s = (link)malloc(sizeof(stacknode));
	
	s -> data = e;
	s -> next = S -> top;		//当前的栈顶赋值给新的元素
	S -> top = s;		//新元素为栈顶 
	S -> count++;
	return true; 
}

// 出栈
 int pop(Linkstack *s, elementype *e){
	link temp;		//临时节点
	
	if(stackempty(*s))
		return 0;
	else
	{
		*e = s -> top -> data;
		temp = s -> top;		//将栈顶指针为临时节点 
		s -> top = s -> top -> next;	//栈顶指针下移一位
		free(temp);		//释放临时节点		
		s -> count--; 
		return 1;
	}	
}

//构造空栈
 bool InitStack(Linkstack *S){ 
    S -> top = (link)malloc(sizeof(stacknode));
    
    if(!S -> top)
        return false;
        
    S -> top = NULL;
    S -> count = 0;
    
    return true;
} 

//将S置为空栈
 bool ClearStack(Linkstack *S){ 
    link p,q;
    p = S -> top;
    
    while(p)
        {  
            q = p;
            p = p -> next;
            free(q);		//释放每个节点 
        } 
        
    S -> count = 0;
    return true;
}

//栈的长度
int StackLength(Linkstack S){ 

    return S.count;
}

//判断是否为空  若不空返回栈顶元素
int GetTop(Linkstack S,elementype *e){
    if (S.top == NULL)
        return 0;
        
    else
        *e = S.top -> data;
        
    return 1;
}

 
