/**
 * @param {string[]} tokens
 * @return {number}
 */
 var evalRPN = function(tokens) {

    //Map 字典数据结构
    const s = new Map([
        ["+",(a,b) => a*1 + b*1],
        ["-",(a,b) => b-a],
        ["*",(a,b) => b*a],
        ["/",(a,b) => (b/a) | 0]
    ])

    const stack = []

    for(const i of tokens){
        //has方法 判断某个键是否在当前Map对象中
        if(!s.has(i)){
            stack.push(i)   //入栈
            continue
        }

        //get方法 读取key对应的键值
        stack.push(s.get(i)(stack.pop(),stack.pop()))  //pop() 栈顶元素出栈  并返回栈顶元素
    }

    return stack.pop();
};