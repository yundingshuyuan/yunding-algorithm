/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var sortedSquares = function(nums) {
    let len = nums.length   //数组长度，遍历
    let res = []   //存放新数组
    let i = 0, j = len - 1, k = len - 1   // 数组结尾\0  加一
    
    //双指针前后同时遍历
    while (i <= j) {   
        let left = nums[i] * nums[i],
            right = nums[j] * nums[j]
            
        if (left < right){
            res[k--] = right  //k中间量
            j--
        } 
        else{
            res[k--] = left
            i++
        }
    }
    return res
};