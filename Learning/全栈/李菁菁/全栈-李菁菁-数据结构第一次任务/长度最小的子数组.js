/**
 * @param {number} target  
 * @param {number[]} nums  
 * @return {number}
*/
var minSubArrayLen = function(target, nums) {
      
        let i,j,sonlen ,sum  //sum为数的和，sonarr为符合条件的子数组的长度
        i=j=sum=0
        sonlen = nums.length+1  //子数组的最大长度为原数组的长度  
        
        while(j<nums.length){
            sum += nums[j]
            j++
    
            while(sum >= target){
                if(sonlen>=j-i+1){
                    sonlen = j-i+1
                }
                else{
                    sonlen = sonlen
                }
    
                sum -= nums[i]
                i++
                j = i
            }
        }
    
        if(sonlen == nums.length+1){
            return 0
        }
        else
        return sonlen
    
    };
