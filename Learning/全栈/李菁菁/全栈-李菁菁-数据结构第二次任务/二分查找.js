/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */

 //左闭右闭
 var search = function(nums, target) {
    let left = 0,right = nums.length-1

    while(left<=right){
        let middle = left+Math.floor((right-left)/2)  //Math.floor() 取整
        if(nums[middle]<target){
            left = middle+1
        }
        else if(nums[middle]>target){
            left = middle-1
        }
        else{
            return middle
        }
    }

    return -1
};