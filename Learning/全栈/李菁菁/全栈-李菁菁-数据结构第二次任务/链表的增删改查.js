//生成链表结构
class ListNode {
    constructor(val,next) {
      this.val = val
      this.next = null
    }
}

//输入头节点
class link_list{
  constructor(val){
    this.head = new ListNode(val)
  }
}

//查找元素
function find(val){
  let node = this.head

  while(node && node.val){
    if(node.val == val){
      console.log(node)
      //return node
    }
    node = node.next  //下一个继续循环
  }
  //不存在  
  //return null 
  console.log(null)
}

//增加元素
//new_val 为新添加的元素 ,pre_val为上一个节点的元素
function add(new_val,pre_val){
  let add_node = new ListNode(new_val)
  let node = this.find(pre_val)  //通过查找功能找到节点

  if(!node){
    console.log("不存在该元素!")
  }

  if(node.next){
    add_node = node.next
    node.next = add_node
  }
  else{
    node.next = add_node
    add_node.next = null
  }
}

//删除元素
function deletee(val){
  let delete_node = this.find(val)  //找到删除的节点

  if(!delete_node){
    console.log("不存在该元素!")
  }
  
  //如果该节点为尾节点
  if(delete_node.next == null){
    (delete_node-1).next = null  //他的上一个节点变为Null
  }
  else{
    (delete_node-1).next = delete_node.next
  }

}

//修改元素
function change(val,new_val){
  let node = this.find(val)

  if(!node){
    console.log("不存在该元素!")
  }
  else{
    node.val = new_val
  }
}

