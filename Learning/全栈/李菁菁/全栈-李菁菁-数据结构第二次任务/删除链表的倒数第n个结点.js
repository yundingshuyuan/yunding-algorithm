/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
 var removeNthFromEnd = function(head, n) {
    let link_list = new ListNode(0,head)
    left=right=link_list

    //right向右移动n+1
    for(let a=0;a<n+1;a++){
        right = right.next
    }

    //同时后移直至right到null处
    while(right != null){
        right = right.next
        left = left.next
    }

    left.next = left.next.next

    return link_list.next
};