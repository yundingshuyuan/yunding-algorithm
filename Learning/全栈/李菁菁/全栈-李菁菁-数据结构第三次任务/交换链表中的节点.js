/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */
 var swapNodes = function(head, k) {
    let count=1,p = head ,front = null ,back = head , temp
    //front 为第k个数 ，back为倒数第k个数

    while(p){
        if(count == k){
            front = p
        }

        if(count > k){
            back = back.next
        }

        p = p.next
        count++
    }

    temp = front.val
    front.val = back.val
    back.val = temp

    return head
};