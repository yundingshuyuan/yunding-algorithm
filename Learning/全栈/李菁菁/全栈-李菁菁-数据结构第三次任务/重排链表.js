/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
 var reorderList = function(head) {
    if(!head.next){
        return head
    }

    let pre = head,ahead = head.next

    while(ahead&&ahead.next){
        pre = pre.next
        ahead = ahead.next

        if(ahead.next){
            ahead = ahead.next
        }
    }

    let newhead = null,p = pre.next,temp
    pre.next = null

    while(p){
        temp = p
        p = p.next
        temp.next = newhead
        newhead = temp
    }

    let resultnode = new ListNode(0),now = resultnode , con = true
    pre = head,ahead = newhead

    while(pre || ahead){
        if(con){
            now.next = pre
            pre = pre.next
        }
        else{
            now.next = ahead,
            ahead = ahead.next
        }

        now = now.next
        con = !con
    }

    return resultnode.next
};