/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
 var reverseList = function(head) {
    if(!head || !head.next)
        return head

    let temp = null ,pre = null , ahead = head

    while(ahead){
        temp = ahead.next
        ahead.next = pre
        pre = ahead
        ahead = temp
    }

    return pre
};