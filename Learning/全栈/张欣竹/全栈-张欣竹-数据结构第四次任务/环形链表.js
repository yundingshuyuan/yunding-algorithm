/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
var hasCycle = function(head) {
    //设置一个快指针，一个慢指针，并且都初始化为head
    let fast = head;
    let slow = head;
    while (fast!= null && fast.next != null) {
        slow = slow.next;//慢指针一次走一步
        fast = fast.next.next;//快指针一次走两步
        if(fast === slow) {
            return true;//快慢指针相遇时，说明是环形链表
        }
    }
    return false;
}