#include <stdio.h>
#include <stdlib.h>

#define MaxSize 50                 //设置栈最大容量处
typedef int ElemType;              //设置栈内元素类型处

/*顺序栈结构*/
typedef struct
{
    ElemType data[MaxSize];           //存放数据数组，ElemType的含义就是“数据元素的类型”
    int top;                          //栈顶指针，即存放栈顶元素在data数组中的下标
}SqStack;

/*****初始化栈*****/
void InitStack(SqStack* &s)
{
    s = (SqStack *)malloc(sizeof(SqStack));    //分配一个顺序栈空间， malloc()函数用来动态地分配内存空间
    s->top = -1;                               //栈顶指针初始化为-1
}

/*****销毁栈*****/
void DestroyStack(SqStack* &s)
{
    free(s);//C语言当中 free() 函数与 malloc() 函数应是成对出现的：malloc() 函数负责空间的申请， malloc() 函数的对头free() 函数则负责将 malloc() 函数申请的空间给释放掉；
}

/*****判断栈是否为空*****/
bool StackEmpty(SqStack* s)
{
    return (s->top == -1);//->s是一个结构体变量，对于结构体变量，对其中的指针成员采取->进行访问,对其他成员，采取.进行访问
}

/*****进栈*****/
bool Push(SqStack* &s, ElemType e)
{
    if (s->top == MaxSize - 1)             //栈满返回false
        return false;
    s->top++;                              //栈顶指针增1
    s->data[s->top] = e;                   //元素e放在栈顶指针处
    return true;
}

/*****出栈*****/
bool Pop(SqStack* &s, ElemType &e)
{
    if (s->top == -1)                       //栈空返回false
        return false;
    e = s->data[s->top];                    //取栈顶元素
    s->top--;
    return true;
}

/*****取栈顶元素*****/
bool GetTop(SqStack* s, ElemType &e)
{
    if (s->top == -1)
        return false;
    e = s->data[s->top];
    return true;
}

/*****获取栈的长度*****/
int getLen(SqStack* s) {
    return s->top + 1;
}


//测试函数 
int main()
{
    SqStack *testStack;
    InitStack(testStack);
    for (int i = 0; i < 5; i++)
    {
        Push(testStack, i);
    }
    int len = getLen(testStack);
    printf("该栈的长度为：%d\n", len);
    int top;
    GetTop(testStack, top);
    printf("栈顶元素为：%d\n", top);
    int n = 0;
    int out;
    while (1)
    {
        if (!Pop(testStack, out))
            break;
        n++;
        printf("第%d次出栈的元素为：%d\n", n, out);
    }
    if (StackEmpty(testStack))
    {
        DestroyStack(testStack);
        printf("栈为空，已成功销毁!\n");
    }
    return 0;
}
