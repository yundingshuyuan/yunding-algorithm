/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
    let stack = [], n = s.length;
    for(let i =0; i < n; i++ ){
        let char = s[i];
        if(char === '(' || char === '[' || char === '{'){ //输入的是左括号，入栈
            stack.push(char);
        }
        else{
            if (stack.length === 0){ //输入的是右括号时
                return false; //此时栈空，无法进行匹配
            }
            const top = stack[stack.length - 1];//获取栈顶
            if (top === '(' && char === ')' || top === '[' && char === ']' || top === '{' && char === '}'){
                stack.pop(); //如果栈顶是对应的左括号，被匹配，出栈
            }
            else{
                return false; //不是对应的左括号，无法匹配
            }
        }
    }
    return stack.length === 0; // // 栈空，则所有左括号找到匹配；栈中还剩有左括号，则没被匹配
};