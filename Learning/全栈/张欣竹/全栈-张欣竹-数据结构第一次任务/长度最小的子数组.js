/*var minSubArrayLen = function(target, nums) {
    // 长度计算一次
    const len = nums.length;
    let l = r = sum = 0, 
        res = len + 1; // 子数组最大不会超过自身
    while(r < len) {
        sum += nums[r++];
        // 窗口滑动
        while(sum >= target) {
            // r始终为开区间 [l, r)
            res = res < r - l ? res : r - l;
            sum-=nums[l++];
        }
    }
    return res > len ? 0 : res;
};
*/

var minSubArrayLen = function(target, nums) {
    for(let i = 0; i < nums.length; i++){
        var value = target - nums[i]//第一层for循环，获取target 与 nums[i]之间的差值 value
        for(let j = i+1; j < nums.length; j++){//第二次循环，将差值value 与 nums[j]进行比对
            if(nums[j] === value)//将差值value 与 nums[j]完全相等时
            return [i,j]//nums[i] 与 nums[j] 的和则为target,数组下标则为 [i,j]
        }
    }
};