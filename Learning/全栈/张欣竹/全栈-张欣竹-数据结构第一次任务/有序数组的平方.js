//时间超时
/*var sortedSquares = function(nums) {
    let n = nums.length;
    let arr = [];
    let i = 0, j = n - 1;
    let left = nums[i] * nums[i],
        right = nums[j] * nums[j];
    while (i <= j){
        if(left < right){
            arr.unshift(left);
        }
        else{
            arr.unshift(right);
        }
    }
    return arr;
};
*/

/**
 * @param {number[]} nums
 * @return {number[]}
 */
var sortedSquares = function(nums) {
    let n = nums.length;
    let res = new Array(n).fill(0);//创建一个数组并填充默认值
    let i = 0, j = n - 1, k = n - 1;//k表示从后往前输入
    while (i <= j) {
        let left = nums[i] * nums[i],
            right = nums[j] * nums[j];
        if (left < right) {
            res[k--] = right;
            j--;
        } else {
            res[k--] = left;
            i++;
        }
    }
    return res;
};