var swapPairs = function (head) {
  let ret = new ListNode(0, head), temp = ret;
  while (temp.next && temp.next.next) {
    let cur = temp.next.next, pre = temp.next;
    pre.next = cur.next;
    cur.next = pre;
    temp.next = cur;
    temp = pre;
  }
  return ret.next;
};


var swapNodes = function (head, k) {
    let i = 1
    let j = head
    let front = null // 定义第K个节点
    let p = head // 定义倒数第K个节点
    while (j) {
        if (i === k) { // 遍历先找到第K个节点
            front = j
        }
        if (i > k) { //开始移动cur指针
            p = p.next
        }
        j = j.next
            ++i;
    }
    // 交换节点的值
    let temp = null
    temp = front.val
    front.val = p.val
    p.val = temp
    return head
};