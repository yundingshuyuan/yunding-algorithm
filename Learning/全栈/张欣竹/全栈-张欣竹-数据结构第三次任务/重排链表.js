var reorderList = function (head) {
  let list = []; // 使用数组存储链表
  let node = head; // 使用node遍历链表

  // 遍历链表，将每个元素依次存入数组
  while (node) {
    list.push(node);
    node = node.next;
  }

  let newList = []; // 使用新数组，存储新排序的链表节点
  let i = 0; // 使用i指针从头往后遍历list
  let j = list.length - 1; // 使用j指针从后往前遍历list

  // 左右指针不断向中间移动，知道相遇
  while (i <= j) {
    // 将i、j指向的元素，依次存入newList
    newList.push(list[i++], list[j--]);
  }

  let newNode = newList[0]; // 缓存新链表的头节点

  // newList的每个元素，就是新链表的每个节点
  for (let i = 1; i < newList.length; i++) {
    // 将每个节点连接到链表
    newNode.next = newList[i];
    // 将新链表节点向后移动一位，待连接下一个节点
    newNode = newNode.next;
  }
  // 将尾节点的next置为null，避免链表出现环
  newNode.next = null;

  // head也是新链表的头结点
  return head;
};
