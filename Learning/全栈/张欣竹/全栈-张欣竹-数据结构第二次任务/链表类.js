//向链表尾部添加一个新的项
 LinkedList.prototype.append=function(data){
    // 1.创建新的节点
    var newNode=new Node(data);
    // 2.判断是否添加的是第一个节点
    if (this.length==0) {
        // 2.1 是第一个节点
        this.head=newNode;
    } 
    else {
        // 2.2不是第一个节点
        var current=this.head;
        // 2.2.1 找到最后一个节点
        while (current.next) {
            current=current.next
        }
        // 2.2.2 将最后一个节点的next指向新的节点
        current.next=newNode;
    }     
    this.length+=1;
}

//修改某个元素的位置
LinkedList.prototype.updata=function(position,element){
    //对position进行越界判断
    if (position<0||position>=this.length) {
        return false;
    }
    var current=this.head;
    var index=0;
    //查找正确的节点
    while (index<position) {
        current=current.next;
        index+=1;
    }
    //将position位置的node的data修改为newData
    current.data=element;
    return true;
}

//根据元素从链表移除一项
//先获取位置，再删除元素
LinkedList.prototype.remove=function(element){
    var index=0;
    var current=this.head;
    var previous=null;
    while (current) {
        if (current.data==element) {
            if (index==0) {
                this.head=this.head.next;
            } else {
                previous.next=current.next;
            }
            this.length-=1;
            return true;
        }
        previous=current;
        current=current.next;
        index+=1;
    }
    return -1;
}
