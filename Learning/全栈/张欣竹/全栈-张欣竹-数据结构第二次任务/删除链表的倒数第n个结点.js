/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function(head, n) {
    var dummy = new ListNode(0) // 定义哑结点
   dummy.next = head
   var len = 0
   var first = head
   //求链表的长度
   while(first != null){
       len++
       first = first.next
   }
   //遍历到n前一个节点，使其指向n的下一个节点
   len = len - n
   first = dummy
   while(len > 0){
       len--
       first = first.next
   }
   first.next = first.next.next
   return dummy.next

};



/*
var removeNthFromEnd = function(head, n) {
    let ret = new ListNode(0, head),
        slow = fast = ret; //定义fast指针和slow指针
    while(n--) fast = fast.next; // fast再提前走一步，因为需要让slow指向删除节点的上一个节点
    while (fast.next !== null) {
        fast = fast.next; 
        slow = slow.next
    };
    slow.next = slow.next.next; //fast 走到结尾后，slow的下一个节点为倒数第N个节点
    return ret.next;
};
*/