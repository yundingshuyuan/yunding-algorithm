#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>

#define STACK_INIT_SIZE 10  //存储空间初始分配量
#define STACK_INCREMENT 2  //存储空间初始分配增量
typedef struct SqStack{
	SElemType*base;  //在栈构造之前和销毁之后，base的值为NULL
	SElemType*top;  //栈顶指针
	int stacksize;  //当前已分配的存储空间，以元素为单位
}SqStack; 

//顺序栈的初始化
void Init Stack(SqStack*S)  //构造一个空栈S
{
	(*S).base=(SElemType*)malloc(STACK_INIT_SIZE*sizeof(SElemType)); 
	if(!(*S). base) 
		exit(OVERFLOW);  //存储分配失败
	(*S).top=(*S).base;
	(*S).stacksize=STACK_INIT_SIZE;
}

//顺序栈的进栈
void Push(SqStack*S,SElemType e)  //插入元素e为新的栈顶元素
{
	if((*S).top-(*S).base>=(*S).stacksize)  //栈满，追加存储空间
	{
		(*S).base=(SElemType*)realloc((*S).base,((*S).stacksize+STACK_INCREMENT)*sizeof(SElemType));
	if(!(*S).base)
		exit(OVERFLOW);  //储分配失败
	(*S).top=(*S).base+(*S).stacksize;
	(*S).stacksize+=STACK_INCREMENT;
	}
	*((*S). top)++=e;
}

//顺序栈的出栈
Status Pop(SqStack*S,SElemType*e)  //若栈不空，则删除S的栈顶元素，用e返回其值，并返回OK；否则返回ERROR
{
	if((*S).top==(*S).base)
		return ERROR;
	*e=*--(*S).top;	
	return OK;
}

//顺序栈判断栈空
Status StackEmpty(SqStackS)  //老栈S为空栈，则返回TRUE，否则返回FALSE
{
	if(S.top==S.base)	
		return TRUE;
	else
		return FALSE; 
}

//求顺序栈中元素个数
int StackLength(SqStackS)  //返回S的元素个数，即栈的长度
{
	return S.top-S.base;
}

//取顺序栈的栈顶元素
Status GetTop(SqStackS,SElemType*e)  //若栈不空，则用e返回S的栈顶元素，并返回OK；否则返回ERROR
{
	if(S.top>S.base)
	{
		*e=*(S.top-1); 
		return OK;
	}
	else
		return ERROR;
}
