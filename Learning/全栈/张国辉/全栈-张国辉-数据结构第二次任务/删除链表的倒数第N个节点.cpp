struct ListNode* removeNthFromEnd(struct ListNode* head, int n)
{
	struct ListNode* slow=head,*fast=head,*pre_slow=head;
	while(n>0)
	{
        fast=fast->next;
        n--;
	
	}
	while(fast != NULL)
	{
		fast=fast->next;
		pre_slow = slow;
		slow=slow->next;
	}
	if(slow == head)
	{
        head = head->next;
        return head;
    }
    pre_slow->next=slow->next;
    return head;
}
