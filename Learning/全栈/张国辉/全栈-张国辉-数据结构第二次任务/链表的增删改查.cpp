#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>

//创建 
typedef struct LNode 
{
	int data;
	struct LNode *next;
}LNode,*LinkList;

//初始化 
int CreateList(LinkList& L)
{
	L=(LinkList)malloc(sizeof(LNode));
	if(!L)
	{
		printf("内存不足 存储分配失败");
	}
	else
	{
		L->next=NULL;
	}
} 

//增 
int InsertList(LinkList &L,int i,int e)
{	
	int j=0;
	LinkList p=L,s;
	while(p!=NULL&&j<i-1)
	{
		p=p->next;
		j++;
	} 
	if(!p||j>i-1)
	return 0;//i小于1或者大于表长
	s=(LinkList)malloc(sizeof(LNode));//s指向新生成的结点 
	s->data=e;//将e存入新结点的数据域 
	s->next=p->next;//将结点的指针域指向第i个结点 
	p->next=s;//第i-1个结点的指针域指向新生成的结点 
	return 1;
}

//删
int ListDelete(LinkList &L,int i,int &e) 
{
	//在带头结点的单链表L中，删除第i个元素，并由e返回其值 
	int j=0;
	LinkList p=L,q;
	while(p->next&&j<i-1)//查找第i-1个结点，并令p指向其前趋 
	{
		p=p->next;
		++j;
	}
	if(!(p->next)||j>i-1)//删除位置不合理
		return 0; 
	q=p->next;//令p指向第i个结点 
	p->next=q->next;//删除并释放结点 
	e=q->data;
	free(q);
	return 1;
}

//改 
int ChangeLinkList(LinkList L,int i,int e){//改变第i位置的值 
	LNode* p=L;
	for(int j=0;j<i;j++){
		p=p->next;
	}
	p->data=e;
	return 1;
}

//查
int SearchLinkList(LinkList L,int i,int &e)
{
	//L为带头结点的单链表的头指针。当第i个元素存在时，其值给e并由e返回1，否则返回0 
	int j=1;//j为计数器 
	LinkList p=L->next;//p指向第一个结点 
	while(p&&j<i)//顺指针向后查找，直到p指向第i个元素或p为空 
	{
		p=p->next;
		j++;
	}
	if(!p||j>i)//第i个元素不存在
	{
		return 0; 
	}
	e=p->data;//取第i个元素 
	return 1; 
}
