void reorderList(struct ListNode* head)
{	//头节点不存在或头节点的下一个节点不存在。此时不需要交换，直接返回head
    if(head == NULL || head->next == NULL)
    {
        return head;
    }
    //第一步：先找到中间结点
    struct ListNode *slow = head , *fast = head->next;
    while(fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
    }
    struct ListNode *head1 = head,*head2 = slow->next ;
    //断开链表的链接
    slow->next = NULL;
    //第二步：逆转后半部分链表
    struct ListNode *p,*q;
    p=head2;
    head2 = NULL;
    while(p)
    {   	
        q=p->next;//暂存 
        p->next = head2;
        head2 = p;
        p=q;
    }
    p=NULL;
    q=NULL;
    //第三步：链表的重组
    while(head1 && head2)
    {
        p=head1->next;
        q=head2->next;
        head1->next = head2;
        head2->next = p;
        head1=p;
        head2=q;
    }
}
