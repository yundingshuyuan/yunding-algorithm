//递归法 
struct ListNode* swapPairs(struct ListNode* head)
{
    //递归结束条件：头节点不存在或头节点的下一个节点不存在。此时不需要交换，直接返回head
    if(head == NULL || head->next  == NULL)
    {
		return head;
	}  
    //创建一个新的节点指针保存头结点下一个节点
    struct ListNode *newHead = head->next;
    //更改头结点+2位节点后的值，并将头结点的next指针指向这个更改过的+2位结点 
    head->next = swapPairs(newHead->next);
    //将新的头结点的next指针指向原先的头节点
    newHead->next = head;
    return newHead;
}
