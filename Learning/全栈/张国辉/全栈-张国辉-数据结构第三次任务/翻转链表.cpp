struct ListNode* reverseList(struct ListNode* head)
{
    struct ListNode* temporary_storage;
    struct ListNode* new_head = NULL;
    while(head != NULL) 
	{
		//将其下一个结点暂存起来 
        temporary_storage = head->next;
        //下一个结点被重新赋值，即反向指向 
        head->next = new_head;
        //被反向指向的结点顺移 
        new_head = head;
        //将暂存的下一个结点作为之后的前一个结点 
        head = temporary_storage;
    }
    return new_head;
}
