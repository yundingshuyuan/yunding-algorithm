#include <stdio.h>
#include <stdlib.h>
int main()
{
	int i,n;
	scanf("%d",n);
	int nums[n];
	while(n--)
	{
		scanf("%d",nums[i]);
	}
	int numsSize;
	numsSize = sizeof(nums);
	int left = 0;
	int right = numsSize - 1;
	int k;
	k = sizeof(nums);
	int square_nums[k];
	for(k = numsSize - 1; k >= 0; k--)
	{
		int left_square,right_square;
		left_square = nums[left] * nums[left];
		right_square = nums[right] * nums[right];
		if(left_square > right_square)
		{
			square_nums[k] = left_square;
			left_square++;
		}
		else
		{
			square_nums[k] = right_square;
		}
	}
	printf("{");
	int j;
	for(j = 0;j < k;j++)
	{
		printf("%d,",square_nums[j]);
	}
	printf("}");
	return 0;
}		
