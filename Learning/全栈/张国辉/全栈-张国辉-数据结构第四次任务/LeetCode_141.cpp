//����ָ�� 
bool hasCycle(struct ListNode *head) {   
    struct ListNode *last = head;
    struct ListNode *fast = head;   
    while(fast && fast->next){
        last = last->next;
        fast = fast->next->next;
        if(last == fast){
            return true;
        }
    }
    return false;
}
