#include <iostream>
#include <stdlib.h>
using namespace std;
// ADT:

//一个链表
struct node{
    int datas;
    struct node *nextPtr;
};

struct node *creatList(void);//初始化链表 返回头指针
void visitList(struct node*head);//访问链表
struct node *searchList(struct node*head,int goal);//查找 返回指针
void sortList(struct node*head);//排序
void insertList(struct node*head,int position,int add);//插入
void deleteList(struct node*head,int position);//删除

struct node *createList(void){
    struct node *head = NULL;//头指针
    struct node *newPtr = NULL;//新节点
    int value;
    printf("please enter value(q to quit):");
    while(scanf("%d",&value)==1){//读入int类型，返回值是1
        newPtr = (struct node*)malloc(sizeof (struct node));//malloc函数返回值是未知类型的指针，需要强制类型转换
        newPtr->nextPtr = NULL;
        newPtr->datas = value;
        if(newPtr == NULL){
            printf("error! cannot allocate memory.\n");
            exit(1);
        }
//        else{//头部插入
//            newPtr->nextPtr = head;//新节点指向head(空指针)
//            head = newPtr;//先访问新的再访问老的head
//            printf("please enter value(q to quit):");
//        }
        else{//尾部插入
            if(head == NULL) head = newPtr;
            else {
                struct node *tmp = head;
                while(tmp->nextPtr != NULL) {//摸索出尾部
                    tmp = tmp->nextPtr;
                }
                tmp->nextPtr = newPtr;
            }
            printf("please enter value(q to quit):");
        }
    }
    return head;//返回链表头指针
}

void visitList(struct node *head){
    struct node *curptr;
    curptr = head;
    while (curptr != NULL){
        printf("%d ",curptr->datas);
        curptr = curptr->nextPtr;
    }
    printf("\n");
}

void sortList(struct node*head){
    struct node *curptr = head;//当前指针
    struct node *moveptr;//移动指针
    struct node *minptr;//最小指针
    int temp;//暂时存放值
    while(curptr->nextPtr != NULL){//遍历
        minptr = curptr;
        moveptr = curptr->nextPtr;
        while (moveptr != NULL){
            if(minptr->datas > moveptr->datas){//minptr一直比较到末尾
                minptr = moveptr;
            }
            moveptr = moveptr->nextPtr;
        }
        if (minptr != curptr){//min指向位置移动过
            temp = minptr->datas;
            minptr->datas = curptr->datas;
            curptr->datas = temp;
        }
        curptr = curptr->nextPtr;
    }
}

struct node *searchList(struct node*head,int goal){
    struct node *curptr = head;
    while((curptr->nextPtr != NULL)&&(curptr->datas != goal)){
        curptr = curptr->nextPtr;
    }
    if (curptr == NULL){//跳出循环的两种情况
        return NULL;
    } else{
        return curptr;
    }
}

void insertList(struct node*head,int position,int value){
    struct node *temp;//新节点
    struct node *beforetemp = head;

    temp = (struct node*) malloc(sizeof(struct node));//为新节点分配空间
    if (temp == NULL){
        printf("error!");
        exit(1);
    }

    if (position == 1){//头节点位置插入
        temp = head;
        head->datas = value;
        head->nextPtr = temp;
    }else{//其他位置插入
        for(int i = 2;i<position;i++){//用来找到位置
            beforetemp = beforetemp->nextPtr;
            if (beforetemp == NULL){
                printf("invalid position");
                return;
            }
        }
        temp->datas = value;
        temp->nextPtr = beforetemp->nextPtr;
        beforetemp->nextPtr = temp;
    }
}

void deleteList(struct node*head,int value){
    struct node *ptr = searchList(head,value);//得到要删除元素的地址

    if (ptr == NULL) return;
    if (ptr == head){//删除头节点:)不能删除只有一个头节点的链表的头节点
        //解决办法传入struct **head(单纯的使head = NULL不会反映到main中)or传入指针的引用 struct node *&head 直接存入外界的head
        head->datas = (head->nextPtr)->datas;
        head->nextPtr = (head->nextPtr)->nextPtr;
        //错误:head = head->nextPtr;
    }

    struct node *temp = head;//修改temp的nextPtr和datas会同步修改head的(因为指向的地址相同),但temp = a 不会反映到head
    while ((temp->nextPtr != ptr)&&(temp->nextPtr != NULL)){//为什么写 与?
        temp = temp->nextPtr;
    }
    temp->nextPtr = ptr->nextPtr;
}


//int main() {
//    int t = 0;
//    int value;
//
//    struct node *head = NULL;
//    head = createList();
//    visitList(head);
//
//    sortList(head);
//    printf("after sorting:");
//    visitList(head);
//
//    printf("please enter a number to search:");
//    fflush(stdin);//因为之前有过输入
//    scanf("%d",&t);
//    struct node *ptr;
//    ptr = searchList(head,t);
//    printf("%p\n",ptr);
//
//    printf("please enter a number and a position to add:");
//    fflush(stdin);
//    scanf("%d",&t);
//    insertList(head,2,1000);
//    visitList(head);
//
//    printf("please enter a number to delete:");
//    fflush(stdin);
//    scanf("%d",&value);
//    deleteList(head,value);
//    visitList(head);
//
//    return 0;
//}

