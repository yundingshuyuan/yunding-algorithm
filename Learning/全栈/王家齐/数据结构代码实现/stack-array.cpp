// 概念：栈;栈顶/栈底;
// 顺序栈
#include <iostream>
#include <vector>
#include <stack>
#define MaxSize 1000
using namespace std;

// ADT:置空栈;判断是否为空;出入栈;得到栈顶元素
// 尝试写了一个类
class stack_array{
private:
    //    struct Array{
    int stack_num;//当前元素数目
    int *stack_arr;//栈结构数组
    int max_num;//栈结构最大可容纳的数目
    //    };
public:
    void stack(){
        stack_num = 0;
        max_num = MaxSize;
        stack_arr = new int[max_num];//分配内存空间
        for (int i = 0; i < max_num; ++i) {
            stack_arr[i] = 0;
        }
    }
    void stack(int num){
        stack_num = 0;
        max_num = num;
        stack_arr = new int[max_num];
        for (int i = 0; i < max_num; ++i) {
            stack_arr[i] = 0;
        }
    }
    //置空栈
    void inistack(){
        for (int i = 0; i < stack_num; ++i) {
            stack_arr[i] = 0;
        }
        stack_num = 0;
    }
    // 判断是否为空
    bool empty(){
        if (stack_num != 0){
            return false;
        }else return true;
    }
    // 入栈
    bool push(int val){
        if (stack_num < max_num){
            stack_arr[stack_num++] = val;
            return true;
        }else{
            printf("error!no memory.");
            return false;
        }
    }
// 出栈
    bool pop(){
        if (stack_num > 0){
            stack_arr[--stack_num] = 0;
            return true;
        }else{
            printf("already empty.");
            return false;
        }
    }
//得到栈顶元素
    int getTop(){
        if(stack_num != 0){
            return stack_arr[stack_num-1];
        }else return -1;
    }
};


//int main(){
//    stack_array test;
//    test.stack(2);
//    test.inistack();
//    cout<<test.empty()<<endl;
//    int num;
//    cin>>num;
//    test.push(num);
//    cout<<test.getTop()<<endl;
//    test.push(3);
//    cout<<test.getTop()<<endl;
//    test.pop();
//    cout<<test.getTop()<<endl;
//}