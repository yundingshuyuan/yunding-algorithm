//线性表:由零个或多个数据元素构成的有限序列
#include <iostream>
#define MaxSize 1000
using namespace std;

// ADT:遍历;查找;增删
struct Array{
    int data[MaxSize];
    int length;
};

// 遍历
void traverse(Array *L){
    for (int i = 0; i < L->length; ++i) {
        cout<<L->data[i]<<" ";
    }
    cout<<endl;
}
// 按值查找
int SearchElem(Array *L,int value){
    for (int i = 0; i < L->length; ++i) {
        if(L->data[i]==value){
            return i;
        }
    }
    return -1;
};
// 插入元素
void InsertElem(Array *L,int value,int k){
    if(k<0 || k>=L->length){
        cout<<"invalid position";
        return ;
    }
    L->length++;
    for (int i = L->length-1; i > k; i--) {
        L->data[i]=L->data[i-1];
    }
    L->data[k]=value;
}
// 删除元素
void DeleteElem(Array *L,int k){
    if(k<0 || k >= L->length){
        cout<<"invalid position";
        return ;
    }
    for (int i = k; i < L->length-1; ++i) {
        L->data[i]=L->data[i+1];
    }
    L->length--;
}
//int main() {
//    Array L;
//    L.length=0;
//    for (int i = 0; i < 5; ++i) {
//        L.data[i]=i;
//        L.length++;
//    }
//    traverse(&L);
//    InsertElem(&L,10,3);
//    traverse(&L);
//    cout<<SearchElem(&L,10)<<endl;
//    DeleteElem(&L,3);
//    traverse(&L);
//    return 0;
//}
