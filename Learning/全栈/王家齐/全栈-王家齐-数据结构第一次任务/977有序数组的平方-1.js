/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var sortedSquares = function(nums) {
    for(let i=0;i<nums.length;i++){
        nums[i]*=nums[i]
    }
    for(let i=0;i<nums.length;i++){
        for(let j=i;j<nums.length-1;j++){
            if(nums[i]>nums[j+1]){
                let tem =nums[i]
                nums[i]=nums[j+1]
                nums[j+1]=tem
            }
        }
    }
    return nums
};