/**
 * @param {number} target
 * @param {number[]} nums
 * @return {number}
 */
 var minSubArrayLen = function(target, nums) {
    let lastlen = nums.length
    for(let i = 0;i<nums.length;i++){
        let sum = 0
        let len = 0
        // console.log('i:'+i)
        for(let j = i;j<nums.length;j++){
            sum += nums[j]
            len++
            // console.log('if前: '+'j:'+j+' sum:'+sum+' len:'+len)
            if(sum >= target){
                len = j - i + 1
                break
            }else if(sum < target && len == nums.length){
                return 0
            }
            // console.log('if后: '+'j:'+j+' sum:'+sum+' len:'+len)
        }
        // console.log('i len: '+len)
        // 收录标准
        if(len<lastlen&&sum>=target){
            lastlen = len
        }
    }
    return lastlen
};