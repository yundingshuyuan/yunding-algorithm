public class LoopList {

    private int value;

    private LoopList next;

    private boolean isover;

    //惯例函数
    LoopList(int value){
        this.value=value;
        this.isover = true;
    }
    LoopList(int value,LoopList next){
        this.value = value;
        this.next = next;
        this.isover = true;
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }
    public LoopList getNext() {
        return next;
    }
    public void setNext(LoopList next) {
        this.next = next;
    }
    public boolean isIsover() {
        return isover;
    }
    public void setIsover(boolean isover) {
        this.isover = isover;
    }

    //正文开始---------------------------------------------------------------------

    //增
    public void add(int value){
        LoopList tar = new LoopList(value,this);
        //找到尾巴
        LoopList current = this;
        while (!current.isIsover()){
            current= current.getNext();
        }
        current.setIsover(false);
        current.setNext(tar);
    }
    //删
    public void delete(int index) {
        LoopList tar = getIndex(index);
        if(tar.getNext()==null){
            System.out.println("cant delete because this is the only node");
        }else {
            tar.setValue(tar.getNext().getValue());
            tar.setNext(tar.getNext().getNext());
        }
    }
    //改
    public void set(int value,int index){
        LoopList tar = getIndex(index);
        tar.setValue(value);
    }
    //查
    public int select(int index) {
        return getIndex(index).getValue();
    }
    //工具方法 找到该位置上的节点
    private LoopList getIndex(int index){
        LoopList tar = this;
        if(tar.getNext()==null) return tar;
        for(int cnt=1;cnt<index; cnt++){
            tar=tar.getNext();
        }
        return tar;
    }

    public static void main(String[] args) {
        LoopList list = new LoopList(1);
        //测试增
        list.add(2);
        list.add(3);
        list.add(4);
        //测试删
//        list.delete(2);
        //测试改
//        list.set(222,2);
        //测试查
        System.out.println(list.select(2));

//        while (true){
//            System.out.print(list.getValue()+"    ");
//            list=list.getNext();
//        }
    }

}
