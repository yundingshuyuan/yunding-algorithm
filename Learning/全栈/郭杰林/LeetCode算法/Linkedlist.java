/*
 *@author LinDanDa
 */

public class Linkedlist {
    //链表值
    private int value;
    //指向下一个的指针
    private Linkedlist next;

    //增
    public void add(int value){
        //创建要增加到尾部的表
        Linkedlist newlist = new Linkedlist(value);
        //找到链表的最后一项
        Linkedlist current = this;
        while (current.getNext()!=null){
            current=current.getNext();
        }
        //完成添加
        current.setNext(newlist);
    }

    //删 (从1开始 1是第1项)
    public void delete(int tar){
        //找到要删除的项
        Linkedlist tarlist=getTarList(tar);
        tarlist.setValue(tarlist.getNext().getValue());
        tarlist.setNext(tarlist.getNext().getNext());
    }

    //改
    public void set(int tar,int value){
        Linkedlist tarList = getTarList(tar);
        tarList.setValue(value);
    }

    //查
    public int select(int tar){
        Linkedlist tarList = getTarList(tar);
        return tarList.getValue();
    }

    //工具方法 获取指定的节点
    private Linkedlist getTarList(int tar){
        Linkedlist tarlist=this;
        for(int cnt=1;cnt<tar;cnt++){
            tarlist=tarlist.getNext();
        }
        return tarlist;
    }


    //两种构造函数
    Linkedlist(int value){
        this.value=value;
        this.next=null;
    }

    Linkedlist(int value,Linkedlist next){
        this.value=value;
        this.next=next;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Linkedlist getNext() {
        return next;
    }

    public void setNext(Linkedlist next) {
        this.next = next;
    }

    public static void main(String[] args) {
        Linkedlist linkedlist = new Linkedlist(1);

        //增
        linkedlist.add(2);
        linkedlist.add(3);
        //删
//        linkedlist.delete(2);
        //改
        linkedlist.set(2,4);
        //查
        System.out.println(linkedlist.select(2));
    }
}
