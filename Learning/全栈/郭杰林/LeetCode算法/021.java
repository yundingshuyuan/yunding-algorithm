/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        if(head.next==null){
            return null;
        }else{
            ListNode current=head;
            int cnt=1;
            for(;current.next!=null;current=current.next,cnt++){}
            int tar=cnt-n+1;
            ListNode current2=head;
            for(int cnt2=1;cnt2!=tar;cnt2++,current2=current2.next){}
            if(current2.next==null){
                current2=null;
            }else{
                current2.val=current2.next.val;
                current2.next=current2.next.next;
            }
            return head;
        }


    }
}