package leetcode.Leetcode_704;

public class 二分查找 {
    public static void main(String[] args) {
        class Solution {
            public int search(int[] nums, int target) {
                int left = 0;
                int right = nums.length - 1;
                while(right >= left){
                    int mid = left + (right - left)/2;

                    if(target == nums[mid]){
                        return mid;
                    }else if(target > nums[mid]){
                        left = mid + 1;
                    }else if(target < nums[mid]){
                        right = mid - 1;
                    }
                }
                return -1;
            }
        }
    }
}

/*
double[] nums = {1,3,4,6,8,9,11,13};
double left = 0;
double right = nums.length - 1;
double mid = left + (right-left)/2;
System.out.println(nums[(int)mid]);//6
System.out.println(mid);//3.5 -> 3
之前忘了会直接取整，还用这个代码验算了一次……
*/