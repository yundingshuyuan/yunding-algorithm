import java.util.ArrayList;

public class myStack {
    //第一个入栈的序号
    private final int first;
    //最后一个入栈的序号
    private int last;
    //栈容量大小
    private int lenth;
    //栈数据
    private ArrayList<String> list;

    //惯例函数
    public myStack(int lenth) {
        this.first = 0;
        this.last = 0;
        this.lenth = lenth;
        this.list = new ArrayList<>();
    }
    public int getLast() {
        return last;
    }

    //入栈
    public void in(String value){
        if(this.last+1>this.lenth){
            System.out.println("out of bounds");
        }else {
            this.last++;
            this.list.add(value);
            System.out.println("add "+value+" in stack");
        }
    }
    //出栈
    public void out() {
        if (this.last==this.first) {
            System.out.println("nothing");
        }else {
            this.last--;
            String i = this.list.get(this.last);
            this.list.remove(this.last);
            System.out.println(i+" has out of stack");
        }
    }
    //展示栈
    public void showStack(){
        for (String i : this.list) {
            System.out.println(i);
        }
    }
    public String getlast(){
        if(this.last!=0) {
            return this.list.get(this.last - 1);
        }else {
            return "nothing";
        }
    }

    public static void main(String[] args) {
        myStack myStack = new myStack(10);
        myStack.in(String.valueOf(1));
        myStack.in(String.valueOf(2));
        myStack.in(String.valueOf(3));
        myStack.showStack();
        myStack.out();
        myStack.showStack();
        System.out.println(myStack.getlast());
    }
}
class Solution {
    public boolean isValid(String s) {
        String[] split = s.split("");
        myStack myStack = new myStack(9999);
        for (int i = 0; i < s.length(); i++) {
            switch (split[i]) {
                case "(":
                    myStack.in("(");break;
                case "[":
                    myStack.in("[");break;
                case "{":
                    myStack.in("{");break;
                case ")":if(myStack.getlast().equals("(")){
                    myStack.out();break;}else {return false;}
                case "]":if(myStack.getlast().equals("[")){
                    myStack.out();break;}else {return false;}
                case "}":if(myStack.getlast().equals("{")){
                    myStack.out();break;}else {return false;}
            }
        }
        if(myStack.getLast()!=0){
            return false;
        }else {
            return true;
        }
    }
}