var detectCycle = function(head) {
    // 第一步：判断是不是环形链表；快慢指针法；相遇就是环形；

    // 如果头指针的下一位为假，就是只有一个节点，就不是环形链表
    if(!head || !head.next)
    return null;

    let slow = head.next;
    let fast = head.next.next;
    let index2;
    let index1 = head;

    while(fast && fast.next) {
        fast = fast.next.next;
        slow = slow.next;
        // 确认存在环之后找入口
        if(fast == slow) {
            index2 = slow;
            while(index1  !== index2) {
                index1 = index1.next;
                index2 = index2.next;
            }
            return index2;
        }
    }
    return null

};