//生成链表结构

class LinkNode {
    constructor(val,next) {
        this.val = val;
        this.next = next;
    }
}

// 先确定环形链表的入口
if (!head || !head.next)
    console.log("null ");

let slow = head.next;
let fast = head.next.next;
let index2;
let index1 = head;
// 存放入口
let enter;

if (fast && fast.next) {
    fast = fast.next.next;
    slow = slow.next;
    // 确认存在环之后找入口
    if (fast == slow) {
        index2 = slow;
        while (index1 !== index2) {
            index1 = index1.next;
            index2 = index2.next;
        }
        console.log(index2);
    }
    enter = index2;
} else {
    console.log("null")
}

// 查
// 环只走一次即可
var find = function (head, val) {
    let code = head;
    if (code.val !== val) {
        code = code.next;
        if (code == enter) {
            // 第一次遇见入口
            code = code.next;
            // 第二次遇见入口还未中找到，就说明没有
            while (code == enter) {
                console.log("can't find this val!")
            }
        }
    }
    // 找到节点并输出
    else {
        console.log(code);
    }
}

// 增
var add = function (oldval,newval) {
    // 创建新节点元素
    let newcode = new LinkNode (newval);

    // 添加位置在头节点
    // 设置虚拟头节点
    if(head.val == oldval) {
    const cur = new ListNode(0, head);
    cur.next = newcode;
    newcode.next = head;
    newcode.val = newval;
    }

    let code = head;
    
    while (code.val == oldval)  {
        // 不添加在尾部(环连接的位置)
        if(code.next!==enter) {
            newcode.next = code.next;
            code.next = newcode;
        }
        // 添加在尾部
        else{
            code.next = newcode;
            newcode.next = enter;
        }
    }
}

// 删除
var remove = function (oldval,newval) {

    // 删除位置在头节点
    // 设置虚拟头节点
    const cur = new ListNode(0, head);
    if(head.val == oldval) {
        cur.next = head.next;
    }
    
    let code = cur;
    code = code.next;
    while (code.next.val == oldval)  {
        // 删除节点不在尾部
        if(code.next.next!==enter) {
            code.next = code.next.next;
        }
        //在尾部
        else{
            code.next = enter;
        }
    }
}

// 改
var change = function(val,newval) {
    
    if(node.val = val) {
        node.val = newval;
    }
    else {
        console.log("不存在需要被替换的元素");
    }
}
