//生成链表结构
class LinkNode {
    constructor(val,next) {
        this.val = val;
        this.next = next;
    }
}

var MyLinkedList = function () {
    this.size = 0;
    this.tail = null;
    this.head = null;
}

// 查
var find = function (val) {
    // 从头节点开始遍历
    let curNode = this.head;
    while (curNode&&curNode.val) {
        if(curNode.val = val) {
            console.log(curNode);
        }
        // 一直遍历结束,元素可能出现两次
        curNode = curNode.next;
    }
    // 元素不存在输出null
    console.log("null");
}


// 增
var add = function(oldval,newval) {
    // newval为新增的节点元素
    let newNode = new LinkNode (newVal);
    // 通过元素找到旧节点
    let oldNode = this.find(oldval);
    if(!oldNode) {
        // 没有找到元素对应的旧节点
        console.log("未找到对应的位置");
    }
    else if(oldNode.next){
        // 新节点覆盖旧节点
        newNode.next = oldNode.next;
        // 将旧节点放在新节点的前面;就是就节点的下一位等于新节点
        oldNode.next = newNode;
    }
    else {
        newNode.next = null;
        oldNode.next = newNode;
    }
}

// 删
var remove = function(val) {
    let reNode = this.find(val);
    // 未找到节点
    if(!reNode) {
        console.log("未找到对应元素");
    }
    else {
        (reNode-1).next = reNode.next;
    }
}

// 改
var change = function(val,newval) {
    let node = this.find(val);
    if(node) {
        node.val = newval;
    }
    else {
        console.log("不存在需要被替换的元素");
    }
}


