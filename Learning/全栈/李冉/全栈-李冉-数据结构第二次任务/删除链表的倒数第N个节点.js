var removeNthFromEnd = function(head, n) {
    // 双指针
    // 1.虚拟头节点
    let ret = new ListNode(0,head);
    // 快慢都指向头节点
    slow = fast = ret;
    // 让快指针先走n+1步
    // 数学计算,就是(n--成立)步
    while(n--) {
        fast = fast.next;
    }
    while (fast.next!==null) {
        fast = fast.next;
        slow = slow.next;
    };
    // 要删除的就是当fast为null的时候slow的下一个节点
    slow.next = slow.next.next;
    // 去除虚拟头节点
    return ret.next;

};