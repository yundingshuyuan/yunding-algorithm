var search = function(nums, target) {
    // 左右界限
    let left = 0;
    let right = nums.length-1;
    // 左闭右闭
    while (left<=right) {
        // let middle = left + Math.floor((right - left)/2);
        let middle = Math.floor((right+left)/2);
        if(nums[middle]>target) {
            // 比中间数大,改变右边界为中间下标-1;实现范围缩小
            right = mid-1;
        }
        else if (nums[middle]<target) {
            // 比中间数小,改变左边界为中间下标+1;实现范围缩小
            left = middle +1;
        }
        // 直到确定数字下标
        else {
            return middle;
        }
    }
    // 直到右边界于左边界重合也未找到，就输出-1
    return -1;
}