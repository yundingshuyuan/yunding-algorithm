/**
 * @param {string[]} tokens
 * @return {number}
 */
 var evalRPN = function(tokens) {
    // 逆波兰表达式：是一种后缀表达式，所谓后缀就是指算符写在后面。


    // Map类型是键值对的有序列表，而键和值都可以是任意类型
    // 设定运算符和他们对应的计算方法
    const s = new Map([
        ["+", (a, b) => a * 1  + b * 1],
        ["-", (a, b) => b - a],
        ["*", (a, b) => b * a],
        ["/", (a, b) => (b / a) | 0]
    ]);
    const stack = [];
    // 遍历
    for (const i of tokens) {
        // has() 方法返回一个布尔值来指示对应的值value是否存在Set对象中。
        // 如果不存在，说明该位置是一个数字，将数字推入栈内
        if(!s.has(i)) {
            stack.push(i);
            // 结束下面的内容进行下一个元素的判断
            continue;
        }
        // 在s中说明为运算符，获取用来计算
        // .get()获取
        // 计算结果要推入栈内
        stack.push(s.get(i)(stack.pop(),stack.pop()))
    }
    // 最后输出结果
     return stack.pop();
};