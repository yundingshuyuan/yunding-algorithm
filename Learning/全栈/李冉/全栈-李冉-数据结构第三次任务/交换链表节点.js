var swapNodes = function(head, k) {

    // 设置计数器,用来判断当前位置是不是到了指定位置
    let count = 1
    // 设置遍历指针，从头开始，方便确定链表什么时候到了末尾（快指针）
    let p = head
    // 定义第K个节点
    let front = null
    // 定义倒数第K个节点（慢指针）
    let back = head
    while (p) {
      // count = k 是， p为第 K 个节点
      if (count === k) {
        // 找到第K个节点
        front = p
      }
      // 当 count > k 时， back 指针开始移动
      // 当count大于k时，快指针和慢指针同步走，当快指针走到末尾null时，慢指针也就到了倒数第k位
      if (count > k) {
        back = back.next
      }
      p = p.next
      count++
    }
    // 交换节点的数值
    let temp = front.val
    front.val = back.val
    back.val = temp
  
    // 完成头节点
    return head
  };