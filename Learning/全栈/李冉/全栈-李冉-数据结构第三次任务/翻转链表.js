var reverseList = function(head) {
// 改变链表next的指向实现翻转;1-2-3-4-5，先将2换到第一个，变为：2-1-3-4-5。然后将3换到第一个，3-2-1-4-5。以此类推。
// 其中，p始终指向1，q指向p的下一个，即本轮需要换到第一个的数。
var cur = head;
var p = cur;
var q = null;

// 先判断链表是否为空
if(p == null) {
    return null;
}
// 不到最后一位时
while(p.next !== null) {
    // q为p的下一位
    q = p.next;
    // p的下一位为q的下一位，即p的下一位就是原来P的第三位，此时已经将p从第一位移动到第二位
    p.next = q.next;
    // q的下一位是头节点，q变成了虚拟头节点
    q.next = cur;
    // 最后让头节点为q，就把第二位变成了头节点,实现互换
    cur = q;
}
return cur;
};