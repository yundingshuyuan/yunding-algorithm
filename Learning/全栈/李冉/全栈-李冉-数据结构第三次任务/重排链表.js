var reorderList = function (head) {
    // 思路：两个指针p和q，p走一步，q走两步，当q走到末尾，p正好走到中间将链表一分为二；
    // 后半链表翻转，与前半链表进行组合

    // 先判断
    if (!head || !head.next || !head.next.next) return head;

    // 获取中间节点
    let fast = slow = head;
    while (fast.next !== null && fast.next.next !== null) {
        fast = fast.next.next;
        slow = slow.next;
    }


    //   依据中间节点将链表分成两个部分，后半部分进行翻转
    let pre = null;
    let cur = slow;
    while (cur !== null) {
        // 找到后一位
        let nextNode = cur.next;
        // 翻转
        cur.next = pre;
        // 集体后移进行下一次翻转
        pre = cur;
        cur = nextNode;
    }

    // 前后拼接
    let next1 = null;
    let next2 = null;
    let head1 = head;
    // 翻转后pre是后半部分链表的头节点
    let head2 = pre;

    while (head1 !== null && head2 !== null) {
        next1 = head1.next;
        next2 = head2.next;

        // 将后半部分链表插入
        head1.next = head2;
        // 插入后后移
        head1 = next1;

        // 将新的head1(就是前半链表的第二个节点)插入到链表二的头节点的后面
        head2.next = head1;
        // 将头节点后移，进行下一次插入
        head2 = next2;
    }
};