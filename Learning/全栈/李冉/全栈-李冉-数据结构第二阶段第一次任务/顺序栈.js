    // 顺序栈是栈的顺序实现。 顺序栈是指利用 顺序存储结构 实现的栈。 
// 采用地址连续的存储空间（数组)依次存储栈中数据元素，
// 由于入栈和出栈运算都是在栈顶进行，而栈底位置是固定不变的，
// 可以将栈底位置设置在数组空间的起始处；栈顶位置是随入栈和出栈操作而变化的，
// 故需用一个整型变量top来记录当前栈顶元素在数组中的位置。

// 顺序栈类
// 因为要开辟地址储存栈内数据，可以写一个数组来开辟地址

class Stack {
    constructor(maxLength) {
        this.arr = new Array(maxLength);
        this.top = -1;
        // this.size = maxLength;
    }
    // 推入元素
    push(value) {
        if (this.top === this.maxLenrth - 1) {
            console.log("stack is empty")
        }
        // 如果栈内储存元素不为0
        // 栈顶标记+1,并将元素放入数组
        this.top++;
        this.arr[this.top] = value;
    }

    // 推出栈顶元素
    pop() {
        if (this.top === -1) {
           console.log("error")
        }
        const topElement =  this.arr[this.top];
        // 每次移除栈顶元素，栈顶元素标记-1，更新为新的栈顶元素
        this.top--;
        // 移除之后，栈内元素的数量减少1
        this.arr.length--;
        // console.log(topElement)
        return topElement;
    }
    // 获取栈内元素的个数
    getLength() {
        return this.arr.length;
        // console.log(this,arr.length)
    }

    // 输出栈内元素
    out() {
        for(var i = 0;i<this.arr.length;i++) {
            console.log(this.arr[i]);
        }
    }
}
const test = new Stack(3);
test.push(9);
test.push(8);
test.push(7);

console.log('栈内元素个数：'+test.getLength());
console.log("栈内元素为：")
test.out();
console.log('移出栈顶元素 :'+test.pop());
console.log("移除后栈内元素为：")
test.out()