var isValid = function(s) {
    // s为需要检验的字符串
    let stack = [];
    for(var i =0;i<s.length;i++) {
        let c = s[i];
         // 依据字符串的每一个元素将其对应的另一半括号输入到栈中
    switch(c) {
        case "(" :
        stack.push(")");
        break;

        case "{" :
        stack.push("}");
        break;

        case "[" :
        stack.push("]");
        break;

    // 先根据左括号将右括号输入栈内，当出现有括号的时候，进入default，进行输出，如果此时的右括号与栈内输出的右括号不一致，或者出现多余或者缺少项的情况，说明该括号字符串有问题
        default :
        if(c!==stack.pop()) {
            return false;
        }
    }
}
   
    return stack.length == 0;
};
