/*
保存一个left值
每次从left开始遍历
找到一个i是的从left到i的值和为target
每次遍历结束以后右移left
直至left指向最后一个值
*/

/**
 * @param {number} target
 * @param {number[]} nums
 * @return {number}
 */
var minSubArrayLen = function (target, nums) {
    let right = 0, left = 0, count = 0;
    while (left < nums.length) {
        for (let i = left; i < nums.length; i++) {
            nowSum = nowSum + nums[i];
            if (nowSum >= target) {
                count = count === 0 ? i - left + 1 : Math.min(count, i - left + 1);
                break;
            }
        }
        left++;
        nowSum = 0;
    }
    return count;
}