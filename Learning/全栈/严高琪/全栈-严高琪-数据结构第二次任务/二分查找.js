var search = function(nums, target) {
    /*let l = 0, r = nums.length-1;
    //区间[l，r）
    while(l<r){
        let mid = (l+r)>>1;
        if(nums[mid]===target) return mid;
        let isSmall = nums[mid]<target;
        l = isSmall ?mid+1:l;
        //所以mid不会取到
        r = isSmall? r:mid;
    }
    return -1;*/
    let left = 0, right = nums.length - 1;
    while (left <= right) {
        let mid = Math.floor(left + (right - left) / 2);
        if (target === nums[mid]) {
            return mid;
        } else if (target > nums[mid]) {
            left = mid + 1;
        } else if (target < nums[mid]) {
            right = mid - 1;
        }
    }
    return -1;
};