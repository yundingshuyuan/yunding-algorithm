var removeNthFromEnd = function(head, n) {
    let cur = head
    let param = head
    if(head.next==null) return null 
    n = n + 1 //找到删除的前一个节点
    while(cur){
        if(n == 0){
            param = param.next //找到要删除的前一个节点
        }else{
            n--
        } 
        cur = cur.next
    }
    if(head == param&&n==1){//如果n == 1 说明找到删除的节点为头节点，且需要后移
        head = head.next
    }else{//不是的话删除当前节点
        param.next = param.next.next
    }
    return head
};
