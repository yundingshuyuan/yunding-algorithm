#include<stdio.h>
#include<math.h> //fmin()快速比较两数大小 ，返回小的那一个 
#include<stdlib.h>//malloc所在头文件 


/*target:一个正整数 
	nums：原始数组
	numsSize:给定数组的全长 
*/ 
// 原型声明：返回长度最小的子数组的长度 
int minSubArrayLen(int target, int* nums, int numsSize);

int main(){
	int target,*nums,numsSize,minLen;
	
	//获取target
	printf("please input the target:");
	scanf("%d",&target);
	//获取原始数组长度numsSize
	printf("please input the numsSize:");
	scanf("%d",&numsSize);
	//申请数组空间
	nums=(int*)malloc(numsSize * sizeof(int));
	//获取原始数组
	printf("please input the value of %d elements\n",numsSize); 	
	//使用数组空间 
	for(int i=0;i<numsSize;i++){ 
		printf("第%d个元素为",i+1);
		scanf("%d",&nums[i]);
		} 
	//调用函数 
	minLen=minSubArrayLen(target,nums,numsSize);
	printf("子数组的最小长度为%d",minLen);
	//最后释放数组空间
	free(nums);
	return 0;
}

int minSubArrayLen(int target, int* nums, int numsSize) {
    if (numsSize == 0) {
        return 0;
    }
    int minLen = INT_MAX;
    for (int i = 0; i < numsSize; i++) {
    	//初始化sum 当 sum >= target 时更新一次minLen 
        int sum = 0;
        for (int j = i; j < numsSize; j++) {
            sum += nums[j];
            if (sum >= target) {
                minLen = fmin(minLen, j - i + 1);
                break;
            }
        }
    }
    return minLen == INT_MAX ? 0 : minLen;
}

