#include<stdio.h>
#include<stdlib.h> 
/*
 nums:原始数组
 numsSize:原始数组长度（元素个数） 
*/

//原型声明 !!要加； 
int* sortedSquares(int *nums,int numsSize);

int main(){
	int *nums,numsSize,*newnums;

	//获取原始数组长度numsSize
	printf("please input the numsSize:");
	scanf("%d",&numsSize);
	//申请数组空间
	nums=(int*)malloc(numsSize * sizeof(int));
	//获取原始数组
	printf("please input the value of %d elements\n",numsSize); 	
	//使用数组空间 
	for(int i=0;i<numsSize;i++){ 
		printf("第%d个元素为",i+1);
		scanf("%d",&nums[i]);
		}
	//打印原始数组
	printf("您定义的数组为：\n");
	for(int i=0;i<numsSize;i++){
		printf("%5d",nums[i]);
	} 
	//调用函数 
	newnums=sortedSquares(nums,numsSize);
	printf("\n排序后的数组为：\n");
	for(int i=0;i<numsSize;i++){
		printf("%5d",newnums[i]);
	} 
}

int* sortedSquares(int *nums,int numsSize){

	//先判断numsSize是否为0 
	if(numsSize==0){
		return 0;
	}
	//将每项平方
	for(int i=0;i<numsSize;i++){
		nums[i]=nums[i]*nums[i];
	} 
	//对新数组进行冒泡排序（升序）
	for(int i=0;i<numsSize;i++){
		for(int j=0;j<numsSize-i-1;j++){
			if(nums[j]>nums[j+1]){
				int temp=nums[j+1];
				nums[j+1]=nums[j];
				nums[j]=temp;
			}
		}
	} 
	return nums;
}

