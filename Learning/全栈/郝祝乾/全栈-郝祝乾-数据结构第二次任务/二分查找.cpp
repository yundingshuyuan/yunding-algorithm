int search(int* nums, int numsSize, int target){
    if(nums[0]>target||nums[numsSize-1]<target)
	{
		return -1;
	}
    else
    {
        int c,b,a;
        a=numsSize-1;
        b=(nums[0]==target)?0:a;
        c=0;
        while(nums[b]!=target)
        {
            b=((a+c)%2?(a+c+1)/2:(a+c)/2);
            if(nums[b]<target)
            {
                c=b;
            }
            else
            {
                a=b;
            }
            if(b==0 || (nums[b-1]<target && nums[b]>target ))
            {
				return -1;
			}
        }
        return b;
    }
}