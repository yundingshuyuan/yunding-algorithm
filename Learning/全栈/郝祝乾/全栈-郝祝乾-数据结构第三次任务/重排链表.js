var reorderList = function(head) {
    if(!head.next) return head;
    let slow = head, fast = head.next; 
    while(fast && fast.next) {
        slow = slow.next;
        fast = fast.next;
        if(fast.next) {
            fast = fast.next;
        }
    }
    let newHead = null, p = slow.next, q;
    slow.next = null;
    while(p) {
        q = p;
        p = p.next;
        q.next = newHead;
        newHead = q;
    }

    let resultHead = new ListNode(0), now = resultHead, flag = true;
    slow = head, fast = newHead;
    while(slow || fast) {
        if(flag) {
            now.next = slow;
            slow = slow.next;
        } else {
            now.next = fast;
            fast = fast.next;
        }
        now = now.next;
        flag = !flag;
    }
    return resultHead.next;
};