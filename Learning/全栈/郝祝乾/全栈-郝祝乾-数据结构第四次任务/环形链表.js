/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
 var hasCycle = function(head) {
    if(head==null || head.next==null) 
        return false
    let h = head;
    while(h.next!=null){
    if(h.if==true){
        return true;
    }
    if(h.next==null){
        break;
    }
    h.if=true;
    h=h.next;
    }
    return false
};