//滑动窗口
int minSubArrayLen(int s, int *nums, int numsSize) {
    if (numsSize == 0) {
        return 0;
    }
    int ans = INT_MAX;
    int start = 0, end = 0;
    int sum = 0;
    while (end < numsSize) {
        sum += nums[end];
        while (sum >= s) {
            ans = fmin(ans, end - start + 1);
            sum -= nums[start];
            start++;
        }
        end++;
    }
    return ans == INT_MAX ? 0 : ans;
}
//滑动窗口优化
int minSubArrayLen(int target, int* nums, int numsSize){
	if(numsSize == 0)
		{return 0;}
	int ans = INT_MAX;
	int start = 0,end = 0;
	    int sum = 0;
	    while (end<numsSize)
	    {
	        sum +=nums[end];
	        if(sum >= target)
	        {
	            ans = end - start + 1;
	            end++;
	            break;
	        }
	        end++;
	    }
	    end--;
	    while(end<numsSize)
	    {
		    while(start<=end)
		    {
		    	sum -=nums[start];
		    	start++;
		    	if(sum >= target)
		        {
		            ans = end - start + 1;
		        }
		        else{
		        	
		        	break;
				}
			}
			end++;
			if(end<numsSize)
			{
				sum +=nums[end];
			}
			else
			{
				break;
			}
		}
	    return ans == INT_MAX ? 0 : ans ;
	}
