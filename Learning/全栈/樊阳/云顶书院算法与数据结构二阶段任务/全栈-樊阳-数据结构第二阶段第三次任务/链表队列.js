/* 队列是只允许在一端进行插入操作，而在另一端 进行删除操作的线性表。与栈相反，队列是一 种先进先出的线性表。
与栈相同的是，队列也是一 种重要的线性结构，实现一个队列同 样需要顺序表或链表作为基础。 */
class Node {
    constructor(data) {
        this.data = data;
        this.next = null;
    }
}
class LinkedQueue {
    constructor() {
        this.length = 0; //长度
        this.head = null; //队头
    }
    //入队：队尾添加元素
    push(...datas) {
        for (let i in datas) {
            let newNode = new Node(datas[i])
            let cur = this.head;
            //空链队
            if (cur === null) {
                this.head = newNode;
            } else {
                //获取最后一个节点
                while (cur && cur.next) {
                    cur = cur.next;
                }
                cur.next = newNode;
            }
            this.length++;
        }
    }
    //出队：队头删除节点，并返回会删除节点
    pop() {
        let cur = this.head;
        //空队列
        if (cur === null) {
            return false
        } else {
            this.length--;
            this.head = cur.next;
            cur.next = null;
            return cur
        }
    }
    //返回当前队长度
    getLength() {
        return this.length;
    }
    //清空队列
    clear() {
        this.head = null;
        this.length = 0;
    }
}
let queue = new LinkedQueue();
queue.push(1, 2, 3);
console.log(queue.pop());
console.log(queue.getLength())