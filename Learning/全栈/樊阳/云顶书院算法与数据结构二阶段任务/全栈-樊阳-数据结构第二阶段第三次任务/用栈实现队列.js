class MyQueue {
    constructor() {
        this.stack1 = []; //存储数据
        this.stack2 = []; //和stack1结合操作数据
    }
    //入队
    push(x) {
        this.stack1.push();
    }
    //出队
    pop(){
        while(this.stack1.length!==0){
            this.stack2.push(this.stack1.pop());
        }
        let pop = this.stack2.pop();
        while(this.stack2.length !== 0){
            this.stack1.push(this.stack2.pop());
        }
        return pop;
    }
    //返回队首元素
    peek(){
        return this.stack1[0];
    }
    //判断是队列是否为空
    empty(){
        return this.stack1.length===0?true: false;
    }
}