/*
数据结构和算法是脱离语言的，比如pop，push在js中可以使用，但是其他的语言也有吗？
不一定，但是都可以通过数据结构和算法写出其功能
    实现一个栈结构/符合LIFO的原则，需要对插入的数据和删除的数据功能进行限制
    实现以下的方法
    push（eles）:添加一个或者多个元素到栈顶
    pop: 移除栈顶的元素，同时返回被移除的元素
    peek:返回栈顶的元素，不对栈本身做任何修改
    isEmpty：判断栈顶的元素，不对栈本身做任何修改
    getSize：返回栈里面的元素个数，和数组的length属性类似
    clear：移除栈元素里边的所有元素
    toString：将栈里边的内容通过逗号字符串进行拼接成一个字符串
 */
class ArrayStack {
    constructor() {
        this.size = 0; //栈的长度（其实根据代码理解为计数器更合适）
        /* 简单说明一下这里为什么我想拿对象而不是数组存储数据，因为拿数组的话，
        栈的长度就等于数组长度，每次push或pop就不需要计算栈的长度，从而构造器里size属性基本失去意义了 */
        this.items = {}; //栈的数据仓库
    }
    push(...elements) {
        //每个参数代表一个元素
        for (let i = 0; i < elements.length; i++) {
            this.items[i] = elements[i]; //这里用到了对象的解构语法
            this.size++;
        }
    }
    getSize() {
        return this.size
    }
    isEmpty() {
        return !this.size
    }
    pop() {
        //判断是否是空栈
        if (this.isEmpty()) {
            return undefined;
        }
        this.size--;
        let removeElement = this.items[this.size]; //获取栈顶的值
        //delete 删除对象的属性
        delete this.items[this.size];
        return removeElement;
    }
    peek() {
        //判断是否为空栈
        if (this.isEmpty()) {
            return undefined;
        }
        return this.items[this.size - 1];
    }
    clear() {
        while (!this.isEmpty()) {
            this.pop();
        }
    }
    toString() {
        let result = '';
        if (this.isEmpty()) {
            return result;
        }
        for (let i = 0; i < this.size; i++) {
            result += `${this.items[i]},`;
        }
        //
        return result.substring(0, result.length - 1);
    }
}
let stack = new ArrayStack();
stack.push(1, 2, 3, 4, 5);
stack.pop();
console.log(stack.toString());
console.log(stack);