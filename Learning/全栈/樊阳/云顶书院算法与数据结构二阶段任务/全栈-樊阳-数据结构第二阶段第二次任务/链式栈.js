class Node {
    constructor(data) {
        this.data = data;
        this.next = null;
    }
}
class LinkedStack {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    //获取栈顶节点
    getLastNode() {
        let cur = this.head;
        while (cur && cur.next) {
            cur = cur.next;
        }
        return cur;
    }
    //push入栈
    push(...data) {
        for (let i in data) {
            this.size++;
            let newNode = new Node(data[i]);
            let lastNode = this.getLastNode();
            // 空链表
            if (lastNode === null) {
                this.head = newNode;
            } else {
                lastNode.next = newNode;
            }
        }
    }
    //pop出栈并返回出栈元素
    pop() {
        let lastNode = this.getLastNode();
        //空链表
        if (lastNode === null) {
            return lastNode;
        } else if (this.head === this.getLastNode()) { //单节点
            this.size--;
            this.head = null;
            return lastNode;
        } else {
            this.size--;
            //获取倒数第二个节点
            let nextToLastNode = this.head;
            while (nextToLastNode.next !== this.getLastNode()) {
                nextToLastNode = nextToLastNode.next;
            }
            nextToLastNode.next = null;
            return lastNode;
        }
    }
}
let stacklink1 = new LinkedStack();
stacklink1.push(1, 2, 3, 4);
console.log(stacklink1.pop());
console.log(stacklink1.head);
console.log(stacklink1.pop());