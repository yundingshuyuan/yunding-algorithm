// 分析：题目要求求字符串的计算值，所以得用后缀表达式；
// 然后得知道后缀表达式（逆波兰式）计算规则：遍历字符串，遇见数字入栈，遇见符号出栈两次，
// 拿到两个数，第一个出栈的数放右边，第二出栈的放左边，中间放符号进行计算，计算完成的数入栈放回直到遍历完，计算完成；
//分析后直接上手
var evalRPN = function (tokens) {
    let stack = [];
    for (let i in tokens) {
        //遇见数字入栈
        if (tokens[i] >= -200 && tokens[i] <= 200) {
            stack.push(+tokens[i]);
        } else { //遇见符号出栈两次
            let right = stack.pop();
            let left = stack.pop();
            let result = 0;
            switch (tokens[i]) {
                case "+":
                    result = left + right;
                    break;
                case "-":
                    result = left - right;
                    break;
                case "*":
                    result = left * right;
                    break;
                case "/":
                    result = Math.trunc(left / right);
                    break;
            }
            stack.push(result);
        }
    }
    return stack[0];
};