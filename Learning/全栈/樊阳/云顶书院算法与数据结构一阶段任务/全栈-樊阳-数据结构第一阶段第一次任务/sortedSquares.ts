//参考sort函数用法详解
function sortNum(a: number, b: number): number{
    return a-b;
}
function sortedSquares(nums: number[]): number[] {
    nums=nums.map(v=>v*v);
    return nums.sort(sortNum);
};
console.log(sortedSquares([-4,-1,0,3,10]));
