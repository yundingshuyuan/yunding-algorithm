//参考了滑动窗口
function minSubArrayLen(target: number, nums: number[]): number {
    let l = 0, r = 0
    let sum = 0, ans = Number.MAX_SAFE_INTEGER
    while(r < nums.length){
        sum += nums[r++]
        while(sum >= target){
            ans = Math.min(ans, r-l)
            sum -= nums[l++]
        }
    }
    return ans === Number.MAX_SAFE_INTEGER ? 0 : ans
}
