class Node {
    constructor(data) {
        this.data = data;
        this.next = null;
    }
}
class circleList {
    constructor(head = null) {
        this.head = head;
        this.length = 0;
    }
    //查找节点
    find(data) {
        let cur = this.head;
        let lastNode = this.findLast();
        while (cur && cur.data !== data) {
            if (lastNode === cur) {
                cur = null;
                break;
            }
            cur = cur.next;
        }
        return cur;
    }
    //获取最后一个节点，尾节点的索引等于链表长度
    findLast() {
        let cur = this.head;
        //节点索引
        let index = 1;
        while (cur && index !== this.length) {
            cur = cur.next;
            index++;
        }
        return cur;
    }
    // 添加节点
    append(data) {
        let newNode = new Node(data);
        //空链表
        if (this.findLast() === null) {
            this.head = newNode;
        } else {
            this.findLast().next = newNode;
        }
        this.length++;
        //最后一个节点指向第一个节点，实现环形链表
        this.findLast().next = this.head;
    }
    //删除节点
    //先用写好的find()方法找到要删除的节点，再用写好的findLast()方法找到最后一个节点。找到要删除的节点后，再
    // 次从头结点开始遍历，直到找到要删除节点的前一个节点。接下来就又要分情况考虑了。
    //1.当待删除的节点是第一个节点时，如果此时单向循环链表只有一个节点，直接将此单向循环链表置空即可。
    //2.当待删除的节点是第一个节点时，且此时单向循环链表不仅只有一个节点时，此时将头结点的next指针指向待删除节点的下一个节点，并将最后一个节点指向待删除节点的下一个节点
    //3.除了前面的两种情况之外，只要将待删除节点的前一个节点next指针指向待删除节点的后一个节点
    remove(data) {
        let curNode = this.find(data); //待删除节点
        if (curNode === null) {
            return console.log("所要删除的节点不存在");
        }
        let lastNode = this.findLast(data); //最后一个节点
        let preCurNode = this.head;
        //找到待删除节点的前一个节点
        while (preCurNode.next !== curNode) {
            preCurNode = preCurNode.next;
        }
        if (curNode === this.head) { //待删除节点是第一个节点的时候
            //只有一个节点的时候
            if (this.length === 1) {
                this.head = null;
            } else {
                this.head = curNode.next;
                lastNode.next = curNode.next;
                curNode.next = null;
            }
        } else {
            preCurNode.next = curNode.next;
        }
        this.length--;
    }
    //更新节点
    updata(oldData, newData) {
        let oldNode = this.find(oldData);
        if (oldNode === null) {
            return console.log("所要更新的节点不存在");
        } else {
            oldNode.data = newData;
        }
    }
    //显示节点数据
    list() {
        let result = '';
        let curNode = this.head;
        let lastNode = this.findLast();
        while (curNode && curNode !== lastNode) {
            result += curNode.data;
            curNode = curNode.next;
            if(curNode) {
                result += '——>';
            }
        }
        //加上最后一个节点的数据
        result+=curNode.data;
        console.log(result);
    }
}

export let list = new circleList();
list.append(1);
list.append(2);
list.append(3);
list.append(4);
list.updata(4,5);
list.remove(5);
list.list();