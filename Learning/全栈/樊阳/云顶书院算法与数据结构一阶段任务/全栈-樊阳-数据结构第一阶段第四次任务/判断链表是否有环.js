/* leetCode 141  */
// 给你一个链表的头节点 head ，判断链表中是否有环。
// 如果链表中存在环 ，则返回 true 。 否则，返回 false 。 
/* 解法：双指针 */
// 每当慢指针 slow 前进一步，快指针 fast 就前进两步。
// 如果 fast 最终遇到空指针，说明链表中没有环；
// 如果 fast 最终和 slow 相遇，那肯定是 fast 超过了 slow n 圈，说明链表中含有环。
import {list} from './环形链表.js'
var hasCycle = function(head) {
    let fast=head;
    let slow = head;
    //非环形链表，fast指针一定会遇到null
    while(fast&&fast.next){
        fast=fast.next.next;
        slow=slow.next;
        if(slow===fast){
            return true;
        }
    }
    return false;
};
console.log(hasCycle(list.head))