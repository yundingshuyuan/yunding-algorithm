import {
    LinkedList
} from './单向链表.js'
//创建链表
let list = new LinkedList();
list.append(1);
list.append(2);
list.append(3);
list.append(4);
list.append(5);
list.list();
/* 反转链表 */
//思路：1.定义一个变量ret存储反转后的链表，定义一个cur指针指向当前链表的头节点
//2.当前节点的指针指向ret；
//3.更新ret
//4.cur指针指向下一个节点，继续遍历
//5. 需要提前定义一个temp变量代表当前节点的指针，因为操作2将当前节点指向下一个节点的指针改变了
let reverseList = function (listHead) {
    let cur = listHead;
    let temp = null;
    let ret = null;
    while (cur) {
        temp = cur.next;
        console.log(temp);
        cur.next = ret;
        ret = cur;
        cur = temp;
    }
    return ret;
};
let ret = new LinkedList(reverseList(list.head));
ret.list();