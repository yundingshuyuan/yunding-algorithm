// 给定一个单链表 L 的头节点 head ，单链表 L 表示为：

// L0 → L1 → … → Ln - 1 → Ln
// 请将其重新排列后变为：

// L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
// 不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。
import {
    LinkedList
} from './单向链表.js'
//创建链表
let list = new LinkedList();
list.append(1);
list.append(2);
list.append(3);
list.append(4);
list.append(5);
list.list();
//分析：从头取一个节点，再从尾部取一个节点，
// 然后再回到头部取头二节点，再从尾部取尾二节点
//依次重复下去，直到到中间把整个链表里面的节点都放到一个新的链表里
var reorderList = function (head) {
    //先把链表里的节点放入到线性表（数组）里面
    const list = [];
    while (head) {
        list.push(head);
        head = head.next;
    }
    //定义一个返回的链表
    let hair = new LinkedList();
    let res = hair;
    //然后通过双指针头尾夹逼来生成我们最终重排的链表
    for (let i = 0, j = list.length-1; i <= j; i++, j--) {
        //存前面的节点
        let cur = list[i];
        res.next = cur;
        cur.next = null;
        res = cur;
        //存后面的节点：条件i和j不指向同一个节点，避免节点重复
        if (i !== j) {
            cur = list[j];
            res.next = cur;
            cur.next = null;
            res = cur;
        }
    }
};
reorderList(list.head);
list.list();