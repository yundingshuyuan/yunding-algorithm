import {
    LinkedList
} from './单向链表.js'
//创建链表
let list = new LinkedList();
list.append(1);
list.append(2);
list.append(3);
list.append(4);
list.append(5);
list.list();
/* 交换链表中的节点 */
let swapNodes = function (head, k) {
    let cur = head;
    //获取链表的长度
    let length = 0;
    while (cur) {
        length++;
        cur = cur.next;
    }
    //当数组长度为0或者为1的时候，链表不会改变
    if (length === 0 || length === 1) {
        return head;
    }
    //获取左节点
    let left = head;
    for (let i = 0; i < k - 1; i++) {
        left = left.next;
    }
    //获取右节点
    let right = head;
    for (let i = 0; i < length - k; i++) {
        right = right.next;
    }
   // 交换节点的值
    let temp = left.data;
    left.data = right.data;
    right.data = temp;
};
swapNodes(list.head, 2);
list.list();