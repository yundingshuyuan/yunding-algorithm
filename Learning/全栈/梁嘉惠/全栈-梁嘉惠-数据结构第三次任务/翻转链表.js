var reverseList = function(head) {
    if(!head) return head;
    let temp = null;//用来存储当前节点
    let pre = null; 
    let cur = head;
    while(cur) {
        temp = cur.next;
        //进行交换操作
        cur.next = pre;
        pre = cur;
        cur = temp;
    }
    return pre;
};