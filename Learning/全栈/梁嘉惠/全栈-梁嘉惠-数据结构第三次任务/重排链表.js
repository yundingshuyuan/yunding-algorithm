var reorderList = function(head) {
    if (!head) return head;
    let fast = head;
    let slow = head;
    
    // 首先找到中间节点
    while (fast&& fast.next) {
      slow = slow.next;
      fast = fast.next.next;
    }
    //将前后链表断开
    let h1 = head;
    let h2 = slow.next;
    slow.next = null;
    
    // 翻转后半段链表
    let cur = h2;
    let pre = null;
    while (cur) {
      let next = cur.next;
      cur.next = pre;
      pre = cur;
      cur = next;
    }
    h2 = pre;
    
    //将前后两链表依次相连
    while (h1 && h2) {
      let n1 = h1.next;
      let n2 = h2.next;
      h1.next = h2;
      h1 = n1;
      h2.next = h1;
      h2 = n2;
    }
    
    return head;
  };