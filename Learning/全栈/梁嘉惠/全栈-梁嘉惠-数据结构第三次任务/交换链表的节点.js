var swapNodes = function (head, k) {
    let i = 1
    let pre = head
    let front = null // 定义第K个节点
    let cur = head // 定义倒数第K个节点
    while (pre) {
        if (i === k) { // 遍历先找到第K个节点
            front = pre
        }
        if (i > k) { //开始移动cur指针
            cur = cur.next
        }
        pre = pre.next
            ++i;
    }
    // 交换节点的值
    let temp = null
    temp = front.val
    front.val = cur.val
    cur.val = temp
    return head
};