/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */


 var hasCycle = function(head) {
    // 快慢指针初始化指向 head
    let slow = head;
    let fast = head;
    // 快指针走到末尾时停止循环
    while (fast && fast.next) {
      // 慢指针走一步，快指针走两步
      slow = slow.next;
      fast = fast.next.next;
      // 快慢指针相遇，说明含有环
      if (slow == fast) {
        return true;
      }
    }
    return false;
  };