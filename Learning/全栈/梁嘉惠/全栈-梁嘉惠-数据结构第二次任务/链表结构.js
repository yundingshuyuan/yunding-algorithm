var ListNode = function () {
    //定义两个指针
    this.head = null;
    this.nav = null;
    this.length = 0;
};

var Node = function (val) {
    this.val = val;
    this.next = null;
}
//将要添加的元素加到数组尾部
ListNode.prototype.add = function (val) {
    const nodes = new Node(val);
    this.nav = nodes;
    const last = this.nav;
    if (last) {
        last.next = this.nav;
    }
    if (!this.head) {
        this.head = nodes;
        this.head.next = null;
    }
    this.length++;
};
//根据索引删除指定元素
ListNode.prototype.delete = function (val) {
    let dummy = new ListNode()//设置虚拟头
    dummy.next = head
    let cur = dummy

    while (head) {
        // 当前节点为匹配值
        if (head.val == val) {
            head = head.next
        } else {
            cur.next = head
            cur = head
            head = head.next
        }
    }
    cur.next = head
    return dummy.next

};
//展示数组内容
// ListNode.prototype.list = function () {
//     let cur = this.head;
//     let lists = '';
//     while (cur) {
//         lists += cur.val + ' ';
//         cur = cur.next;
//     }
//     return lists;
// };

// let node = new ListNode()
// node.add('1');
// node.add('2');
// node.add('3');
// node.delete('2'); 