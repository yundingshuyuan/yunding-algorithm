var removeNthFromEnd = function (head, n) {
  const ret = new ListNode();// 定义虚拟头结点
  ret.next=head;
  let slow = fast=ret;//定义快慢指针
  // if (head.next === null && n === 1) {
  //   return head = head.next;
  // }
  for(i=0;i<n;i++) {  //快指针先走n步
      fast = fast.next;
  }
  while (fast.next !== null) {//快慢指针同时移动
      slow = slow.next;
      fast = fast.next;
  }
  slow.next = slow.next.next ;// 删除倒数第n个结点
  return ret.next;
};
