let isValid = function(s) {
    let stack = [], length = s.length;
    if(length % 2) return false;
    for(let item of s){//for of 拿到数组对象的值
        switch(item){
            //遇上左括号，压入栈
            case "{":
            case "[":
            case "(":
                stack.push(item);
                break;
            //遇上右括号，弹出栈顶
            //若两者对应,就抵消；若不对应，返回false
            case "}":
                if(stack.pop() !== "{") return false;
                break;
            case "]":
                if(stack.pop() !== "[") return false;
                break;
            case ")":
                if(stack.pop() !== "(") return false;
                break;
        }
    }
    return !stack.length;
};
