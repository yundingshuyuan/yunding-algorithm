//初步想法，建立一个新数组，将数组的每一个元素平方后传入新数组，再进行排序
// function fn(){
//     var arr=[5,3,7,2,1,6];
//     var arr1=[];
//     for(var i=0;i<arr.length;i++){
//         arr1[i]=arr[i]*arr[i];
//     }
//     console.log(arr1);
//     // 平方后排序
//     for(var i=0;i<arr1.length-1;i++){
//         for(var j=0;j<arr1.length-1-i;j++){
//             if(arr1[j]>arr1[j+1]){
//                 var temp = arr1[j];
//                 arr1[j] = arr1[j+1];
//                 arr1[j+1] = temp;
//             }
//         }
//     }
//     console.log(arr1);
// }
// fn();

//双指针方法
var sortedSquares = function (nums) {
    let left = 0;//左指针，指向原数组最左边
    let right = nums.length - 1;//// 右指针，指向原数组最右边
    let res = [];//创建一个新数组，存储平方值
    while (left <= right) {
      if (nums[left] * nums[left]> nums[right] * nums[right]) {
        res.unshift(nums[left] * nums[left]);//unshift() 方法可向数组的开头添加一个或更多元素，并返回新的长度。
        left++; // 左指针右移
      } else {
        res.unshift(nums[right] * nums[right]);
        right--; // 右指针左移
      }
    }
    return res;
  };