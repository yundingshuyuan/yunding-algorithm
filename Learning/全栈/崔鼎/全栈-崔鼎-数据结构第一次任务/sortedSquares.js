/**
 * @param {number[]} nums
 * @return {number[]}
 */
//双指针法
var sortedSquares = function (nums) {
  let left = 0,
    right = nums.length - 1;
  let res = [];
  while (left <= right) {
    let leftResult = nums[left] * nums[left],
      rightResult = nums[right] * nums[right];
    if (leftResult > rightResult) {
      res.unshift(leftResult);
      left++;
    } else {
      res.unshift(rightResult);
      right--;
    }
  }
  return res;
};
