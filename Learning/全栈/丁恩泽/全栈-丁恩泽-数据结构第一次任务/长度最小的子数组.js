/**
 * @param {number} target
 * @param {number[]} nums
 * @return {number}
 */
 var minSubArrayLen = function(target, nums) {
    let left=0;
    let right =-1;
    //当前子数组的和
    let sum = 0;
    //返回的最小子数组的长度
    let res = nums.length+1;
    while (left<nums.length){
        //存在滑动窗口
        if(right+1 <nums.length && sum <target){
            right ++;
            sum += nums[right];
        }
        else{
            sum -=nums[left];
            left ++;
        }

        if(sum >=target){
            res = Math.min(res,right-left+1);
        }
    }
    if(res == nums.length+1){
        return 0;
    }
    return res; 
};