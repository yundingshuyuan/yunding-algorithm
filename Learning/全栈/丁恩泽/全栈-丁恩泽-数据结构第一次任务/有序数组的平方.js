/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var sortedSquares = function(nums) {
    var len = nums.length;
        var nums1 = [];
        for (var i = 0; i < len; i++){
            nums1[i] = nums[i] * nums[i];
        }
       nums1.sort(function(a,b){
           return a-b});
        return nums1;
};