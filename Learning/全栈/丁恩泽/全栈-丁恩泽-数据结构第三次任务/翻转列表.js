var reseveList = function(head){
    let temp = null;
    let pre = null;
    let cur = head;
    if(cur ==null){
        return null;
    }
    while(cur.next != null){
        temp = cur.next;
        cur.next = pre;
        pre = cur;
        cur = temp;
    }
    return pre;
};