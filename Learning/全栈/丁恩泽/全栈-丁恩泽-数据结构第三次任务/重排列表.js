/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {void} Do not return anything, modify head in-place instead.
 */
var reorderList = function (head) {
    if (!head || !head.next || !head.next.next) {
        return head;
    }
    const arr = [];
    let len = 0;
    while (head) {
        arr[len] = head;
        len++;
        head = head.next;
    }

    let l = Math.floor(len / 2);
    for (let i = 0; i < l; i++) {
        if (i !== l - 1 || len % 2 !== 0) {
            arr[len - i - 1].next = arr[i].next;
            arr[i].next = arr[len - i - 1];
        }

        if (i === l - 1) {    //最后一个节点
            if (len % 2 !== 0) {
                arr[len - i - 1].next.next = null;
            } else {
                arr[i].next.next = null;
            }
        }
    }
};
// 学习中