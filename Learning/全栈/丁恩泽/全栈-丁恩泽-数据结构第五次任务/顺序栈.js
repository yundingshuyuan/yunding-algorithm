// 顺序栈类
// 因为要开辟地址储存栈内数据，可以写一个数组来开辟地址
    class Stack {
        // // 该类只是在TS中实现
        // data: Array<any>;
        // top: number; // 栈顶
        // size: number;
        // constructor(maxLength: number) {
        //     this.data = new Array(maxLength);
        //     this.top = -1;
        //     this.size = maxLength;
        // }
        constructor(maxLength) {
            this.arr = new Array(maxLength);
            this.top = -1;
            this.size = maxLength;
        }
        // 推入元素
        push(value) {
            if (this.top === this.arr.length - 1) {
                // throw new Error('Overflow!');
                console.log("stack is empty");
            }
            // 如果栈内储存元素不为0
            // 栈顶标记+1,并将元素放入数组
            this.top++;
            this.data[this.top] = value;
        }
        // 推出栈元素
        pop() {
            if (this.top === -1) {
                console.log('Empty stack!');
            }
            const element = this.data[this.top];
            this.top--;
            this.data.length--;
            return element;
        }
        // 获取栈元素的个数
        getLength() {
            return this.arr.length;
        }
        // 输出元素
        print() {
            for(let i = 0;i<this.arr.length;i++) {
                console.log(this.arr[i]);
            }
        }
    }
    const stack = new Stack(5);
    stack.push(3);
    stack.push(8);
    stack.push(2);
    stack.push(9);
    stack.push(5);
    console.log('移出栈顶元素 :', stack.pop());
    console.log('栈内元素数量：', stack.getLength());
    console.log('栈内元素都为：',stack.print());

