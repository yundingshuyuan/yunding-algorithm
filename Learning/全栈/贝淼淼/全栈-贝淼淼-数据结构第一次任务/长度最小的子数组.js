// /**
//  * @param {number} target
//  * @param {number[]} nums
//  * @return {number}
//  */
var minSubArrayLen = function(target, nums) {
    let sum = 0;
    let i = 0; 
    let result = nums.length + 1;
    for (let j = 0; j < nums.length; j++) { 
        sum = sum + nums[j]; 
        while (sum >= target) { 
            let sub = j - i + 1;
            result = result < sub ? result : sub; 
            sum = sum - nums[i++];
        }
    }
    return result > nums.length ? 0 : result;
};
