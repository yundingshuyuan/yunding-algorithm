#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<windows.h>

typedef int Status;
typedef int SElemType;

//动态分配存储结构
#define LENGTH  100   // 栈的存储空间初始分配量 
#define  ADDLENGTH  10   //分配增量

typedef struct {
	SElemType *top;  //栈顶指针
	SElemType *base;  //栈底指针 
	int stacklength;  //当前已分配的存储空间 
}SqStack; 

//构造顺序栈
Status InitStack(SqStack &S){
	S.base = (SElemType *)malloc(LENGTH * sizeof(SElemType));  //开辟连续的空间
	
	//如果存储空间分配失败 
	if(!S.base){
		exit(-2);
	} 
	
	S.top = S.base;
	S.stacklength = LENGTH;
	return 1;
} 

//批量元素入栈
//num为要入栈的元素数量 
Status ValueStack(SqStack &S,int num){
	//栈不存在
	if(!S.base){
		printf("栈不存在！");
		return 2;
	} 
	
	//判断传来的栈是否为空栈
	if(S.base != S.top){
		if(num >= S.stacklength){
			//重新分配存储空间  realloc函数 
			S.base = (SElemType *)realloc(S.base,(S.stacklength + ADDLENGTH)*sizeof(SElemType));
			S.stacklength = S.stacklength + ADDLENGTH; 
		}
		
		int temp[num];
		
		 for(int i = 1;i<=num;i++){
		 	printf("请输入第%d个元素的值：",i);
		 	scanf("%d",&temp[i-1]);
		 }
		 
		 for(int j = 1;j<=num;j++){
		 	*S.top = temp[j-1];
		 	S.top++;
		 }
		 
		 printf("%d个元素入栈成功！",num);
	} 
	
	else{
		for(int i = 1;i<=num;i++){
			printf("请输入第%d个元素的值：",i);
			scanf("%d",&S.base[i-1]);
			*(S.top++);
		}
		
		printf("%d个元素入栈成功！",num);
	}
	
	return 1;
} 

//销毁顺序栈
Status DistoryStack(SqStack &S){
	if(!S.base){
		printf("顺序栈不存在！");
		return 2;
	}
	
	
	free(S.base);  //free函数释放空间
	S.stacklength = 0;
	S.top = NULL;
	S.base = NULL;
	
	printf("成功销毁顺序栈！");
	
	return 1; 
} 

//重置顺序栈
Status ClearStack(SqStack &S){
	if(!S.base){
		printf("顺序栈不存在！");
		return 2;
	}
	
	S.top = S.base;
	
	printf("成功重置顺序栈！");
	
	return 1;
} 

//判断顺序栈是否为空
Status StackEmpty(SqStack &S){
	if(!S.base){
		return 2;
	}
	
	if(S.top == S.base){
		return 1;
	}
	else{
		return 0;
	}
} 

//获取顺序栈长度
Status StackLength(SqStack &S){
	if(!S.base){
		printf("顺序栈不存在！");
		return 2;
	}
	
	int len;
	len = S.top - S.base;
	
	printf("顺序栈的长度为：%d",len);
	
	return 1;
} 

//获取栈顶元素
Status GetTop(SqStack &S){
	if(!S.base || S.top == S.base){
		printf("顺序栈不存在！或栈为空");
		return 2;
	}
	
	int top;
	top = *(S.top - 1);
	
	printf("栈顶元素为%d",top);
	
	return 1;
} 

//打印顺序栈
Status PrintStack(SqStack &S){
	if(!S.base){
		printf("顺序栈不存在！");
		return 2;
	}
	else if(S.top == S.base){
		printf("顺序栈为空栈！");
	}
	
	printf("顺序栈元素为：");
	
	for(int i=0;i<(S.top - S.base);i++){
		printf("%d",S.base[i]);
	}
	
	printf("\n");
	
	return 1;
} 
