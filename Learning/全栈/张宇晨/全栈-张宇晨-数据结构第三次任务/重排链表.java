/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public void reorderList(ListNode head) {
        if(head == null || head.next == null || head.next.next == null){
            return;
        }
        int len = 0;
        ListNode cur =head;
        while(cur != null){
            len++;
            cur = cur.next;
        }
        reorderListHelper(head,len);
    }
    private ListNode reorderListHelper(ListNode head,int len){
        if(len == 1){
            ListNode outTail = head.next;
            head.next = null;
            return outTail;
        }
        if(len == 2){
            ListNode outTail = head.next.next;
            head.next.next = null;
            return outTail;
        }
        ListNode tail = reorderListHelper(head.next,len-2);
        ListNode subHead = head.next;
        ListNode outTail =tail.next;
        tail.next = subHead;
        return outTail;
    }
}