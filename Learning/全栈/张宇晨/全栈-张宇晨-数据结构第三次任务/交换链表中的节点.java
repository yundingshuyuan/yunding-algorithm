/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode swapNodes(ListNode head, int k) {
        ListNode cur = head;
        ListNode first = head;
        ListNode last = head;
        int count = 1;
        while(cur.next != null){
            if(count < k){
                first = first.next;
            }else if(count >= k){
                last = last.next;
            }
            count++;
            cur = cur.next;
        }
        count = first.val;
        first.val = last.val;
        last.val = count;
        return head;

    }
}