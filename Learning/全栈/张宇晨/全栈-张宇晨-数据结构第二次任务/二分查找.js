/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function(nums, target) {
    //左闭右闭
    let left = 0, right = nums.length-1;
    while(left <= right){
        let mid = left + Math.floor((right - left)/2);
        //Math.floor(X)返回值小于等于x，且与x最接近的整数。
        if(nums[mid] > target){
            left = left,right = mid - 1;
        }else if(nums[mid] < target){
            left = mid + 1,right = right;
        }else if(nums[mid] = target){
            return mid;
        }
    }
    return -1;
};