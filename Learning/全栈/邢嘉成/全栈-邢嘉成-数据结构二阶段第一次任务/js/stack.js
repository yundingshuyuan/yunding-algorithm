class Stack {
    constructor(maxLength) {
        this.data = new Array(maxLength);
        this.top = -1;
        this.size = maxLength;
    }
    push(value) {
        if (this.top === this.size - 1) {
            console.log('操作无效，溢出了');
            return;
        }
        this.top++;
        this.data[this.top] = value;
    }
    pop() {
        if (this.top === -1) {
            console.log('操作无效，栈内为空');
            return;
        }
        this.top--;
        this.data.length--;
    }
    getLength() {
        return this.top + 1;
    }
    print() {
        for (let item of this.data) {
            console.log(item);
        }
    }
}
let stack = new Stack(5);
stack.push(1);
stack.push(3);
stack.push(2);
stack.push(4);
stack.push(5);
stack.push(6);
stack.pop();
console.log('栈内元素数量：', stack.getLength());
stack.print();

//呃呃，不晓得怎么搞，就照着写一遍