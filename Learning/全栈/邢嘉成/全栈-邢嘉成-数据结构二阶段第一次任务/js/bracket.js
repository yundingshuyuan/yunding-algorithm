/**
 * @param {string} s
 * @return {boolean}
 */
let isValid = function(s) {
    let stack = [];
    let bracket = {
        '(': ')',
        '[': ']',
        '{': '}'
    };
    for(let i of s){
        if (i in bracket) {
            stack.push(i);
            continue;
        }
        if (bracket[stack.pop()] !== i) {
            return false;
        }
    };
    return stack.length === 0;
}