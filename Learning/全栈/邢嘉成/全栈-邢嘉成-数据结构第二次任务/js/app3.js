"use strict";
function ListNode() {
    function Node(data) {
        this.data = data;
        this.next = null;
    }
    this.head = null;
    this.length = 0;
    //add
    ListNode.prototype.add = function (data) {
        let newNode = new Node(data);
        if (this.length == 0) {
            this.head = newNode;
        }
        else {
            let current = this.head;
            while (current.next) {
                current = current.next;
            }
            current.next = newNode;
        }
        this.length++;
    };
    //list
    ListNode.prototype.list = function () {
        let current = this.head;
        let listString = '';
        while (current) {
            listString += current.data + ' ';
            current = current.next;
        }
        console.log(listString);
        return listString;
    };
    //updata
    ListNode.prototype.updata = function (position, newData) {
        position--;
        if (position < 0 || position >= this.length) {
            return false;
        }
        let current = this.head;
        let index = 0;
        while (index++ < position) {
            current = current.next;
        }
        current.data = newData;
        return true;
    };
    //delete
    ListNode.prototype.delete = function (position) {
        position--;
        if (position < 0 || position >= this.length) {
            return false;
        }
        let current = this.head;
        let index = 0;
        while (index + 1 < position) {
            current = current.next;
        }
        current.next = current.next.next;
    };
}
let node = new ListNode();
node.add('1');
node.add('2');
node.add('3');
node.list(); //1 2 3
node.updata('3', '4');
node.list(); //1 2 4
node.delete('2');
node.list(); //1 4
