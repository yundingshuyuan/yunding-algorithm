/**
 * Definition for singly-linked list.
 * class ListNode {
 *     val: number
 *     next: ListNode | null
 *     constructor(val?: number, next?: ListNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.next = (next===undefined ? null : next)
 *     }
 * }
 */

function swapNodes(head: ListNode | null, k: number): ListNode | null {
    let Node = {
        val: null,
        next: null
    };
    Node.next = head;
    let fast = Node;
    while (k != 0 && fast.next) {
        fast = fast.next;
        k--;
    }
    let fastCopy = fast;
    let slow = Node;
    while (fastCopy) {
        fastCopy = fastCopy.next;
        slow = slow.next;
    }
    [fast.val, slow.val] = [slow.val, fast.val];
    return Node.next;
};

let b = {
    val: 1,
    next: {
        val: 2,
        next: {
            val: 3,
            next: {
                val: 4,
                next: {
                    val: 5,
                    next: null
                }
            }
        }
    }
}

console.log(swapNodes(b, 2));