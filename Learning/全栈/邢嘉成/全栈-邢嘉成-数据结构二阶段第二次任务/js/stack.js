function Stack(max) {
    function stack(data) {
        this.data = data;
        this.next = null;
    }
    this.head = null;
    this.length = 0;
    //push
    Stack.prototype.push = function (data) {
        if (this.length >= max) {
            console.log('操作无效，溢出了');
            return;
        }
        let newstack = new stack(data);
        if (this.length == 0) {
            this.head = newstack;
        }
        else {
            let current = this.head;
            while (current.next) {
                current = current.next;
            }
            current.next = newstack;
        }
        this.length++;
    };
    //pop
    Stack.prototype.pop = function () {
        let current = this.head;
        while (current.next.next) {
            current = current.next;
        }
        current.next = null;
    };
    //list
    Stack.prototype.list = function () {
        let current = this.head;
        let listString = '';
        while (current) {
            listString += current.data + ' ';
            current = current.next;
        }
        console.log(listString);
        return listString;
    };
}
let stack = new Stack(3);
stack.push(1);
stack.push(2);
stack.push(3);
stack.push(4);
stack.list(); //1 2 3 4
stack.pop();
stack.list(); //1 2 3