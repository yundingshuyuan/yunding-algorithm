/**
 * @param {string[]} tokens
 * @return {number}
 */
var evalRPN = function(tokens) {
    let stack = [];
    for (let i of tokens) {
        let a = Number(stack[stack.length - 1]);
        let b = Number(stack[stack.length - 2]);
        if (i == '+') {
            stack.pop();
            stack.pop();
            let c = a + b;
            stack.push(c);
        }
        else if (i == '-') {
            stack.pop();
            stack.pop();
            let c = b - a;
            stack.push(c);
        }
        else if (i == '*') {
            stack.pop();
            stack.pop();
            let c = a * b;
            stack.push(c);
        }
        else if (i == '/') {
            stack.pop();
            stack.pop();
            let c = (b - b % a) / a;
            stack.push(c);
            console.log(c);
        }
        else {
            stack.push(i);
        }
    }
    return stack[0];
};

evalRPN([]);