"use strict";
function minSubArrayLen(target, nums) {
    let result = [];
    let result_count = 0;
    for (var i = 0; i < nums.length; i++) {
        // console.log('first='+nums[i]);
        let sum = nums[i];
        let count = 1;
        for (let m = i + 1; sum < target; m++) {
            sum = sum + nums[m];
            count++;
        }
        // console.log('sum='+sum);
        // console.log('count='+count);
        if (sum >= target) {
            result[result_count] = count;
            result_count++;
            // console.log('success!');
        }
        // console.log('///////');
    }
    if (result.length != 0) {
        // console.log(result);
        let min = result[0];
        for (let n = 0; n < result.length; n++) {
            if (result[n] < min) {
                min = result[n];
            }
        }
        return min;
    }
    else {
        return 0;
    }
}
///击败12%.....................
minSubArrayLen(7, [2, 3, 1, 2, 4, 3]);
minSubArrayLen(4, [1, 4, 4]);
minSubArrayLen(11, [1, 1, 1, 1, 1, 1, 1, 1]);
