class Solution:
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        left = right = 0
        res = float('inf')
        temp = nums[0]
        while right<len(nums) and left<=right:
            if temp >= target:
                res = min(res, right-left+1)
                temp -= nums[left]
                left += 1
            elif right != len(nums)-1 and temp<target:
                right += 1
                temp += nums[right]
            else:
                temp -= nums[left]
                left += 1
        return res if res != float('inf') else 0