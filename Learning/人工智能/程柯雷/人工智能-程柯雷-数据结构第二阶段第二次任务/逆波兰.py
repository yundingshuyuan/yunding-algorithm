class Solution:
    def evalRPN(self, tokens: List[str]) -> int:

        def option(li, token):
            if token == '+':
                li[-2] += li[-1]
                li.pop()
            elif token == '-':
                li[-2] -= li[-1]
                li.pop()
            elif token == '/':
                li[-2] = int(li[-2]/li[-1])
                li.pop()
            else:
                li[-2] *= li[-1]
                li.pop()

        res = []
        token = {'+', '-', '*', '/'}
        for i in tokens:
            if i in token:
                option(res, i)
            else:
                res.append(int(i))
        return res[0]
