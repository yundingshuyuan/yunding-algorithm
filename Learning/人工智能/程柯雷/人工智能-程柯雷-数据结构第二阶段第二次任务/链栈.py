from functools import cached_property


class Node:
    
    def __init__(self, val):
        self.val = val
        self.pre = None
        self.next = None
        
        

class LinkStack:
    
    def __init__(self) -> None:
        self.head = None
        self.tail = None
        
    def is_empty(self):
        return not bool(self.head)

    def clear(self):
        self.head = self.tail = None
    
    @cached_property
    def __length(self):
        res = 0
        for _ in self:
            res += 1
        return res
    
    def __len__(self):
        return self.__length
    
    def __str__(self):
        return f'<'+', '.join([str(i) for i in self])+'>'
    
    def __iter__(self):
        self.curr = self.tail
        return self
    
    def __next__(self):
        if self.curr:
            rt_item = self.curr.val
            self.curr = self.curr.pre
            return rt_item
        raise StopIteration
    
    def get_top(self):
        return self.tail.val
    
    def pop(self):
        if self.__length > 0:
            pop_item = self.tail
            self.tail = self.tail.pre
            if not self.tail:
                self.head = None
            self.__length -= 1
        else:
            raise IndexError("linkStack is empty")
        return pop_item.val
    
    def append(self, val):
        self.__length += 1
        if self.is_empty():
            self.head = self.tail = Node(val)
        else:
            self.tail.next = Node(val)
            self.tail.next.pre = self.tail
            self.tail = self.tail.next
 
    def visit(self):
        print(self)


if __name__ == '__main__':
    import sys

    print(f'Python {sys.version} on {sys.platform}')
    print('Type "help", "copyright", "credits" or "license" for more information.')
    while True:
        try:
            order = input('>>> ')
            if 'while' in order or 'for' in order or 'def' in order \
                    or 'class' in order or 'try' in order or 'match' in order:
                order += '\n'
                temp = input('... ')
                temp.replace('\t', '    ')
                while temp:
                    order += temp
                    temp = input('... ')
            try:
                exec(f"if (temp:={order}) is not None:print(temp)")
            except Exception as _:
                exec(order)
        except Exception as error:
            print(type(error), error, sep='\n    ')
