class Solution:
    def hasCycle(self, head: Optional[ListNode]) -> bool:
        temp = set()
        while head:
            if head in temp:
                return True
            temp.add(head)
            head = head.next
        return False