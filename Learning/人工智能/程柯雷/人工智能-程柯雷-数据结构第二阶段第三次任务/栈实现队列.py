class MyQueue:

    def __init__(self):
        self.in_ = deque()
        self.out = deque()

    def push(self, x: int) -> None:
        self.in_.append(x)

    def pop(self) -> int:
        if self.out:
            return self.out.pop()
        else:
            MyQueue.dump(self)
        return MyQueue.pop(self)


    def peek(self) -> int:
        if self.out:
            return self.out[-1]
        else:
            MyQueue.dump(self)
        return MyQueue.peek(self)

    def empty(self) -> bool:
        return not (bool(self.in_) or bool(self.out))


    def dump(self):
        while self.in_:
                self.out.append(self.in_.pop())



# Your MyQueue object will be instantiated and called as such:
# obj = MyQueue()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.peek()
# param_4 = obj.empty()