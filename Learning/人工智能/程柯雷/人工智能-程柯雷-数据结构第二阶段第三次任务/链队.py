from functools import cached_property


class Node:
    
    def __init__(self, val):
        self.val = val
        self.pre = None
        self.next = None
        
        

class LinkQue:
    
    def __init__(self) -> None:
        self.head = None
        
    def is_empty(self):
        return not bool(self.head)

    def clear(self):
        self.head = None
    
    @cached_property
    def __length(self):
        res = 0
        for _ in self:
            res += 1
        return res
    
    def __len__(self):
        return self.__length
    
    def __str__(self):
        return f'| '+', '.join([str(i) for i in self])+' |'
    
    def __iter__(self):
        self.curr = self.head
        return self
    
    def __next__(self):
        if self.curr:
            rt_item = self.curr.val
            self.curr = self.curr.next
            return rt_item
        raise StopIteration
    
    def get_top(self):
        if self.head:
            return self.head.val
        raise IndexError("linkQue is empty")
    
    def pop(self):
        if self.__length > 0:
            pop_item = self.head
            self.head = self.head.next
            self.__length -= 1
        else:
            raise IndexError("linkQue is empty")
        return pop_item.val
    
    def append(self, val):
        self.__length += 1
        if self.is_empty():
            self.head = Node(val)
        else:
            temp_head = self.head
            self.head = Node(val)
            self.head.next = temp_head
 
    def visit(self):
        print(self)


if __name__ == '__main__':
    import sys

    print(f'Python {sys.version} on {sys.platform}')
    print('Type "help", "copyright", "credits" or "license" for more information.')
    while True:
        try:
            order = input('>>> ')
            if 'while' in order or 'for' in order or 'def' in order \
                    or 'class' in order or 'try' in order or 'match' in order:
                order += '\n'
                temp = input('... ')
                temp.replace('\t', '    ')
                while temp:
                    order += temp
                    temp = input('... ')
            try:
                exec(f"if (temp:={order}) is not None:print(temp)")
            except Exception as _:
                exec(order)
        except Exception as error:
            print(type(error), error, sep='\n    ')
