class Solution:
    def swapNodes(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        curr = left = right = head
        step = 1
        while curr:
            if step < k:
                curr = curr.next
                step += 1
            elif step == k:
                left = curr
                step += 1
                curr = curr.next
            else:
                right = right.next
                curr = curr.next
        left.val, right.val = right.val, left.val
        return head