class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        if not head:
            return head
        que = []
        while head:
            que.append(head)
            head = head.next
        temp = res = que.pop()
        while que:
            temp.next = que.pop()
            temp = temp.next
        temp.next = None
        return res
