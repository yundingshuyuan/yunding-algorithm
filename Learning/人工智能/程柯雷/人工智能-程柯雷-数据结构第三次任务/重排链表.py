class Solution:
    def reorderList(self, head: ListNode) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        que = []
        while head:
            que.append(head)
            head = head.next
        lenght = len(que)
        left = que[:(lenght>>1)+lenght%2]
        right = que[(lenght>>1)+lenght%2:][::-1]
        for i in range(lenght):
            que[i] = right[i//2] if i%2 else left[i//2]
        temp = head = que.pop(0)
        while que:
            temp.next = que.pop(0)
            temp = temp.next
        temp.next = None
        return head

# 缺点：没有做到交换位置时交换值，导致时间复杂度过高，且利用left与right作为辅助，导致空间复杂度为O(2.5n)
