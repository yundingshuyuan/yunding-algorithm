class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        pre = curr = head
        for i in range(n):
            curr = curr.next
        if not curr:
            return head.next
        while curr.next:
            pre = pre.next
            curr = curr.next
        pre.next = pre.next.next
        return head
        