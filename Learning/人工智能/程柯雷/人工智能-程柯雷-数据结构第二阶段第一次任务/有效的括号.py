class Solution:
    def isValid(self, s: str) -> bool:
        if len(s) % 2 == 1:
            return False
        result = []
        elem = [ '(',')','{','}','[',']']
        for i in s:
            if i in elem[::2]:
                if i == elem[0]:
                    result.insert(0,elem[1])
                elif i == elem[2]:
                    result.insert(0,elem[3])
                else:
                    result.insert(0,elem[5])
            else:
                if not result:
                    return False
                if i == result[0]:
                    result.pop(0)
                else:
                    return False
        return not result